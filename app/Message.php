<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasPath, RecordsActivity;
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'sender_id', 'receiver_id', 'body', 'read_at',
    ];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['sender', 'receiver'];

    /**
     * A message belongs to a sender.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    /**
     * A message belongs to a receiver.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function receiver()
    {
        return $this->belongsTo(User::class, 'receiver_id');
    }

    /**
     * Mark the message as read.
     *
     * @return void
     */
    public function markAsRead()
    {
        if (is_null($this->read_at)) {
            $this->forceFill(['read_at' => $this->freshTimestamp()])->save();
        }
    }
}
