<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewServiceRequestNotificationToTeam extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Appointment data
     *
     * @var [type]
     */
    protected $appointment;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailSubject = "New Service Request at " . env('APP_NAME');

        return $this->markdown('emails.appointments.request')
            ->subject($emailSubject)
            ->with([
                'appointment' => $this->appointment
            ]);
    }
}
