<?php

namespace App\Mail;

use Illuminate\Mail\Mailable;

class SendEstimate extends Mailable
{   
    protected $user;
    protected $emailBody;
    protected $pdf;
    protected $pdfName;
    protected $emailSubject;
    protected $emailBodyHTML;
    public $filesNames;

    public function __construct($user, string $emailBody, string $emailBodyHTML, $pdf, string $pdfName, string $emailSubject, $filesNames)
    {
        $this->user = $user;
        $this->emailBody = $emailBody;
        $this->pdf = $pdf;
        $this->pdfName = $pdfName;
        $this->emailSubject = $emailSubject;
        $this->emailBodyHTML = $emailBodyHTML;
        $this->filesNames = $filesNames;
    }

    public function build(): self
    {
        $email = $this->view('emails.estimate')
            ->replyTo($this->user->email, $this->user->name)
            ->text('emails.estimate_plain')
            ->attachData($this->pdf->output(), $this->pdfName, ['mime' => 'application/pdf'])
            ->with(['emailBody' => $this->emailBody, 'emailBodyHTML' => $this->emailBodyHTML])
            ->subject($this->emailSubject);

        foreach ($this->filesNames as $fileName) {
            $email->attachFromStorageDisk('shared', $fileName);
        }

        return $email;
    }
}
