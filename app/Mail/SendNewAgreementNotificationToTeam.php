<?php

namespace App\Mail;

use App\Agreement;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewAgreementNotificationToTeam extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var $agreement
     */
    protected $agreement;

    /**
     * Create a new message instance.
     *
     * @param Agreement $agreement
     */
    public function __construct(Agreement $agreement)
    {
        $this->agreement = $agreement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->agreement->isInitiatedAndNotsigned()) {
            $status = 'initiated';
        } elseif($this->agreement->isSigned()) {
            $status = 'executed';
        } elseif($this->agreement->isInitiatedAndSignedByTheEmployee()) {
            $status = 'initiated_and_signed_by_employee';
        } elseif($this->agreement->isInitiatedAndSignedByTheClient()) {
            $status = 'initiated_and_signed_by_client';
        }

        $options = json_decode($this->agreement['options'], true);

        $emailSubject = "A new agreement has been " . str_replace('_', ' ', $status) . " at " . env('APP_NAME');

        return $this->markdown('emails.agreements.agreement_' . $status)
            ->subject($emailSubject)
            ->with([
                'agreement' => $this->agreement,
                'options' => $options
            ]);
    }
}
