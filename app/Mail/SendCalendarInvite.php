<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class SendCalendarInvite extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Invitation source code needed to generate the .ics file
     *
     * @var string
     */
    protected $invitation;

    /**
     * @var object
     */
    protected $appointment;

    /**
     * Create a new message instance.
     *
     * @param $appointment
     * @param $invitation
     */
    public function __construct($appointment, $invitation)
    {
        $this->appointment = $appointment;
        $this->invitation = $invitation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.appointments.invitation')
            ->subject('Miami Gas Appointment')
            ->with([
                'appointment' => $this->appointment
            ])
            ->attachData($this->invitation, 'invitation.ics', [
                'mime' => 'text/calendar',
            ]);
    }
}
