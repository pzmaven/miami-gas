<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContractDownloadLinkToClient extends Mailable
{
    /**
     * Clients first name.
     *
     * @var string $firstName
     */
    protected $firstName;

    /**
     * Hashed contract link.
     *
     * @var string $contractLink
     */
    protected $contractLink;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($firstName, $contractLink)
    {
        $this->firstName = $firstName;
        $this->contractLink = $contractLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailSubject = "Agreement from " . env('APP_NAME');

        return $this->markdown('emails.contracts.download')
            ->subject($emailSubject)
            ->with([
                'firstName' => $this->firstName,
                'contractLink' => $this->contractLink
            ]);
    }
}
