<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReviewAgreementLinkToClient extends Mailable
{
    /**
     * Authenticated user.
     * @var $user
     */
    protected $user;

    /**
     * Clients first name.
     *
     * @var string $firstName
     */
    protected $firstName;

    /**
     * Email body.
     *
     * @var string $emailBody
     */
    protected $emailBody;

    /**
     * Hashed agreement link.
     *
     * @var string $agreementLink
     */
    protected $agreementLink;

    /**
     * The file names of the attachment files.
     *
     * @var array $filesNames
     */
    public $filesNames;

    /**
     * Create a new message instance.
     *
     * @param $user
     * @param $firstName
     * @param $emailBody
     * @param $agreementLink
     * @param null $filesNames
     */
    public function __construct($user, $firstName, $emailBody, $agreementLink, $filesNames = null)
    {
        $this->user = $user;
        $this->firstName = $firstName;
        $this->emailBody = $emailBody;
        $this->agreementLink = $agreementLink;
        $this->filesNames = $filesNames;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailSubject = "Agreement from " . env('APP_NAME');

        $email = $this->markdown('emails.agreements.hash')
            ->subject($emailSubject)
            ->replyTo($this->user->email, $this->user->name)
            ->with([
                'firstName' => $this->firstName,
                'emailBody' => $this->emailBody,
                'agreementLink' => $this->agreementLink
            ]);

        if($this->filesNames !== null) {
            foreach ($this->filesNames as $fileName) {
                $email->attachFromStorageDisk('shared', $fileName);
            }
        }

        return $email;
    }
}
