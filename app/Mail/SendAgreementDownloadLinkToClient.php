<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAgreementDownloadLinkToClient extends Mailable
{
    /**
     * Client's first name.
     *
     * @var string $firstName
     */
    protected $firstName;

    /**
     * Hashed agreement link.
     *
     * @var string $agreementLink
     */
    protected $agreementLink;

    /**
     * Create a new message instance.
     *
     * @param $firstName
     * @param $agreementLink
     */
    public function __construct($firstName, $agreementLink)
    {
        $this->firstName = $firstName;
        $this->agreementLink = $agreementLink;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailSubject = "Agreement from " . env('APP_NAME');

        return $this->markdown('emails.agreements.download')
            ->subject($emailSubject)
            ->with([
                'firstName' => $this->firstName,
                'agreementLink' => $this->agreementLink
            ]);
    }
}
