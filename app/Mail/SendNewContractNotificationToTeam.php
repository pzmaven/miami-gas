<?php

namespace App\Mail;

use App\Contract;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendNewContractNotificationToTeam extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var $contract
     */
    protected $contract;

    /**
     * Create a new message instance.
     *
     * @param Contract $contract
     */
    public function __construct(Contract $contract)
    {
        $this->contract = $contract;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if($this->contract->isInitiatedAndNotsigned()) {
            $status = 'initiated';
        } elseif($this->contract->isSigned()) {
            $status = 'executed';
        } elseif($this->contract->isInitiatedAndSigned()) {
            $status = 'initiated_and_signed';
        }

        $emailSubject = "A new agreement has been " . str_replace('_', ' ', $status) . " at " . env('APP_NAME');

        return $this->markdown('emails.contracts.new_contract_' . $status)
            ->subject($emailSubject)
            ->with([
                'contract' => $this->contract
            ]);
    }
}
