<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendContractPdfToClient extends Mailable
{
    /**
     * Authenticated user.
     * @var $user
     */
    protected $user;

    /**
     * PDF file.
     * @var $user
     */
    protected $pdf;

    /**
     * Client's first name.
     * @var $user
     */
    protected $firstName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $pdf, string $firstName)
    {
        $this->user = $user;
        $this->pdf = $pdf;
        $this->firstName = $firstName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailSubject = "Agreement from " . env('APP_NAME');
        $pdfName = "agreement-" . Carbon::now() . ".pdf";
        $pdf = $this->pdf->output();
        $firstName = $this->firstName;

        return $this->markdown('emails.contracts.pdf')
            ->attachData($pdf, $pdfName, ['mime' => 'application/pdf'])
            ->with([
                'firstName' => $firstName,
            ])
            ->subject($emailSubject)
            ->replyTo($this->user->email, $this->user->name);
    }
}
