<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReviewContractLinkToClient extends Mailable
{
    /**
     * Authenticated user.
     * @var $user
     */
    protected $user;

    /**
     * Clients first name.
     *
     * @var string $firstName
     */
    protected $firstName;

    /**
     * Email body.
     *
     * @var string $emailBody
     */
    protected $emailBody;

    /**
     * Hashed contract link.
     *
     * @var string $contractLink
     */
    protected $contractLink;

    /**
     * The file names of the attachment files.
     *
     * @var array $filesNames
     */
    public $filesNames;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $firstName, $emailBody, $contractLink, $filesNames = null)
    {
        $this->user = $user;
        $this->firstName = $firstName;
        $this->emailBody = $emailBody;
        $this->contractLink = $contractLink;
        $this->filesNames = $filesNames;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $emailSubject = "Agreement from " . env('APP_NAME');

        $email = $this->markdown('emails.contracts.hash')
            ->subject($emailSubject)
            ->replyTo($this->user->email, $this->user->name)
            ->with([
                'firstName' => $this->firstName,
                'emailBody' => $this->emailBody,
                'contractLink' => $this->contractLink
            ]);

        if($this->filesNames !== null) {
            foreach ($this->filesNames as $fileName) {
                $email->attachFromStorageDisk('shared', $fileName);
            }
        }

        return $email;
    }
}
