<?php

namespace App\Mail;

use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendAgreementPdfToClient extends Mailable
{
    /**
     * Authenticated user.
     * @var $user
     */
    protected $user;

    /**
     * PDF file.
     * @var $user
     */
    protected $pdf;

    /**
     * Client's first name.
     * @var $user
     */
    protected $firstName;

    /**
     * Create a new message instance.
     *
     * @param User $user
     * @param $pdf
     * @param string $firstName
     */
    public function __construct(User $user, $pdf, string $firstName)
    {
        $this->user = $user;
        $this->pdf = $pdf;
        $this->firstName = $firstName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $firstName = $this->firstName;
        $emailSubject = "Agreement from " . env('APP_NAME');

        $pdfName = "agreement-" . Carbon::now() . ".pdf";
        $pdf = $this->pdf->output();

        return $this->markdown('emails.agreements.pdf')
            ->attachData($pdf, $pdfName, ['mime' => 'application/pdf'])
            ->with([
                'firstName' => $firstName,
            ])
            ->subject($emailSubject)
            ->replyTo($this->user->email, $this->user->name);
    }
}
