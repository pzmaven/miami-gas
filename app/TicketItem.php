<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TicketItem extends Model
{
    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
    * A ticket item belongs to a ticket.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }

    /**
    * A ticket item belongs to a job item.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function jobItem()
    {
        return $this->belongsTo(JobItem::class);
    }
}
