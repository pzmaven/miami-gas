<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contract extends Model
{
    use HasPath, RecordsActivity, SoftDeletes;

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path', 'isSigned'];

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
    * Boot the model.
    */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($contract) {
            $contract->contractServices->each->delete();
            $contract->contractCancellationFees->each->delete();
            $contract->status->delete();
        });
    }

    /**
     * A contract belongs to a contact.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * A contract belongs to an estimate.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function estimate()
    {
        return $this->belongsTo(Estimate::class);
    }

    /**
     * A contract has many contract services.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contractServices()
    {
        return $this->hasMany(ContractServices::class);
    }

    /**
     * A contract has many contract cancellation fees.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contractCancellationFees()
    {
        return $this->hasMany(ContractCancellationFees::class);
    }

    /**
    * Fetch the contract's date as a property.
    */
    public function setContractDateAttribute($value){
        $this->attributes['contract_date'] = Carbon::createFromFormat('m-d-Y', $value)->format('Y-m-d');
    }

    /**
     * A contract has a status.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(ContractStatus::class);
    }

    /**
     * Check if the contract is signed by both client and employee.
     * @return boolean
     */
    public function isSigned()
    {
        return $this->status['client_signed'] && $this->status['company_signed'];
    }

    /**
     * Check if the contract is initiated and signed only by the employee.
     * @return boolean
     */
    public function isInitiatedAndSigned()
    {
        return !$this->status['client_signed'] && $this->status['company_signed'];
    }

    /**
     * Check if the contract is initiated and NOT signed by the employee or client.
     * @return boolean
     */
    public function isInitiatedAndNotsigned()
    {
        return !$this->status['client_signed'] && !$this->status['company_signed'];
    }

    /**
     * Fetch the contract's isSigned, as a property.
     * @return boolean
     */
    public function getIsSignedAttribute()
    {
        return $this->isSigned();
    }

    /**
     * Scope a query to only include pending contracts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePending($query)
    {
        return $query->join('contract_statuses', 'contract_statuses.contract_id', '=', 'contracts.id')
            ->select('contracts.*')
            ->where('contract_statuses.client_signed', 0)
            ->orWhere('contract_statuses.company_signed', 0)
            ->orderBy('contract_date', 'desc')
            ->get();
    }

    /**
     * Scope a query to only include signed contracts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSigned($query)
    {
        return $query->join('contract_statuses', 'contract_statuses.contract_id', '=', 'contracts.id')
            ->select('contracts.*')
            ->where('contract_statuses.client_signed', 1)
            ->where('contract_statuses.company_signed', 1)
            ->orderBy('contract_date', 'desc')
            ->get();
    }
}
