<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
    * A role belongs to many permissions.
    *
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }

    /**
     * Give permissions to the role.
     * @param array $permissions
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function givePermissions($permissions)
    {
        return $this->permissions()->sync($permissions);
    }

    /**
     * Scope a query to exclude roles of a given type.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  mixed $role
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExclude($query, $role)
    {
        return $query->where('name', '!=', $role);
    }
}
