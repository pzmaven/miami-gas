<?php

namespace App;

use App\Activity;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Appointment extends Model
{
    use SoftDeletes, RecordsActivity, Notable, HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['address', 'user'];

    /**
    * An appointment belongs to an address.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function address()
    {
        return $this->belongsTo(Address::class)->withTrashed();
    }

    /**
    * An appointment belongs to a user.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Scope a query to only include upcoming appointments.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUpcoming($query)
    {
        return $query->where('date', '>', \Carbon\Carbon::now())->get();
    }
}
