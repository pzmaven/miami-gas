<?php

namespace App\Http\Requests;

use App\Contact;
use Illuminate\Foundation\Http\FormRequest;

class ContactsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method())
        {
            case 'POST':
            {
                return [
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
                    'email' => 'nullable|email|unique:contacts',
                    'phone' => 'nullable|max:50|regex:/^\(?([0-9]{3})\) [0-9]{3}-[0-9]{4}( ext:[0-9]{0,5})?/',
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'first_name' => 'required|min:2',
                    'last_name' => 'required|min:2',
                    'email' => 'nullable|email|unique:contacts,email,'.$this->contact->id,
                    'phone' => 'nullable|max:50|regex:/^\(?([0-9]{3})\) [0-9]{3}-[0-9]{4}( ext:[0-9]{0,5})?/',
                ];
            }
            default:break;
        }
    }
}
