<?php

namespace App\Http\Controllers;

use App\User;
use App\Signature;
use App\Http\Requests\UsersFormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\ValidationException;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::latest()->get();

        return view('users.list', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_users');

        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(UsersFormRequest $request)
    {
        $this->authorize('create_users');

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        if($request->role_user) {
            $roles = $request->role_user;
            $user->assignRoles($roles);
        }

        return redirect($user->path())
            ->with('flash', 'A new user has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        $this->authorize('read_users');

        $user = User::find($id);

        if($user->hasSignature) {
            $user->signaturePath = Storage::url($user->signature->path);
        }

        $user->avatar = $user->getAvatar();

        if (request()->expectsJson()) {
            return $user;
        }

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('update_users');

        $user = User::find($id);

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UsersFormRequest $request, User $user)
    {
        $this->authorize('update_users');

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return redirect($user->path())
            ->with('flash', 'The user has been updated!');
    }

    /**
     * @param User $user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws ValidationException
     */
    public function changePassword(User $user, Request $request)
    {
        $request->validate([
            'password' => 'required|confirmed|min:6',
        ]);

        if(!Hash::check($request->old_password, $user->password)) {
            throw ValidationException::withMessages([
                'old_password' => ['Old password is incorrect'],
            ]);
        }

        $user->update([
           'password' => Hash::make($request->password)
        ]);

        return response()->json([
            'message' => 'Password changed successfully'
        ], 200);
    }

    /**
     * Update user's roles.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function roles(Request $request)
    {
        $this->authorize('update_roles');

        $user = User::find($request->user_id);

        if(!$user->isSuperAdmin()) {
            $user->assignRoles($request->role_user);

            return redirect($user->path())
                ->with('flash', 'User roles have been changed!');
        } else {
            abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(User $user)
    {
        $this->authorize('delete_users');

        if(!$user->isSuperAdmin()) {
            $user->delete();
        } else {
            abort(403, 'This action is unauthorized.');
        }

        return [
            'redirect' => route('users.index'),
            'message' => 'The user was deleted!'
        ];
    }

    /**
     * Search users by string.
     *
     * @param $string
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($string)
    {
        $users = User::where('name' , 'LIKE' , '%' . $string . '%')
            ->take(5)
            ->get();

        return response()->json($users);
    }

    /**
     * Update user's avatar.
     * @param User $user
     * @param Request $request
     * @return string
     */
    public function updateAvatar(User $user, Request $request)
    {
        $this->deleteAvatar($user->avatar);

        $storagePath = Storage::disk('public')->put('/avatars/users', $request->file);

        $user->update([
            'avatar' => $storagePath,
        ]);

        return $storagePath;
    }

    /**
     * Remove user's avatar from storage.
     * @param string $avatar
     * @return
     */
    public function deleteAvatar($avatar)
    {
        if($avatar != '/images/user.png') {
            Storage::disk('public')->delete($avatar);
        }
    }

    public function getAllUnreadNotifications(User $user)
    {
        return $user->unreadNotifications;
    }

    public function markNotificationAsRead(User $user, Request $request)
    {
        $notification = $user->notifications()->find($request->id);
        $message = $user->receivedMessages()->find($request->messageId);

        if($notification) {
            $notification->markAsRead();
        }

        if($message) {
            $message->markAsRead();
        }

        if (request()->expectsJson()) {
            return [
                'message' => 'Your notification was marked as read!',
            ];
        }
    }

    public function markAllNotificationsAsRead(User $user, Request $request)
    {
        foreach ($user->unreadNotifications as $notification) {
            $notification->markAsRead();
        }

        if (request()->expectsJson()) {
            return [
                'message' => 'You marked all notifications as read!',
            ];
        }
    }

    public function deleteNotification(User $user, Request $request)
    {
        $notification = $user->notifications()->find($request->id);

        if($notification) {
            $notification->delete();
        }

        if (request()->expectsJson()) {
            return $notification->fresh();
        }
    }
}
