<?php

namespace App\Http\Controllers;

use App\Address;
use App\Agreement;
use App\Http\Requests\AgreementsRequest;
use App\Mail\SendAgreementDownloadLinkToClient;
use App\Mail\SendAgreementPdfToClient;
use App\Mail\SendNewAgreementNotificationToTeam;
use App\Mail\SendReviewAgreementLinkToClient;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use PDF;

class AgreementsController extends Controller
{
    /**
     * @var string
     */
    public $teamEmail;

    /**
     * AgreementsController constructor.
     */
    public function __construct()
    {
        $this->teamEmail = config('mail.team.address');
    }

    /**
     * Get all agreements.
     *
     * @return View
     */
    public function index()
    {
        $agreements = Agreement::latest()->get();

        return view('agreements.list', compact('agreements'));
    }

    /**
     * Display the specified resource.
     *
     * @param Agreement $agreement
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Agreement $agreement)
    {
        return redirect()->route('agreements.sign', [
            $agreement->id, $agreement->status->hash, 'employee'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Agreement $agreement
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Agreement $agreement)
    {
        $this->authorize('delete_contracts');

        $agreement->delete();

        return [
            'redirect' => route('agreements.index'),
            'message' => 'The agreement was deleted!'
        ];
    }

    /**
     * Initiate a new agreement.
     *
     * @param Request $request
     * @return View
     */
    public function create(Request $request)
    {
        $base = $request->base;
        $address = false;

        if(!empty($base)) {
            $bases = config('agreements.templates');

            if (!array_key_exists($base, $bases)) {
                abort(404, 'This page does not exist!');
            }
        }

        if (!empty($request->address)) {

            $address = Address::findOrFail($request->address);
        }

        return view('agreements.create', compact('base', 'address'));
    }

    /**
     * Prepare the agreement.
     *
     * @param Request $request
     * @return View
     */
    public function prepare(Request $request)
    {
        if($request->isMethod('POST')) {
            $base = $request->base;
            $address = Address::findOrFail($request->address_id);
        } else if ($request->isMethod('GET')) {
            if($request->session()->has('errors')) {
                $oldInput = $request->session()->get('_old_input');

                $base = $oldInput['base'];
                $address = Address::findOrFail($oldInput['address_id']);

            } else {
                abort(401, 'Unauthorized');
            }
        }

        return view("agreements.bases.{$base}.prepare", compact('base', 'address'));
    }

    /**
     * Select the services for the agreement.
     *
     * @param Request $request
     * @return View
     */
    public function services(AgreementsRequest $request)
    {
        $options = $request->validated();
        $options['agreement_date'] = Carbon::createFromFormat('m-d-Y', $options['agreement_date'])->format('Y-m-d');

        $agreement = [
            'contact_id' => $request->contact_id,
            'address_id' => $request->address_id,
            'base' => $request->base,
            'options' => json_encode($options)
        ];

        $request->session()->put('agreement', $agreement);

        return view("agreements.bases.{$request->base}.services");
    }

    /**
     * Configure the selected services.
     *
     * @param Request $request
     * @return View
     */
    public function configuration(Request $request)
    {
        if(!$request->session()->has('agreement')) {
            abort(404);
        }

        $services = $request->services;
        $base = session()->get('agreement.base');

        if(empty($request->services)) {
            return view("agreements.bases.{$base}.configuration");
        }

        foreach ($services as $key => $service) {
            $services[$key]['charges'] = config('agreements.templates.' . $base . '.services.' . $key . '.charges');

            if(!isset($service['name'])) {
                unset($services[$key]);
            }
        }

        $options = json_decode($request->session()->get('agreement.options'), true);
        $options['services'] = $services;
        $options['cancellation_fees'] = config('agreements.templates.' . $base . '.cancellation_fees');

        $request->session()->put('agreement.options', json_encode($options));

        return view("agreements.bases.{$base}.configuration", compact( 'options'));
    }

    /**
     * Show the filled agreement and the signature pad to the client.
     *
     * @param Request $request
     * @return View
     */
    public function showFilledAgreementToClient(Request $request)
    {
        $options = json_decode($request->session()->get('agreement.options'), true);
        $options['services_prices'] = $request->services_prices;
        $options['cancellation_fees'] = $request->cancellation_fees;

        $request->session()->put('agreement.options', json_encode($options));

        if($request->submit == 'sendEmail') {
            $viewName = 'agreements.agreement_filled_email';
        } else if($request->submit == 'signNow') {
            $viewName = 'agreements.agreement_filled_client';
        }

        return view($viewName, [
            'agreement' => session()->get('agreement'),
            'base' => session()->get('agreement.base'),
            'options' => json_decode($request->session()->get('agreement.options'), true),
            'pdfable' => false,
        ]);
    }

    /**
     * Show the filled agreement and the signature pad to the employee.
     *
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\Contracts\View\Factory|View
     * @return View
     */
    public function showFilledAgreementToEmployee(Request $request)
    {
        $base = session()->get('agreement.base');
        $company_signature_required = config("agreements.templates.{$base}.company_signature_required");

        if(!$company_signature_required) {
            session()->put('agreement.company_signature_name_typed', NULL);
            session()->put('agreement.company_signature_date', NULL);
            session()->put('agreement.company_signature', NULL);

            return redirect('agreements/store');
        }

        if(auth()->user()->hasSignature && auth()->user()->signature->auto_sign && $company_signature_required) {
            $signaturePath = $this->getSignaturePath();

            $signature['path'] = auth()->user()->signature->path;

            Storage::disk('public')->copy($signature['path'], $signaturePath);

            $signature['date'] = session('agreement.client_signature_date');

            session()->put('agreement.company_signature_name_typed', auth()->user()->name);
            session()->put('agreement.company_signature_date', $signature['date']);
            session()->put('agreement.company_signature', $signaturePath);

            return redirect('agreements/store');
        }

        return view('agreements.agreement_filled_employee', [
            'base' => session()->get('agreement.base'),
            'options' => json_decode($request->session()->get('agreement.options'), true),
            'pdfable' => false,
        ]);
    }

    /**
     * Sign the agreement now by the client.
     *
     * @param Request $request
     */
    public function signNowClient(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $request->session()->put('agreement.client_signature_name_typed', trim($request->full_name_typed));
        $request->session()->put('agreement.client_signature_date', $signature['date']);
        $request->session()->put('agreement.client_signature', $signature['path']);
    }

    /**
     * Sign the agreement now by the employee.
     *
     * @param Request $request
     */
    public function signNowEmployee(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $request->session()->put('agreement.company_signature_name_typed', trim($request->full_name_typed));
        $request->session()->put('agreement.company_signature_date', $signature['date']);
        $request->session()->put('agreement.company_signature', $signature['path']);
    }

    /**
     * Sign the agreement via e-mail by the client.
     *
     * @param Request $request
     * @return array
     */
    public function signEmailClient(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $agreement = Agreement::findOrFail($request->agreementId);

        $agreement->update([
            'client_signature_name_typed' => trim($request->full_name_typed),
            'client_signature_date' => $signature['date'],
            'client_signature' => $signature['path']
        ]);

        $agreement->status()->update([
            'client_signed' => true,
        ]);

        $agreement = $agreement->fresh();
        $agreement->link = $this->generateAgreementLink($agreement->id, $agreement->status->hash);

        Mail::to($this->teamEmail)->send(
            new SendNewAgreementNotificationToTeam($agreement)
        );

        $this->sendAgreementDownloadLink($agreement);

        return [
            'redirect' => route('agreements.thankyou'),
            'message' => 'The agreement was successfully signed!'
        ];
    }

    /**
     * Sign the agreement via e-mail by the employee.
     *
     * @param Request $request
     * @return array
     */
    public function signEmailEmployee(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $agreement = Agreement::findOrFail($request->agreementId);

        $agreement->update([
            'company_signature_name_typed' => trim($request->full_name_typed),
            'company_signature_date' => $signature['date'],
            'company_signature' => $signature['path']
        ]);

        $agreement->status()->update([
            'company_signed' => true,
        ]);

        $this->sendAgreementDownloadLink($agreement->fresh());

        return [
            'redirect' => route('agreements.index'),
            'message' => 'The agreement was successfully signed!'
        ];
    }

    /**
     * Save the signatures on storage.
     *
     * @param $signatureData
     * @param $signatureDate
     * @return array
     */
    public function storeSignature($signatureData, $signatureDate)
    {
        $signaturePath = $this->getSignaturePath();
        $signature = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $signatureData));

        Storage::disk('public')->put($signaturePath, $signature);

        $signatureDate = Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $signatureDate)->format('Y-m-d');

        return [
            'path' => $signaturePath,
            'date' => $signatureDate
        ];
    }

    /**
     * Generate the signature path.
     *
     * @return string
     */
    public function getSignaturePath()
    {
        $now = Carbon::now();

        return "signatures/" . $now->format('Y') . "_" . $now->format('m') . "/" . uniqid() . ".png";
    }

    /**
     * Store the agreement to the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        if(!session()->has('agreement')) {
            abort(404);
        }

        $data = session()->get('agreement');

        $agreement = Agreement::create([
            'contact_id' => $data['contact_id'],
            'address_id' => $data['address_id'],
            'base' => $data['base'],
            'options' => $data['options'],
            'client_signature_name_typed' => $data['client_signature_name_typed'],
            'client_signature_date' => $data['client_signature_date'],
            'client_signature' => $data['client_signature'],
            'company_signature_name_typed' => $data['company_signature_name_typed'],
            'company_signature_date' => $data['company_signature_date'],
            'company_signature' => $data['company_signature']
        ]);

        $agreement = $agreement->fresh();
        $options = json_decode($agreement->options, true);

        $hash = $this->generateAgreementHash($agreement);

        $agreement->status()->create([
            'agreement_id' => $agreement->id,
            'hash' => $hash,
            'client_signed' => true,
            'company_signed' => true,
        ]);

        $agreementPdf = $this->generateAgreementPDF($agreement->id);

        $agreement = $agreement->fresh();
        $agreement->link = $this->generateAgreementLink($agreement->id, $hash);
        $user = auth()->user();

        Mail::to($options['email'])->send(
            new SendAgreementPdfToClient($user, $agreementPdf, $options['first_name']));

        Mail::to($this->teamEmail)->send(
            new SendNewAgreementNotificationToTeam($agreement)
        );

        return redirect('/agreements')->with('flash', 'The agreement was created!');
    }

    /**
     * Generate agreement and send it to the client via e-mail.
     *
     * @return \Illuminate\Http\RedirectResponse|array
     */
    public function sendAgreementViaEmail(Request $request)
    {
        $emails = $request->emails;
        $emailBody = $request->emailBody;
        $filesNames = $request->filesNames;
        $user = auth()->user();

        $data = session()->get('agreement');

        $agreement = Agreement::create([
            'contact_id' => $data['contact_id'],
            'address_id' => $data['address_id'],
            'base' => $data['base'],
            'options' => $data['options'],
        ]);

        $agreement = $agreement->fresh();
        $options = json_decode($agreement->options, true);

        $hash = $this->generateAgreementHash($agreement);

        $agreement->status()->create([
            'agreement_id' => $agreement->id,
            'hash' => $hash,
            'client_signed' => false,
            'company_signed' => false,
        ]);

        $company_signature_required = config("agreements.templates.{$data['base']}.company_signature_required");

        if(!$company_signature_required) {
            $agreement->update([
                'company_signature_name_typed' => NULL,
                'company_signature_date' => NULL,
                'company_signature' => NULL
            ]);

            $agreement->status()->update([
                'company_signed' => true,
            ]);
        }

        if(auth()->user()->hasSignature && auth()->user()->signature->auto_sign && $company_signature_required) {
            $signaturePath = $this->getSignaturePath();

            $signature['path'] = auth()->user()->signature->path;
            $signature['date'] = Carbon::now()->format('Y-m-d');

            Storage::disk('public')->copy($signature['path'], $signaturePath);

            $agreement->update([
                'company_signature_name_typed' => trim(auth()->user()->name),
                'company_signature_date' => $signature['date'],
                'company_signature' => $signature['path']
            ]);

            $agreement->status()->update([
                'company_signed' => true,
            ]);
        }

        $agreement = $agreement->fresh();
        $agreement->link = $this->generateAgreementLink($agreement->id, $hash);

        foreach ($emails as $email) {
            Mail::to($email)->send(
                new SendReviewAgreementLinkToClient($user, $options['first_name'], $emailBody, $agreement->link, $filesNames)
            );
        }

        Mail::to($this->teamEmail)->send(
            new SendNewAgreementNotificationToTeam($agreement)
        );

        if (request()->expectsJson()) {
            return [
                'redirect' => route('agreements.pending'),
                'message' => 'A link to the agreement was sent to the client!'
            ];
        }

        return redirect('/agreements')->with('flash', 'A link to the agreements was sent to the client!');;
    }

    /**
     * Resend agreement link to client.
     *
     * @param Request $request
     * @return array
     */
    public function resendAgreementLink(Request $request)
    {
        $agreement = $request->agreement;
        $options = json_decode($agreement['options'], true);

        $link = $this->generateAgreementLink($request->agreement['id'], $request->agreement['status']['hash']);

        foreach ($request->emails as $email) {
            Mail::to($email)->send(
                new SendReviewAgreementLinkToClient(auth()->user(), $options['first_name'], $request->emailBody, $link)
            );
        }

        return [
            'message' => 'The agreement link e-mail was resent!',
        ];
    }

    /**
     * Generate a unique agreement hash.
     *
     * @param $param
     * @return string
     */
    public function generateAgreementHash($param)
    {
        return base64_encode(\Hash::make($param));
    }

    /**
     * Generate the PDF file for the agreement with the given id.
     *
     * @param $id
     * @return mixed
     */
    public function generateAgreementPDF($id)
    {
        $agreement = Agreement::findOrFail($id);

        $base = $agreement->base;
        $options = json_decode($agreement->options, true);

        $pdfable = true;

        return PDF::loadView('agreements.pdf', compact('agreement','base', 'options', 'pdfable'));
    }

    /**
     * Generate the full url address for the agreement.
     *
     * @param $id
     * @param $hash
     * @return string
     */
    public function generateAgreementLink($id, $hash)
    {
        return env('APP_URL') . 'agreements/'. $id . '/sign/' . $hash;
    }

    /**
     * Agreement link landing page.
     *
     * @param $id
     * @param $hash
     * @param Request $request
     * @return View
     */
    public function signAgreementLink($id, $hash, Request $request)
    {
        if($request->user == 'employee' && !auth()->id()) {
            $this->middleware('auth');
        }

        $agreement = Agreement::findOrFail($id);

        if($agreement->status->hash != $hash) {
            abort(401, 'This action is unauthorized.');
        }

        if(!auth()->user() && $agreement->isSigned()) {
            return view('agreements.download', compact('id', 'hash'));
        }

        if(!auth()->user() && $agreement->status->client_signed) {
            return view('agreements.thankyou');
        }

        return view('agreements.agreement_filled_email', [
            'base' => $agreement->base,
            'agreement' => $agreement,
            'options' => json_decode($agreement->options, true),
            'pdfable' => false,
        ]);
    }

    /**
     * Download the PDF file for the agreement with the given id.
     *
     * @param $id
     * @return mixed
     */
    public function downloadAgreementPDF($id)
    {
        $pdf = $this->generateAgreementPDF($id);

        return $pdf->stream('document-agreement-' . $id . '.pdf');
    }

    /**
     * Download the agreement pdf from the public link.
     *
     * @param int $id
     * @param string $hash
     * @return mixed
     */
    public function downloadAgreementLink($id, $hash)
    {
        $agreement = Agreement::findOrFail($id);

        if($agreement->status->hash != $hash) {
            abort(401, 'This action is unauthorized.');
        }

        $pdf = $this->generateAgreementPDF($id);

        return $pdf->download('document-agreement-' . $id . '.pdf');
    }

    /**
     * Send agreement download link to the client after both parties signed it.
     *
     * @param Agreement $agreement
     */
    public function sendAgreementDownloadLink(Agreement $agreement)
    {
        if($agreement->isSigned()) {
            $agreementLink = $this->generateAgreementLink($agreement->id, $agreement->status->hash);
            $options = json_decode($agreement->options, true);

            Mail::to($options['email'])->send(
                new SendAgreementDownloadLinkToClient($options['first_name'], $agreementLink)
            );
        }
    }

    /**
     * Get a list of all the pending agreements.
     *
     * @return View
     */
    public function getPendingAgreements()
    {
        $pendingAgreements = Agreement::pending();

        return view('agreements.pending', compact('pendingAgreements'));
    }

    /**
     * Get a list of all signed agreements.
     *
     * @return View
     */
    public function getSignedAgreements()
    {
        $signedAgreements = Agreement::signed();

        return view('agreements.signed', compact('signedAgreements'));
    }
}
