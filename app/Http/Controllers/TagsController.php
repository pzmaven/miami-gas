<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;

class TagsController extends Controller
{
    /**
     * Get all tags.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll()
    {
        $tags = Tag::all()->toArray();

        return response()->json([
            'tags' => $tags
        ], 200);
    }
}
