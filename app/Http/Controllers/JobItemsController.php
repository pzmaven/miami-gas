<?php

namespace App\Http\Controllers;

use App\JobItem;
use App\Estimate;
use App\EstimateItem;
use Illuminate\Http\Request;

class JobItemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('read_jobitems');

        $jobitems = JobItem::get();

        if (request()->expectsJson()) {
            return $jobitems;
        }

     	return view('jobitems.list', compact('jobitems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create_jobitems');

        return view('jobitems.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create_jobitems');

        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'um' => 'required',
        ]);

        $jobitem = JobItem::create([
            'name' => $request->name,
            'description_internal' => $request->description_internal,
            'description_customer' => $request->description_customer,
            'type' => $request->type,
            'um' => $request->um,
            'cost' => $request->cost,
            'price' => $request->price
        ]);

        return redirect('/jobitems')
            ->with('flash', 'A new jobitem has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\JobItem  $jobitem
     * @return \Illuminate\Http\Response
     */
    public function edit(JobItem $jobitem)
    {
        $this->authorize('update_jobitems');

        return view('jobitems.edit', compact('jobitem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\JobItem  $jobitem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobItem $jobitem)
    {
        $this->authorize('update_jobitems');

        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'um' => 'required',
        ]);

        $jobitem->update($request->all());

        return redirect('/jobitems')
            ->with('flash', 'The item was updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\JobItem  $jobitem
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobItem $jobitem)
    {
        $this->authorize('delete_jobitems');

        $jobitem->delete();

        return [
            'redirect' => route('jobitems.index'),
            'message' => 'Your jobitem was deleted!'
        ];
    }
}
