<?php

namespace App\Http\Controllers;

use App\Address;
use App\Contact;
use App\Lead;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function search(Request $request)
    {
        $query = $request['query'];

        $addresses = Address::search($query)->get();
        $contacts = Contact::search($query)->get();
        $leads = Lead::search($query)->get();

        return view('search.results', [
            'query' => $query,
            'addresses' => $addresses,
            'contacts' => $contacts,
            'leads' => $leads
        ]);
    }
}
