<?php

namespace App\Http\Controllers;

use App\Message;
use Illuminate\Http\Request;

class MessagesController extends Controller
{

    public function send(Request $request)
    {
        $user = auth()->user();

        $request->validate([
            'body' => 'required|min:2',
        ]);

        foreach ($request->recipients as $recipientId) {
            $user->sendMessageTo($recipientId, $request->body);
        }

        return [
            'message' => 'The message was sent successfully!'
        ];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Message $message)
    {
        if(!auth()->id() && !auth()->id() == $message->receiver_id) {
            abort(403, 'Unauthorized action.');
        }

        return $message;
    }

    public function markMessageAsRead(Message $message)
    {
        if(!auth()->id() && !auth()->id() == $message->receiver_id) {
            abort(403, 'Unauthorized action.');
        }

        if(!$message) {
            abort(404, 'Invalid URL');
        }

        $message->markAsRead();

        return $message;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message $message
     * @return array
     */
    public function destroy(Message $message)
    {
        $message->delete();

        return [
            'message' => 'The message was deleted!'
        ];
    }
}
