<?php

namespace App\Http\Controllers;

use App\Signature;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SignaturesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        if(auth()->id() == $user->id) {
            $signatureFile = $this->decodeSignature($request->signature);

            $pathToSignature = 'signatures/users/' . uniqid() . '.png';

            Storage::disk('public')->put($pathToSignature, $signatureFile);

            $signature = Signature::create([
                'user_id' => $user->id,
                'path' => $pathToSignature
            ]);

            if (request()->expectsJson()) {
                return $signature->fresh();
            }
        } else {
            abort(403, 'This action is unauthorized.');
        }
    }

    /**
     * Decode signature.
     *
     * @param $signature
     * @return bool|string
     */
    private function decodeSignature($signature)
    {
        return base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $signature));
    }

    /**
     * Update the specified resource in storage.
     * @param int $userId
     * @return mixed
     */
    public function update($userId)
    {
        $user = User::findOrFail($userId);
        $signature = $user->signature;

        $signature->update([
            'auto_sign' => request('auto_sign'),
        ]);

        if (request()->expectsJson()) {
            return $signature;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $userId
     * @return array
     */
    public function destroy($userId)
    {
        $user = User::find($userId);
        $signature = $user->signature;

        Storage::disk('public')->delete($signature->path);
        $signature->delete();

        return [
            'message' => 'The signature was deleted!'
        ];
    }
}
