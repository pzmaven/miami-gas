<?php

namespace App\Http\Controllers;

use App\EstimateItem;
use Illuminate\Http\Request;

class EstimateItemsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function store(Request $request)
    {
        $items = $request->all();

        $response = [];

        foreach ($items as $item) {
            $data = array(
                'estimate_id' => $item['estimate_id'],
                'job_item_id' => $item['job_item_id'],
                'name' => $item['name'],
                'description_customer' => $item['description_customer'],
                'qty' => $item['qty'],
                'um' => $item['um'],
                'price' => $item['price'],
                'price_extended' => $item['price_extended'],
                'taxed' => $item['taxed']
            );

            $item = EstimateItem::create($data);

            array_push($response, $item);
        }

        if (request()->expectsJson()) {
            return $response;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $item = EstimateItem::find($id);

        $item->update([
            'description_customer' => request('description_customer'),
            'price' => request('price'),
            'qty' => request('qty'),
            'price_extended' => request('price_extended'),
            'taxed' => request('taxed')
        ]);

        if (request()->expectsJson()) {
            return $item;
        }
    }

    public function updateByColumn($id)
    {
        $item = EstimateItem::find($id);

        $column = request('column');

        $item->update([
            $column => request($column)
        ]);

        if (request()->expectsJson()) {
            return $item;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = EstimateItem::find($id);

        $item->delete();
    }
}
