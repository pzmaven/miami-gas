<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;

class NotesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $note = Note::create([
            'user_id' => auth()->id(),
            'notable_id' => $request->notable_id,
            'notable_type' => $request->notable_type,
            'body' => $request->body,
            'privacy' => $request->privacy,
        ]);

        if (request()->expectsJson()) {
            return $note->fresh();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Note  $note
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Note $note)
    {
        $this->authorize('delete_notes');

        $note->delete();

        return [
            'message' => 'The note was deleted!'
        ];
    }
}
