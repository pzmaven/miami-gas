<?php

namespace App\Http\Controllers;

use App\Services\DashboardReports;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     * @throws \ReflectionException
     */
    public function index(DashboardReports $dashboardReports)
    {
        $reports = $dashboardReports->generate();

        return view('dashboard.dashboard', compact('reports'));
    }
}
