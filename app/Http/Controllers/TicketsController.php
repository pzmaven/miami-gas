<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ticket;
use App\JobItem;
use App\TicketItem;
use App\User;

class TicketsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tickets = Ticket::latest()->take(100)->get();

        return view('tickets.list', compact('tickets'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2',
        ]);

        $ticket = Ticket::create([
            'user_id' => auth()->id(),
            'name' => $request->name,
            'status' => 0, // status 0 = tanks not filled
        ]);

        $this->addItems($request, $ticket);

        if (request()->expectsJson()) {
            return [
                'redirect' => route('tickets.open'),
                'message' => 'A new ticket has been created!'
            ];
        }

        return redirect($ticket->path())
            ->with('flash', 'A new ticket has been created!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Ticket $ticket
     * @return array
     * @throws \Exception
     */
    public function destroy(Ticket $ticket)
    {

        $ticket->delete();

        return [
            'redirect' => route('tickets.index'),
            'message' => 'The ticket was deleted!'
        ];
    }

    /**
     * Get a list of all available tank types.
     *
     * @return mixed
     */
    public function getTanks()
    {
        $tanks = JobItem::where('type', 'Ticket')
            ->where('um', 'Tank')
            ->get();
        
        return $tanks;
    }

    /**
     * Add items to a ticket.
     *
     * @param Request $request
     * @param Ticket $ticket
     * @return Ticket|array
     */
    public function addItems(Request $request, Ticket $ticket)
    {
        $tanks = $request->tanks;
        $gallons = $request->gallons;
        $customerType = $request->customerType;
        $customerDiscounts = config('tickets.customer_discounts');

        if($gallons > 0) {
            // there must be only one jobitem with type = ticket and um = gallons
            // the tanks have um = tank
            $propanePerGallon = JobItem::where('type', 'Ticket')->where('um', 'Gallons')->first();
            
            if(!isset($customerDiscounts[$customerType][$propanePerGallon->name])) {
                $price = $propanePerGallon->price - $customerDiscounts[$customerType]['default_discount'] / 100;
            } else {
                $price = $customerDiscounts[$customerType][$propanePerGallon->name];
            }

            $ticketItem = TicketItem::updateOrCreate([
                'ticket_id' => $ticket->id,
                'job_item_id' => $propanePerGallon->id,
                'name' => $propanePerGallon->name,
                'um' => $propanePerGallon->um,
            ]);

            $ticketItem->qty = $gallons;
            $ticketItem->price = $price;
            $ticketItem->price_extended = $gallons * $price;

            $ticketItem->save();
        }

        if($ticket->items->isEmpty() && empty($customerType)) {
            foreach($tanks as $tank) {
                if($tank['qty'] > 0) {
                
                    $item = [
                        'ticket_id' => $ticket->id,
                        'job_item_id' => $tank['id'],
                        'name' => $tank['name'],
                        'um' => $tank['um'],
                        'qty' => $tank['qty'],
                    ];
        
                    TicketItem::create($item);
                }
            }
        } elseif($ticket->items->isEmpty() && !empty($customerType)) {
            foreach($tanks as $tank) {
                if($tank['qty'] > 0) {
                    
                    if(!isset($customerDiscounts[$customerType][$tank['name']]) && $tank['price'] > 0) {
                        $price = $tank['price'] - $customerDiscounts[$customerType]['default_discount'] / 100;
                    } elseif($tank['price'] < 1) {
                        $price = 0;
                    } else {
                        $price = $customerDiscounts[$customerType][$tank['name']];
                    }
                    
                    $item = [
                        'ticket_id' => $ticket->id,
                        'job_item_id' => $tank['id'],
                        'name' => $tank['name'],
                        'um' => $tank['um'],
                        'qty' => $tank['qty'],
                        'price' => $price,
                        'price_extended' => $tank['qty'] * $price,
                    ];
        
                    TicketItem::create($item);
                }
            }

            $ticket->status = 1; // status 1 = tanks filled
            $ticket->save();
            
        } elseif($ticket->items->isNotEmpty() && !empty($customerType)) {
            foreach($ticket->items as $item) {
                foreach($tanks as $tank) {
                    if($tank['qty'] > 0) {
                            if(!isset($customerDiscounts[$customerType][$tank['name']]) && $tank['price'] > 0) {
                                $price = $tank['price'] - $customerDiscounts[$customerType]['default_discount'] / 100;
                            } elseif($tank['price'] < 1) {
                                $price = 0;
                            } else {
                                $price = $customerDiscounts[$customerType][$tank['name']];
                            }

                        $item = TicketItem::updateOrCreate([
                            'ticket_id' => $ticket->id,
                            'job_item_id' => $tank['id'],
                            'name' => $tank['name'],
                            'um' => $tank['um'],
                        ]);
            
                        $item->qty = $tank['qty'];
                        $item->price = $price;
                        $item->price_extended = $item->qty * $price;
            
                        $item->save();
                    }
                }
            }

            $ticket->status = 1; // status 1 = tanks filled
            $ticket->save();
        }

        if (request()->expectsJson()) {
            return [
                'redirect' => route('tickets.open'),
                'message' => 'The ticket has been updated!',
            ];
        } else {
            return $ticket;
        }
    }

    /**
     * Change a ticket status.
     *
     * @param Ticket $ticket
     * @param Request $request
     * @return array
     */
    public function changeStatus(Ticket $ticket, Request $request)
    {
        $request->validate([
            'status' => 'integer|between:0,3'
        ]);

        $ticket->status = $request->status;
        $ticket->save();

        $ticket = $ticket->fresh();

        $redirect = $ticket->status == 2 ? 'tickets.submitted' : 'tickets.cleared';
        
        switch ($ticket->status) {
            case '1':
                $message = 'This ticket was submitted!';
                break;
            
            case '2':
                $message = 'This ticket was marked as paid';
                break;
            
            case '3':
                $message = 'This ticket was closed!';
                break;
            
            default:
                $message = 'This ticket was opened!';
                break;
        }

        return [
            'ticket' =>$ticket->fresh(),
            'message' => $message,
            'redirect' => route($redirect)
        ];
    }

    /**
     * Get the status for the specified ticket.
     *
     * @param Ticket $ticket
     * @return string
     */
    public function getStatus(Ticket $ticket)
    {
        $status = $ticket->status;

        return $this->getStatusName($status);
    }

    /**
     * List view for the open tickets.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getOpenTickets()
    {
        $tickets = Ticket::where('status', 0)->latest()->get();

        return view('tickets.open', compact('tickets'));
    }

    /**
     * List view for the submitted tickets.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getSubmittedTickets()
    {
        $tickets = Ticket::where('status', 1)->latest()->get();

        return view('tickets.submitted', compact('tickets'));
    }

    /**
     * List view for the cleared tickets.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClearedTickets()
    {
        $tickets = Ticket::where('status', 2)->latest()->get();

        return view('tickets.cleared', compact('tickets'));
    }

    /**
     * List view for the closed tickets.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClosedTickets()
    {
        $tickets = Ticket::where('status', 3)->latest()->take(250)->get();

        return view('tickets.closed', compact('tickets'));
    }

    /**
     * Get customer types.
     *
     * @return array
     */
    public function getCustomerTypes()
    {   
        return config('tickets.customer_types');
    }

    /**
     * Get ticket activity.
     *
     * @param Ticket $ticket
     * @return json
     */
    public function getTicketActivity(Ticket $ticket)
    {
        $activities = $ticket->activity;
        
        $results = [];
        
        foreach($activities as $activity) {
            $user = User::find($activity->user_id);

            if($activity->type == 'created_ticket') {
                $data = [
                    'type' => 'created_ticket',
                    'user_name' => $user->name,
                    'date' => $activity->created_at->format('m-d-Y h:ia')
                ];

                array_push($results, $data);
            }

            if($activity->type == 'updated_ticket') {
                $data = [
                    'type' => 'updated_ticket',
                    'user_name' => $user->name,
                    'new_value' => $this->getStatusName($activity->new_value),
                    'old_value' => $this->getStatusName($activity->old_value),
                    'date' => $activity->created_at->format('m-d-Y h:ia')
                ];

                array_push($results, $data);
            }
        }

        return $results;
    }

    /**
     * Return ticket status by name.
     *
     * @param int $status
     * @return void
     */
    private function getStatusName($status) {
        if($status == 0) {
            return 'Open';
        } elseif($status == 1) {
            return 'Submitted';
        } elseif($status == 2) {
            return 'Cleared';
        } elseif($status == 3) {
            return 'Closed';
        }
    }
}
