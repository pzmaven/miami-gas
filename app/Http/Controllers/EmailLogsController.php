<?php

namespace App\Http\Controllers;

use DB;
use App\EmailLog;
use Illuminate\Http\Request;

class EmailLogsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $logs = EmailLog::latest()->get();

        return view('emails.list', compact('logs'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return array
     */
    public function destroy($id)
    {
        $emailLog = EmailLog::findOrFail($id);

        $emailLog->delete();

        return [
            'redirect' => route('emails.logs'),
            'message' => 'The email log was deleted!'
        ];
    }
}
