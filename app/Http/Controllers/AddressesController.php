<?php

namespace App\Http\Controllers;

use App\Address;
use App\Http\Requests\AddressFormRequest;
use Illuminate\Http\Request;

class AddressesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('read_addresses');

        $addresses = Address::latest()->get();

        return view('addresses.list', compact('addresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_addresses');

        return view('addresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(AddressFormRequest $request)
    {
        $this->authorize('create_addresses');

        $address = Address::create([
            'contact_id' => $request->contact_id,
            'street_address' => $request->street_address,
            'suite_no' => $request->suite_no,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
        ]);

        if (request()->expectsJson()) {
            return $address->fresh();
        }

        return redirect('/addresses')
            ->with('flash', 'A new address has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Address $address
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Address $address)
    {
        $this->authorize('read_addresses');

        return view('addresses.show', compact('address'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Address $address
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Address $address)
    {
        $this->authorize('update_addresses');

        return view('addresses.edit', compact('address'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Address $address
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(AddressFormRequest $request, Address $address)
    {
        $this->authorize('update_addresses');

        $address->update([
            'contact_id' => $request->contact_id,
            'street_address' => $request->street_address,
            'suite_no' => $request->suite_no,
            'city' => $request->city,
            'state' => $request->state,
            'zip' => $request->zip,
        ]);

        if (request()->expectsJson()) {
            return $address->fresh();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Address $address
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Address $address)
    {
        $this->authorize('delete_addresses');

        $address->delete();

        return [
            'redirect' => route('addresses.index'),
            'message' => 'The address was deleted!'
        ];
    }

    /**
     * Search addresses by street address or contact first and last name.
     *
     * @param string $string
     * @return \Illuminate\Http\JsonResponse
     */
    public function search($string)
    {
        $addresses = Address::where('street_address', 'LIKE', '%' . $string . '%')
            ->orWhere('suite_no', 'LIKE', '%' . $string . '%')
            ->orWhereHas('contact', function ($query) use ($string) {
                $query->where(\DB::raw('concat(first_name," ",last_name)') , 'LIKE' , '%' . $string . '%');
            })->take(5)->get();

        return response()->json($addresses);
    }
}
