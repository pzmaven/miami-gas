<?php

namespace App\Http\Controllers;

use App\Services\GetFilesNames;
use Illuminate\Http\Request;

class AttachmentsController extends Controller
{
    /**
     * @var GetFilesNames $getFilesNames
     */
    private $getFilesNames;

    /**
     * EstimatesController constructor.
     *
     * @param GetFilesNames $getFilesNames
     */
    public function __construct(GetFilesNames $getFilesNames)
    {
        $this->getFilesNames = $getFilesNames;
    }

    /**
     * Get a list of all directories inside shared disk.
     * @return array
     */
    public function directories()
    {
        $folders = \Storage::disk('shared')->allDirectories();

        // Remove the thumbs folder (if it exists) from the array
        if(in_array('thumbs', $folders)) {
            unset($folders[array_search('thumbs', $folders)]);

            foreach ($folders as $key => $value) {
                if(strpos($value, '/thumbs') > 0) {
                    unset($folders[$key]);
                }
            }
        }

        return $folders;
    }

    /**
     * Get a list of all the files inside a given directory.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function files(Request $request)
    {
        return $this->getFilesNames->from($request->directory);
    }
}
