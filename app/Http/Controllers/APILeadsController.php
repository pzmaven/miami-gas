<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
use App\Address;
use App\Lead;
use App\Services\Cargas\CargasAPI;
use Illuminate\Support\Facades\Mail;

class APILeadsController extends Controller
{
    /**
     * Create request.
     * @param Request $request
     */
    public function createRequest(Request $request)
    {
	    


		
			//Require first name or last name to insert anything at all
			if(isset($request->clientfirstname) || isset($request->clientlastname)){
			
			//Prepare for CargasAPI
			
			if(isset($request->clientunit)){$request->clientaddress = $request->clientaddress." #".$request->clientunit ; }
			
			
			$inputs = array(
	          'firstname' => $request->clientfirstname,
	          'lastname' => $request->clientlastname,
	          'email' => $request->clientemail,
	          'phone' => $request->clientphone,
	          'customertype' => 'Residential',
	          'referral' => $request->referralsource,
	          'salesperson' => 'Joel Almanzar',
	          'clientaddress' => $request->clientaddress,
	          'clientcity' => $request->clientcity,
	          'clientstate' => $request->clientstate,
	          'clientzip' => $request->clientzip
	      );
	      
	      
	     
	      
	      //Add Contact to Cargas
		  $cargasApi = new CargasApi ();
		  $customers = $cargasApi->create_customer($inputs);
		  
		  
		   
		Mail::send(['html'=>'emails.blank'], $customers, function($message) {
         $message->to('joel@miamigas.com', 'DEBUG')->subject
            ('DEBUG FROM LEADS CONTROLLER');
         $message->from('system@miamigas.com','Miami Gas');
      	});




      
	      $contact = Contact::create([
	          'first_name' => $request->clientfirstname,
	          'last_name' => $request->clientlastname,
	          'email' => $request->clientemail,
	          'phone' => $request->clientphone,
	          'company' => $request->clientcompanyname, // make sure to change this on the Wordpress side
	      ]);

		//Require address to insert address

	       if(isset($request->clientaddress)){
	       $address = Address::create([
		           'contact_id' => $contact->id,
		           'street_address' => $request->clientaddress,
		           'suite_no' => $request->clientunit,
		           'city' => $request->clientcity,
		           'state' => $request->clientstate,
		           'zip' => $request->clientzip,
		       ]);
		   };

		//Require contact_id and address_id to add a lead

		   if(isset($contact->id) && isset($address->id)){
		       $lead = Lead::create([
		           'contact_id' => $contact->id,
		           'address_id' => $address->id,
		           'interest' => $request->clientservicerequested,
		           'lead_notes' => $request->clientnotes,
		           'ref'=>$request->clientrep
		       ]);

		       // also add the lead's first note to its related contact
		       $contact->addNote($request->clientnotes);
			}
			
		
			
			
			
			
		}
       echo  response()->json($contact, 201);
    }
}
