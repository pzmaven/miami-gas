<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Cargas\CargasAPI;

class CargasController extends Controller
{
    
	public function index(Request $request)
    {
        
		$cargasApi = new CargasApi ();
	    $inventory = $cargasApi->inventory($request);
	    //$this->authorize('cargas'); 
		return view('cargas.index', compact('inventory'));
    }
    
    public function inventory(Request $request)
    {
        
		$cargasApi = new CargasApi ();
	    $inventory = $cargasApi->inventory($request);
	    //$this->authorize('cargas'); 
		return view('cargas.inventory', compact('inventory'));
    }
    
    public function customers(Request $request)
    {
        
		$cargasApi = new CargasApi ();
	    $customers = $cargasApi->customers($request);
	    //$this->authorize('cargas'); 
		return view('cargas.customers');
    }
	    
	    
	    
	    
	    
	    
	    
	    
	    
    
    
    
}
