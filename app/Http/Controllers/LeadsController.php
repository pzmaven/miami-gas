<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadsFormRequest;
use App\Lead;
use App\LeadActivity;
use App\Tag;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LeadsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request, Lead $lead)
    {
        $leads = $lead->newQuery();

        if($request->has('bucket')) {
            if($request->bucket == '0') {
                $leads->where('bucket', '>', 0);
            } else {
                $leads->where('bucket', $request->bucket);
            }
        }

        if($request->has('assignee')) {
            if($request->assignee != 0) {
                $leads->whereHas('assignees', function ($query) use ($request) {
                    $query->where('lead_user.user_id', $request->assignee);
                });
            }
        }

        if($request->has('sort_by')) {
            if($request->sort_by == 'name') {
                $leads->select('leads.*')
                    ->leftJoin('contacts', 'leads.contact_id', '=', 'contacts.id')
                    ->orderBy('contacts.last_name', $request->sort_order);
            } else {
                $leads->orderBy($request->sort_by, $request->sort_order);
            }

        }

        $leads = $leads->latest()->get();
        $buckets = config('leads.buckets');
        $users = User::all();

        return view('leads.list', [
            'leads' => $leads,
            'buckets' => $buckets,
            'users' => $users,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param Lead $lead
     * @return \Illuminate\View\View
     */
    public function show(Lead $lead)
    {
        $activities = $lead->activities;

        foreach ($activities as $activity) {
            if(Carbon::now()->startOfDay()->equalTo(Carbon::parse($activity->date))) {
                $activity->class_name = 'due-today';
            } else if(Carbon::now()->startOfDay()->greaterThan(Carbon::parse($activity->date))) {
                $activity->class_name = 'expired';
            }
        }

        return view('leads.show', compact('lead'));
    }

    /**
     * Show the form for creating a new resource
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_contacts');

        return view('leads.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LeadsFormRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(LeadsFormRequest $request)
    {
        $this->authorize('create_contacts');

        $tags = [];

        if(!empty($request->tags)) {
            $tags = explode(',', $request->tags);

            foreach ($tags as $key => $tag) {
                $tags[$key] = ucwords($tag);
            }
        }

        $lead = Lead::create([
            'address_id' => $request->address_id,
            'contact_id' => $request->contact_id,
            'bucket' => $request->bucket,
            'priority' => $request->priority,
            'lead_notes' => $request->lead_notes,
            'tags' => $tags,
        ]);

        return redirect($lead->path())
            ->with('flash', 'A new lead has been created!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Lead $lead
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Lead $lead)
    {
        return view('leads.edit', compact('lead'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Lead $lead
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Lead $lead, Request $request)
    {
        $buckets = config('leads.buckets');
        $priorities = config('leads.priorities');

        if(!array_key_exists($request->priority, $priorities)) {
            $request->offsetUnset('priority');
        }

        if(!array_key_exists($request->bucket, $buckets)) {
            $request->offsetUnset('bucket');
        }

        $request->validate([
            'bucket' => 'required',
            'priority' => 'required',
        ]);

        $tags = [];

        if(!empty($request->tags)) {
            $tags = explode(',', $request->tags);

            foreach ($tags as $key => $tag) {
                $tags[$key] = ucwords($tag);
            }
        }

        $lead->update([
            'bucket' => $request->bucket,
            'priority' => $request->priority,
            'lead_notes' => $request->lead_notes,
            'tags' => $tags
        ]);

        return redirect($lead->path())->with('flash', 'The lead has been updated!');
    }

    /**
     * Add a lead activity.
     *
     * @param Lead $lead
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addActivity(Lead $lead, Request $request)
    {
        $request->validate([
            'type' => 'required',
            'bucket' => 'required',
            'date' => 'required',
            'time' => 'required',
            'notes' => 'required'
        ]);

        $activity = LeadActivity::create([
            'user_id' => auth()->id(),
            'lead_id' => $lead->id,
            'type' => $request->type,
            'bucket' => $request->bucket,
            'date' => date('Y-m-d', strtotime($request->date)),
            'time' => $request->time,
            'notes' => $request->notes,
        ]);

        if($request->change_bucket) {
            $activity->lead()->update([
                'bucket' => $request->bucket,
            ]);
        }

        return response()->json([
            'message' => 'A new activity was added',
            'redirect' => $lead->path(),
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Lead $lead
     * @return array
     * @throws \Exception
     */
    public function destroy(Lead $lead)
    {
        $lead->delete();

        return [
            'redirect' => route('leads.index'),
            'message' => 'Your Lead was deleted!'
        ];
    }

    /**
     * Assign lead to user.
     *
     * @param Lead $lead
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function assign(Lead $lead, User $user)
    {
        try {
            $user->assignLeads([$lead->id]);
        } catch (\Exception $e) {
            abort(422, 'Something went wrong');
        }

        return response()->json([
            'message' => 'The lead was assigned to ' . $user->name,
            'redirect' => route('leads.index'),
        ], 200);
    }
}
