<?php

namespace App\Http\Controllers;

use App\Appointment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Contact;
use App\User;
use App\Mail\SendNewServiceRequestNotificationToTeam;
use Illuminate\Support\Facades\Mail;
use Spatie\CalendarLinks\Link;
use App\Mail\SendCalendarInvite;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('read_appointments');

        $appointments = Appointment::where('type', '!=', 'Service Request')->latest()->get();

        return view('appointments.list', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_appointments');

        return view('appointments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create_appointments');

        $date = Carbon::createFromFormat('m-d-Y', $request->date)->format('Y-m-d');

        $appointment = Appointment::create([
            'address_id' => $request->address_id,
            'user_id' => $request->user_id,
            'type' => $request->type,
            'urgency' => $request->urgency,
            'status' => 'Scheduled',
            'date' => $date,
            'time' => $request->time,
        ]);

        if($request->invite) {
            $this->sendCalendarInvite($appointment);
        }

        return redirect('/appointments')
            ->with('flash', 'A new appointment has been created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(Appointment $appointment)
    {
        $this->authorize('read_appointments');

        return view('appointments.show', compact('appointment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Appointment $appointment)
    {
        $this->authorize('update_appointments');

        return view('appointments.edit', compact('appointment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, Appointment $appointment)
    {
        $this->authorize('update_appointments');

        $date = Carbon::createFromFormat('m-d-Y', $request->date)->format('Y-m-d');

        $appointment->update([
            'address_id' => $request->address_id,
            'user_id' => $request->user_id,
            'type' => $request->type,
            'urgency' => $request->urgency,
            'status' => $request->status,
            'date' => $date,
            'time' => $request->time,
        ]);

        if($appointment->wasChanged('date') || $appointment->wasChanged('time')) {
            $message = 'Change detected! Remember to resend a calendar invite!';
        } else {
            $message = 'The appointment has been updated!';
        }

        return redirect('/appointments/' . $appointment->id)
            ->with('flash', $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Appointment $appointment)
    {
        $this->authorize('delete_appointments');

        $appointment->delete();

        return [
            'redirect' => route('appointments.index'),
            'message' => 'The appointment was deleted!'
        ];
    }

    /**
     * Get all service requests.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllServiceRequests()
    {
        $requests = Appointment::where('type', 'Service Request')
            ->latest()
            ->get();

        return view('appointments.requests', compact('requests'));
    }

    /**
     * Service request form.
     *
     * @return \Illuminate\Http\Response
     */
    public function getServiceRequest()
    {
        return view('appointments.request');
    }

    /**
     * Store the service request into the database.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postServiceRequest(Request $request)
    {
        try {
            $appointment = Appointment::create([
                'address_id' => $request->address_id,
                'user_id' => 1,
                'type' => 'Service Request',
                'urgency' => 'High',
                'status' => 'Requested',
            ]);
    
            $appointment->notes()->create([
                'user_id' => 1, // the id 1 is reserved for the superadmin user called "System"
                'body' => 'Client says: ' . $request->message,
                'privacy' => 'Public'
            ]);
    
            Mail::to('delivery@miamigas.com')->send(
                new SendNewServiceRequestNotificationToTeam($appointment)
            ); 
        } catch (\Exception $e) {
            $response = [
                'message' => $e->getMessage(),
            ];
    
            $status = 404;
    
            return response()->json($response, $status);
        }

        $response = [
            'message' => 'Thank you, your request has been successfully submitted to Miami Gas, you can expect a call from us to confirm a date and time within 24 hours.',
        ];

        $status = 200;

        return response()->json($response, $status);
    }

    /**
     * Check if the entered phone number exists in the database.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyPhone(Request $request)
    {
        $phone = preg_replace('/[^0-9]/', '', $request->phone);

        // if the number contains the country prefix "+1", we remove it to match the 10-digit format.
        $length = strlen($phone);
        if ($length == 11 && substr($phone, 0, 1) == '1'){
            $phone = substr($phone, 1, 10);
        }
        
        $contacts = Contact::all();

        foreach($contacts as $contact) {
            if($contact->getPhoneNumberAttribute() == $phone) {

                $response = [
                    'contact' => $contact,
                ];
        
                if(count($contact->addresses) > 1) {
                    $response['message'] = 'We found more than one address associated with this account. Please select the address you are requesting service for.';
                } else {
                    $response['message'] = 'Is this the correct address?';
                } 
        
                $status = 200;
        
                return response()->json($response, $status);
            } else {
                $response = [
                    'message' => 'This phone number does not exist in our records',
                ];
        
                $status = 404;
            }
        }
        
        return response()->json($response, $status);
    }

    /**
     * Send calendar invite route for ajax calls.
     *
     * @param Appointment $appointment
     * @return \Illuminate\Http\JsonResponse
     * @throws \Spatie\CalendarLinks\Exceptions\InvalidLink
     */
    public function calendarInvite(Appointment $appointment)
    {
        $this->sendCalendarInvite($appointment);

        $status = 200;
        $response = [
            'message' => 'Calendar invite sent successfully!',
        ];

        return response()->json($response, $status);
    }

    /**
     * Send calendar invitation for the appointment.
     *
     * @param $appointment
     * @throws \Spatie\CalendarLinks\Exceptions\InvalidLink
     */
    private function sendCalendarInvite($appointment)
    {
        $title = $appointment->type . ' appt - Miami Gas';
        $description = $appointment->type . ' Appointment @ ' . $appointment->address->contact->name . ', ' . $appointment->address->street_address . ', ' . $appointment->address->contact->phone . ', ' . $appointment->address->contact->email;
        $date = $appointment->date . ' ' . $appointment->time;

        $from = Carbon::createFromFormat('Y-m-d H:i:s', $date);
        $to = Carbon::createFromFormat('Y-m-d H:i:s', $date)->addMinutes(30);

        $link = Link::create($title, $from, $to)
            ->description($description)
            ->address($appointment->address->fullAddress);

        $invitation = $link->ics();

        // prepare invitation for attachment file
        $invitation = str_replace('%0d%0a', PHP_EOL, $invitation);
        $invitation = str_replace('data:text/calendar;charset=utf8,', '', $invitation);

        Mail::to('calendar@miamigas.com')
            ->cc($appointment->user->email)
            ->send(
                new SendCalendarInvite($appointment, $invitation)
            );
    }
}
