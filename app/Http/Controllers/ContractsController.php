<?php

namespace App\Http\Controllers;

use App\Address;
use App\Agreement;
use App\Contract;
use App\ContractServices;
use App\ContractCancellationFees;
use App\Mail\SendContractPdfToClient;
use App\Mail\SendContractDownloadLinkToClient;
use App\Mail\SendNewContractNotificationToTeam;
use App\Mail\SendReviewContractLinkToClient;
use App\Http\Requests\ContractDataRequest;
use App\Services\ContractDataTransformer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use PDF;

class ContractsController extends Controller
{
    /**
     * @var ContractDataTransformer $contractDataTransformer
     */
    private $contractDataTransformer;

    /**
     * ContractsController constructor.
     *
     * @param ContractDataTransformer $contractDataTransformer
     */
    public function __construct(ContractDataTransformer $contractDataTransformer)
    {
        $this->contractDataTransformer = $contractDataTransformer;
    }

    /**
     * Get all contracts.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $contracts = Contract::latest()->get();

        return view('contracts.list', compact('contracts'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Contract  $contract
     * @return \Illuminate\Http\RedirectResponse
     */
    public function show(Contract $contract)
    {
        return redirect()->route('contracts.sign', [
            $contract->id, $contract->status->hash, 'employee'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Contract  $contract
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Contract $contract)
    {
        $this->authorize('delete_contracts');

        $contract->delete();

        return [
            'redirect' => route('contracts.index'),
            'message' => 'The contract was deleted!'
        ];
    }

    /**
     * Select the base template for the contract.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function selectBase()
    {
        $base = request()->base;
        $id = request()->id;
        $templates = config('contracts.templates');

        if(!empty($base)) {
            if(!in_array($base, array_keys($templates))) {
                abort(404);
            }

            return $this->showContractForm($id);
        } else {
            $templates = config('contracts.templates');
            $model = current(explode('/', request()->decodedPath()));

            return view('contracts.select_base')->with([
                'templates' => $templates,
                'model' => $model
            ]);
        }
    }

    /**
     * Contract initial form.
     *
     * @param $id
     * @return \Illuminate\View\View
     */
    public function showContractForm($id)
    {
        $data = $this->contractDataTransformer->getContractFormData($id);
        $data['base'] = request()->base;

        return view('contracts.form', compact('data'));
    }

    /**
     * Configure contract services.
     *
     * @param ContractDataRequest $request
     * @return \Illuminate\View\View
     */
    public function configureServices(ContractDataRequest $request)
    {
        $contractData = $request->all();

        $this->contractDataTransformer
            ->saveContractServicesInSession($contractData, $request->services_qty, $request->session());

        $services = session('services');
        $cancellationFees = config('contract_' . $contractData['base'] . '.cancellation_fees');
        $request->session()->put('cancellationFees', $cancellationFees);

        $contractDate = $this->getContractDateArray($contractData['contract_date'], 'm-d-Y');
        $request->session()->put('contractDate', $contractDate);

        return view('contracts.services_configuration', compact('contractData', 'services', 'cancellationFees'));
    }

    /**
     * Get contract date and return it as an array
     * @param  string $date
     * @return array
     */
    public function getContractDateArray($date, $format)
    {
        $date = Carbon::createFromFormat($format, $date);

        return [
            'day' => $date->format('jS'),
            'month' => $date->format('F'),
            'year' => $date->year
        ];
    }

    /**
     * Show the contract and the signature pad to the client.
     *
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function showFilledContractToClient(Request $request)
    {
        $servicesPrices = $request->services_prices;
        $cancellationPrices = $request->cancellation_fees;

        $this->contractDataTransformer
            ->savePricesOfContractServicesInSession($servicesPrices, $request->session());

        $this->contractDataTransformer
            ->saveCancellationFeesInSession($cancellationPrices, $request->session());

        if($request->submit == 'sendEmail') {
            $viewName = 'contracts.contract_filled_email';
        } else if($request->submit == 'signNow') {
            $viewName = 'contracts.contract_filled_client';
        }

        return view($viewName, [
	        'contractData' => session('contractData'),
            'contractDate' => session('contractDate'),
            'services' => session('services'),
            'propanePrice' => session('propanePrice'),
            'pipingReimbursementPrice' => session('cancellationFees.charges.piping_reimbursement.amount'),
            'cancellationFeePrice' => session('cancellationFees.charges.cancellation_fee.amount'),
            'pdfable' => false,
        ]);
    }

    /**
     * Generate contract and send it to the client via e-mail.
     *
     * @return \Illuminate\Http\RedirectResponse|array
     */
    public function sendContractViaEmail(Request $request)
    {
        $emails = $request->emails;
        $emailBody = $request->emailBody;
        $filesNames = $request->filesNames;
        $user = auth()->user();

        $contractData = session('contractData');
        $services = session('services');
        $contractDate = session('contractDate');
        $cancellationFees = session('cancellationFees');
        $propanePrice = session('propanePrice');
        $pipingReimbursementPrice = session('cancellationFees.charges.piping_reimbursement.amount');
        $cancellationFeePrice = session('cancellationFees.charges.cancellation_fee.amount');

        $contract = Contract::create($contractData);

        foreach ($services as $service) {
            foreach ($service['charges'] as $charge) {
                if(!isset($service['qty'])) {
                    $service['qty'] = 1;
                }

                $contract->contractServices()->create([
                    'service_name' => $service['name'],
                    'charge_type' => $charge['name'],
                    'amount' => $charge['amount'],
                    'qty' => $service['qty'],
                ]);
            }
        }

        foreach ($cancellationFees['charges'] as $charge) {
            $contract->contractCancellationFees()->create([
                'charge_type' => $charge['name'],
                'amount' => $charge['amount'],
            ]);
        }

        $hash = $this->generateContractHash($contract);

        $contract->status()->create([
            'contract_id' => $contract->id,
            'hash' => $hash,
            'client_signed' => false,
            'company_signed' => false,
        ]);

        if(auth()->user()->hasSignature && auth()->user()->signature->auto_sign) {
            $signaturePath = $this->getSignaturePath();

            $signature['path'] = auth()->user()->signature->path;
            $signature['date'] = Carbon::now()->format('Y-m-d');

            Storage::disk('public')->copy($signature['path'], $signaturePath);

            $contract->update([
                'company_signature_name_typed' => trim(auth()->user()->name),
                'company_signature_date' => $signature['date'],
                'company_signature' => $signature['path']
            ]);

            $contract->status()->update([
                'company_signed' => true,
            ]);
        }

        $contract = $contract->fresh();
        $contract->link = $this->generateContractLink($contract->id, $hash);

        foreach ($emails as $email) {
            Mail::to($email)->send(
                new SendReviewContractLinkToClient($user, $contractData['first_name'], $emailBody, $contract->link, $filesNames)
            );
        }

        Mail::to('joel@miamigas.com')->send(
            new SendNewContractNotificationToTeam($contract)
        );

        if (request()->expectsJson()) {
            return [
                'redirect' => route('contracts.pending'),
                'message' => 'A link to the contract was sent to the client!'
            ];
        }

        return redirect('/contracts')->with('flash', 'A link to the contract was sent to the client!');;
    }

    /**
     * Resend contract link to client.
     *
     * @param Request $request
     * @return array
     */
    public function resendContractLink(Request $request)
    {
        $contractLink = $this->generateContractLink($request->contract['id'], $request->contract['status']['hash']);

        foreach ($request->emails as $email) {
            Mail::to($email)->send(
                new SendReviewContractLinkToClient(auth()->user(), $request->contract['first_name'], $request->emailBody, $contractLink)
            );
        }

        return [
            'message' => 'The contract link e-mail was resent!',
        ];
    }

    /**
     * Generate a unique contract hash.
     *
     * @param $param
     * @return string
     */
    public function generateContractHash($param)
    {
        return base64_encode(\Hash::make($param));
    }

    /**
     * Generate the full url address for the contract.
     *
     * @param $id
     * @param $hash
     * @return string
     */
    public function generateContractLink($id, $hash)
    {
        return env('APP_URL') . 'contracts/'. $id . '/sign/' . $hash;
    }

    /**
     * Contract link landing page.
     *
     * @param $id
     * @param $hash
     * @return \Illuminate\View\View
     */
    public function signContractLink($id, $hash, Request $request)
    {
        if($request->user == 'employee' && !auth()->id()) {
            $this->middleware('auth');
        }

        $contractData = Contract::findOrFail($id);

        if($contractData->status->hash != $hash) {
            abort(403, 'This action is unauthorized.');
        }

        if(!auth()->user() && $contractData->isSigned()) {
            return view('contracts.download', compact('id', 'hash'));
        }

        if(!auth()->user() && $contractData->status->client_signed) {
            return view('contracts.thankyou');
        }

        $contractDate = $this->getContractDateArray($contractData['contract_date'], 'Y-m-d');
        $contractServices = ContractServices::where('contract_id', $id)->get();

        $cancellationFee = ContractCancellationFees::where([
            ['contract_id', $id],
            ['charge_type', '=', 'Cancellation Fee']
        ])->first();

        $pipingReimbursement = ContractCancellationFees::where([
            ['contract_id', $id],
            ['charge_type', 'Piping Reimbursement']])
            ->first();

        $cancellationFeePrice = $cancellationFee->amount;
        if($pipingReimbursement) {
            $pipingReimbursementPrice = $pipingReimbursement->amount;
        }

        $propanePrice = ContractServices::where('contract_id', $id)->first()->amount;

        if($contractData->base == 'forklift') {
            $services = $this->contractDataTransformer->getForkliftContractServicesAsArray($contractServices);
        } else {
            $services = $this->contractDataTransformer->getContractServicesAsArray($contractServices);
        }

        $pdfable = false;

        return view('contracts.contract_filled_email', compact(
            'contractData',
            'services',
            'propanePrice',
            'cancellationFeePrice',
            'pipingReimbursementPrice',
            'contractDate',
            'pdfable'
        ));
    }

    /**
     * Generate the signature path.
     *
     * @return string
     */
    public function getSignaturePath()
    {
        $now = Carbon::now();

        return "signatures/" . $now->format('Y') . "_" . $now->format('m') . "/" . uniqid() . ".png";
    }

    /**
     * Save the signatures on storage.
     *
     * @param $signatureData
     * @param $signatureDate
     * @return array
     */
    public function storeSignature($signatureData, $signatureDate)
    {
        $signaturePath = $this->getSignaturePath();
        $signature = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $signatureData));

        Storage::disk('public')->put($signaturePath, $signature);

        $signatureDate = Carbon::createFromFormat('Y-m-d\TH:i:s.uP', $signatureDate)->format('Y-m-d');

        return [
            'path' => $signaturePath,
            'date' => $signatureDate
        ];
    }

    /**
     * Sign the contract via e-mail by the client.
     *
     * @param Request $request
     * @return array
     */
    public function signEmailClient(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $contract = Contract::findOrFail($request->contractId);

        $contract->update([
            'client_signature_name_typed' => trim($request->full_name_typed),
            'client_signature_date' => $signature['date'],
            'client_signature' => $signature['path']
        ]);

        $contract = $contract->fresh();

        $contract->status()->update([
            'client_signed' => true,
        ]);

        $contract->link = $this->generateContractLink($contract->id, $contract->status->hash);

        Mail::to('joel@miamigas.com')->send(
            new SendNewContractNotificationToTeam($contract)
        );

        $this->sendContractDownloadLink($contract->fresh());

        return [
            'redirect' => route('contracts.thankyou'),
            'message' => 'The contract was successfully signed!'
        ];
    }

    /**
     * Sign the contract via e-mail by the employee.
     *
     * @param Request $request
     * @return array
     */
    public function signEmailEmployee(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $contract = Contract::findOrFail($request->contractId);

        $contract->update([
            'company_signature_name_typed' => trim($request->full_name_typed),
            'company_signature_date' => $signature['date'],
            'company_signature' => $signature['path']
        ]);

        $contract->status()->update([
            'company_signed' => true,
        ]);

        $this->sendContractDownloadLink($contract->fresh());

        return [
            'redirect' => route('contracts.index'),
            'message' => 'The contract was successfully signed!'
        ];
    }

    /**
     * Sign the contract now by the client.
     *
     * @param Request $request
     */
    public function signNowClient(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $request->session()->put('contractData.client_signature_name_typed', trim($request->full_name_typed));
        $request->session()->put('contractData.client_signature_date', $signature['date']);
        $request->session()->put('contractData.client_signature', $signature['path']);
    }

    /**
     * Show the contract and the signature pad to the employee.
     *
     * @return \Illuminate\View\View
     */
    public function showFilledContractToEmployee()
    {
        if(auth()->user()->hasSignature && auth()->user()->signature->auto_sign) {
            $signaturePath = $this->getSignaturePath();

            $signature['path'] = auth()->user()->signature->path;

            Storage::disk('public')->copy($signature['path'], $signaturePath);

            $signature['date'] = session('contractData.client_signature_date');

            session()->put('contractData.company_signature_name_typed', auth()->user()->name);
            session()->put('contractData.company_signature_date', $signature['date']);
            session()->put('contractData.company_signature', $signaturePath);

            return redirect('contracts/store');
        }

        return view('contracts.contract_filled_employee', [
            'contractData' => session('contractData'),
            'contractDate' => session('contractDate'),
            'services' => session('services'),
            'cancellationFees' => session('cancellationFees'),
            'propanePrice' => session('propanePrice'),
            'pipingReimbursementPrice' => session('cancellationFees.charges.piping_reimbursement.amount'),
            'cancellationFeePrice' => session('cancellationFees.charges.cancellation_fee.amount'),
            'pdfable' => false,
        ]);
    }

    /**
     * Sign the contract now by the employee.
     *
     * @param Request $request
     */
    public function signNowEmployee(Request $request)
    {
        $signature = $this->storeSignature($request->signature, $request->signature_date);

        $request->session()->put('contractData.company_signature_name_typed', trim($request->full_name_typed));
        $request->session()->put('contractData.company_signature_date', $signature['date']);
        $request->session()->put('contractData.company_signature', $signature['path']);
    }

    /**
     * Store the contract to the database.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store()
    {
        $contractData = session('contractData');
        $services = session('services');
        $contractDate = session('contractDate');
        $cancellationFees = session('cancellationFees');
        $propanePrice = session('propanePrice');
        $pipingReimbursementPrice = session('cancellationFees.charges.piping_reimbursement.amount');
        $cancellationFeePrice = session('cancellationFees.charges.cancellation_fee.amount');

        $contract = Contract::create($contractData);
        $user = auth()->user();

        foreach ($services as $service) {
            foreach ($service['charges'] as $charge) {
                if(!isset($service['qty'])) {
                    $service['qty'] = 1;
                }

                $contract->contractServices()->create([
                    'service_name' => $service['name'],
                    'charge_type' => $charge['name'],
                    'amount' => $charge['amount'],
                    'qty' => $service['qty'],
                ]);
            }
        }

        foreach ($cancellationFees['charges'] as $charge) {
            $contract->contractCancellationFees()->create([
                'charge_type' => $charge['name'],
                'amount' => $charge['amount'],
            ]);
        }

        $hash = $this->generateContractHash($contract);

        $contract->status()->create([
            'contract_id' => $contract->id,
            'hash' => $hash,
            'client_signed' => true,
            'company_signed' => true,
        ]);

        $contractPdf = $this->generateContractPDF($contract->id);
        $contract = $contract->fresh();
        $contract->link = $this->generateContractLink($contract->id, $hash);

        Mail::to($contractData['email'])->send(
            new SendContractPdfToClient($user, $contractPdf, $contractData['first_name']));

        Mail::to('joel@miamigas.com')->send(
            new SendNewContractNotificationToTeam($contract)
        );

        return redirect('/contracts')->with('flash', 'The contract was created!');
    }

    /**
     * Generate the PDF file for the contract with the given id.
     *
     * @param $id
     * @return mixed
     */
    public function generateContractPDF($id)
    {
        $contractData = Contract::findOrFail($id);
        $contractDate['day'] = Carbon::createFromFormat('Y-m-d', $contractData['contract_date'])->format('jS');
        $contractDate['month'] = Carbon::createFromFormat('Y-m-d', $contractData['contract_date'])->format('F');
        $contractDate['year'] = Carbon::createFromFormat('Y-m-d', $contractData['contract_date'])->format('Y');

        $contractServices = ContractServices::where('contract_id', $id)->get();
        $cancellationFee = ContractCancellationFees::where([
            ['contract_id', $id],
            ['charge_type', '=', 'Cancellation Fee']
        ])->first();

        $pipingReimbursement = ContractCancellationFees::where([
            ['contract_id', $id],
            ['charge_type', 'Piping Reimbursement']])
            ->first();

        $cancellationFeePrice = $cancellationFee->amount;

        if($pipingReimbursement) {
            $pipingReimbursementPrice = $pipingReimbursement->amount;
        }

        $propanePrice = ContractServices::where('contract_id', $id)->first()->amount;

        if($contractData->base == 'forklift') {
            $services = $this->contractDataTransformer->getForkliftContractServicesAsArray($contractServices);
        } else {
            $services = $this->contractDataTransformer->getContractServicesAsArray($contractServices);
        }

        $pdfable = true;
        $contractPdf = PDF::loadView('contracts.pdf', compact(
            'contractData',
            'services',
            'propanePrice',
            'cancellationFeePrice',
            'pipingReimbursementPrice',
            'contractDate',
            'pdfable'
        ));

        return $contractPdf;
    }

    /**
     * Download the PDF file for the contract with the given id.
     *
     * @param $id
     * @return mixed
     */
    public function downloadContractPDF($id)
    {
        $contractPdf = $this->generateContractPDF($id);

        return $contractPdf->download('document-agreement-' . $id . '.pdf');
    }

    /**
     * Download the contract pdf from the public link.
     *
     * @param int $id
     * @param string $hash
     * @return mixed
     */
    public function downloadContractLink($id, $hash)
    {
        $contract = Contract::findOrFail($id);

        if($contract->status->hash != $hash) {
            abort(403, 'This action is unauthorized.');
        }

        $contractPdf = $this->generateContractPDF($id);

        return $contractPdf->download('document-agreement-' . $id . '.pdf');
    }

    /**
     * Send contract download link to the client after both parties signed it.
     *
     * @param Contract $contract
     */
    public function sendContractDownloadLink(Contract $contract)
    {
        if($contract->isSigned()) {
            $contractLink = $this->generateContractLink($contract->id, $contract->status->hash);

            Mail::to($contract->email)->send(
                new SendContractDownloadLinkToClient($contract->first_name, $contractLink)
            );
        }
    }

    /**
     * Get a list of all the pending contracts.
     *
     * @return \Illuminate\View\View
     */
    public function getPendingContracts()
    {
        $pendingContracts = Contract::pending();

        return view('contracts.pending', compact('pendingContracts'));
    }

    /**
     * Get a list of all signed contracts.
     *
     * @return \Illuminate\View\View
     */
    public function getSignedContracts()
    {
        $signedContracts = Contract::signed();

        return view('contracts.signed', compact('signedContracts'));
    }

    /**
     * Migrate existing contracts to new agreements.
     */

//    *** IMPORTANT! This was already used for migrating contracts to agreements. This should NOT be used again! ***
//
//    public function migrate()
//    {
//        $contracts = Contract::all();
//
//        foreach ($contracts as $contract) {
//            $contractServices = $contract->contractServices;
//
//            foreach ($contractServices as $service) {
//                $service->name = $service->service_name;
//                unset($service->id);
//                unset($service->contract_id);
//                unset($service->service_name);
//            }
//
//            $contractCancellationFees = $contract->contractCancellationFees;
//            $contractStatus = $contract->status;
//
//            $address = Address::where('contact_id', $contract->contact_id)->first();
//
//            // check if agreement already exists
//            $exists = Agreement::where('contact_id', '=', $contract->contact_id)
//                ->where('address_id', '=', $address->id)
//                ->where('base', '=', $contract->base)
//                ->where('created_at', '=', $contract->created_at)
//                ->exists();
//
//            if(!$exists) {
//                $data = [
//                    'contact_id' => $contract->contact_id,
//                    'address_id' => $address->id,
//                    'base' => $contract->base,
//                    'client_signature_name_typed' => $contract->client_signature_name_typed,
//                    'client_signature_date' => $contract->client_signature_date,
//                    'client_signature' => $contract->client_signature,
//                    'company_signature_name_typed' => $contract->company_signature_name_typed,
//                    'company_signature_date' => $contract->company_signature_date,
//                    'company_signature' => $contract->company_signature,
//                    'created_at' => $contract->created_at,
//                    'updated_at' => $contract->updated_at,
//                    'deleted_at' => $contract->deleted_at
//                ];
//
//                $data['options']['first_name'] = $contract->first_name;
//                $data['options']['last_name'] = $contract->last_name;
//                $data['options']['client_company_name'] = $contract->client_company_name;
//                $data['options']['client_company_address'] = $contract->client_company_address;
//                $data['options']['email'] = $contract->email;
//                $data['options']['phone'] = $contract->phone;
//                $data['options']['street_address'] = $contract->street_address;
//                $data['options']['city'] = $contract->city;
//                $data['options']['state'] = $contract->state;
//                $data['options']['zip'] = $contract->zip;
//                $data['options']['agreement_date'] = $contract->contract_date;
//                $data['options']['agreement_duration'] = $contract->contract_duration;
//                $data['options']['agreement_type'] = $contract->contract_type;
//                $data['options']['emergency_contact_name'] = $contract->emergency_contact_name;
//                $data['options']['emergency_contact_phone'] = $contract->emergency_contact_phone;
//
//                $services_prices = [];
//
//                foreach ($contractServices as $contractService) {
//                    $service_key = strtolower(str_replace(' ', '_', $contractService->name));
//                    $charge_key = strtolower(str_replace(' ', '_', $contractService->charge_type));
//
//                    if ($service_key == 'propane') {
//                        $services_prices[$service_key] = [
//                            'name' => $contractService->name,
//                            $charge_key => $contractService->amount,
//                        ];
//                    } else {
//                        $services_prices[$service_key]['name'] = $contractService->name;
//                        $services_prices[$service_key]['charges'][$charge_key]['name'] = $contractService->charge_type;
//                        $services_prices[$service_key]['charges'][$charge_key]['amount'] = $contractService->amount;
//                    }
//                }
//
//                $groupedServices = [];
//
//                // parse contract services and group them by name
//                foreach ($contractServices as $contractService) {
//                    if(array_search($contractService->name, array_column($groupedServices, 'name')) === false) {
//                        $groupedServices[] = [
//                            'amount' => $contractService->amount,
//                            'charge_type' => $contractService->charge_type,
//                            'qty' => $contractService->qty,
//                            'name' => $contractService->name,
//                        ];
//                    }
//                }
//
//                // add services to agreements options
//                $data['options']['services'] = $groupedServices;
//
//                $cancellation_fees = [];
//
//                foreach ($contractCancellationFees as $cancellationFee) {
//                    $key = strtolower(str_replace(' ', '_', $cancellationFee->charge_type));
//
//                    $cancellation_fees[$key] = $cancellationFee->amount;
//                }
//
//                $data['options']['services_prices'] = $services_prices;
//                $data['options']['cancellation_fees'] = $cancellation_fees;
//
//                $agreement = Agreement::create([
//                    'contact_id' => $data['contact_id'],
//                    'address_id' => $data['address_id'],
//                    'base' => $data['base'],
//                    'options' => json_encode($data['options']),
//                    'client_signature_name_typed' => $data['client_signature_name_typed'],
//                    'client_signature_date' => $data['client_signature_date'],
//                    'client_signature' => $data['client_signature'],
//                    'company_signature_name_typed' => $data['company_signature_name_typed'],
//                    'company_signature_date' => $data['company_signature_date'],
//                    'company_signature' => $data['company_signature'],
//                    'created_at' => $data['created_at'],
//                    'updated_at' => $data['updated_at'],
//                    'deleted_at' => $data['deleted_at']
//                ]);
//
//                $agreement->fresh()->status()->create([
//                    'agreement_id' => $agreement->id,
//                    'hash' => $contractStatus->hash,
//                    'client_signed' => $contractStatus->client_signed,
//                    'company_signed' => $contractStatus->company_signed,
//                    'created_at' => $contractStatus->created_at,
//                    'updated_at' => $contractStatus->updated_at
//                ]);
//            }
//        }
//
//        return redirect()->route('agreements.index')->with([
//            'flash' => 'Contracts migration to agreements completed'
//        ]);
//    }
}
