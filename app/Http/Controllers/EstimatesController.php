<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Estimate;
use App\EstimateItem;
use App\User;
use PDF;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEstimate;
use Carbon\Carbon;


class EstimatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('read_estimates');

        $estimates = Estimate::latest()->get();

        return view('estimates.list', compact('estimates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create()
    {
        $this->authorize('create_estimates');

        return view('estimates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create_estimates');

        $createdDate = Carbon::createFromFormat('m-d-Y', $request->date_created)->format('Y-m-d');

        $expirationDate = Carbon::createFromFormat('m-d-Y', $request->date_created)
            ->addDays($request->expires)
            ->format('Y-m-d');

        $estimate = Estimate::create([
            'address_id' => $request->address_id,
            'prefix' => $request->prefix,
            'salesperson' => $request->salesperson,
            'job_name' => $request->job_name,
            'payment_terms' => $request->payment_terms,
            'date_created' => $createdDate,
            'date_expiration' => $expirationDate,
            'creator' => $request->creator,
            'message_client' => $request->message_client,
            'message_internal' => $request->message_internal,
        ]);

        return redirect($estimate->path() . '/edit')
            ->with('flash', 'The estimate was created!');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($id)
    {
        $this->authorize('read_estimates');

        $estimate = Estimate::with('items')->find($id);

        $estimate->date_created = Carbon::createFromFormat('Y-m-d', $estimate->date_created)->format('m-d-Y');
        $estimate->date_expiration = Carbon::createFromFormat('Y-m-d', $estimate->date_expiration)->format('m-d-Y');

        $categories = $this->getCategories($estimate->items);

        return view('estimates.show', compact('estimate', 'categories'));
    }

    /**
     * Group job items by categories.
     *
     * @param $estimateItems
     * @return array
     */
    protected function getCategories($estimateItems)
    {
        $groupedItems = $estimateItems
            ->load('jobItem')
            ->groupBy('job_item_id');

        $categories = [];

        foreach($groupedItems as $items) {
            for($i=0; $i<count($items); $i++) {
                $category = $items[$i]->jobItem->type;

                if(!array_key_exists($category, $categories)) {
                    $categories[$category] = $items[$i]->price_extended;
                } else {
                    $categories[$category] += $items[$i]->price_extended;
                }
            }
        }

        return $categories;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit($id)
    {
        $this->authorize('create_estimates');

        $estimate = Estimate::with('items')->find($id);

        $estimate->date_created = Carbon::createFromFormat('Y-m-d', $estimate->date_created)->format('m-d-Y');
        $estimate->date_expiration = Carbon::createFromFormat('Y-m-d', $estimate->date_expiration)->format('m-d-Y');

        $users = User::get();

        return view('estimates.edit', compact('estimate', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Estimate $estimate
     * @return Estimate
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Estimate $estimate)
    {
        $this->authorize('update_estimates');

        $estimate->update([
            'subtotal' => request('subtotal'),
            'total' => request('total'),
            'sales_tax' => request('sales_tax')
        ]);

        $this->calculateProfit($estimate);

        if (request()->expectsJson()) {
            return $estimate;
        }
    }

    /**
     * Update estimate's columns by id.
     *
     * @param $id
     * @return mixed
     */
    public function updateByColumn($id)
    {
        $estimate = Estimate::find($id);

        $column = request('column');

        $estimate->update([
            $column => request($column)
        ]);

        if (request()->expectsJson()) {
            return $estimate->fresh();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Estimate $estimate
     * @return array
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Estimate $estimate)
    {
        $this->authorize('delete_estimates');

        $estimate->delete();

        return [
            'redirect' => route('estimates.index'),
            'message' => 'The estimate was deleted!'
        ];
    }

    /**
     * Calculate profit for the estimate and update the value in the database.
     *
     * @param Estimate $estimate
     */
    public function calculateProfit(Estimate $estimate)
    {
        $profit = $estimate->profit();

        $estimate->update([
            'profit' => $profit,
        ]);
    }

    /**
     * Generate a .pdf file for the requested estimate.
     *
     * @param Request $request
     * @return mixed
     */
    public function generatePdf(Request $request)
    {
        $id = $request->id;
        $estimate = Estimate::with('items')->find($id);

        if($request->type == 'detailed') {
            $pdf = PDF::loadView('estimates.detailedPDF', compact('estimate'));
        } else if($request->type == 'summary') {
            $categories = $this->getCategories($estimate->items);
            $pdf = PDF::loadView('estimates.summaryPDF', compact('estimate', 'categories'));
        }

        return $pdf->download('document-estimate-' . $id . '.pdf');
    }

    /**
     * Send the .pdf estimate file to the client.
     *
     * @param Request $request
     */
    public function sendEstimatePDFByEmail(Request $request)
    {
        $estimateId = $request->estimateId;
        $estimate = Estimate::with('items')->find($estimateId);
        $emails = $request->emails;
        $emailBody = $request->emailBody;
        $pdfName = 'document-estimate-' . $estimateId . '.pdf';
        $emailSubject = "Estimate#" . $estimateId . " from " . env('APP_NAME');
        $emailBodyHTML = nl2br(e($emailBody));
        $filesNames = $request->filesNames;
        $user = auth()->user();

        if($request->type == 'detailed') {
            $pdf = PDF::loadView('estimates.detailedPDF', compact('estimate'));
        } else if($request->type == 'summary') {
            $categories = $this->getCategories($estimate->items);
            $pdf = PDF::loadView('estimates.summaryPDF', compact('estimate', 'categories'));
        }

        foreach($emails as $email) {
            Mail::to($email)
            ->send(new SendEstimate($user, $emailBody, $emailBodyHTML, $pdf, $pdfName, $emailSubject, $filesNames));
        }
    }

    /**
     * Get a list of active estimates.
     *
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function active()
    {
        $this->authorize('read_estimates');

        $activeEstimates = Estimate::active();

        return view('estimates.active', compact('activeEstimates'));
    }

    /**
     * Get a list of expired estimates.
     *
     * @return \Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function expired()
    {
        $this->authorize('read_estimates');

        $expiredEstimates = Estimate::expired();

        return view('estimates.expired', compact('expiredEstimates'));
    }
}
