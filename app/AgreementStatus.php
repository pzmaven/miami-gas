<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AgreementStatus extends Model
{
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * An agreement status belongs to an agreement.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function agreement()
    {
        return $this->belongsTo(Agreement::class);
    }
}
