<?php

namespace App\Services;

use DB;
use App\EmailLog;
use Illuminate\Mail\Events\MessageSending;

class EmailLogger
{
    /**
     * Handle the event.
     *
     * @param MessageSending $event
     */
    public function handle(MessageSending $event)
    {
        $message = $event->message;

        $attachmentsNames = $this->getAttachmentsNames($message);

        EmailLog::create([
            'user_id' => auth()->id(),
            'from' => $this->formatAddressField($message, 'From'),
            'to' => $this->formatAddressField($message, 'To'),
            'cc' => $this->formatAddressField($message, 'Cc'),
            'bcc' => $this->formatAddressField($message, 'Bcc'),
            'subject' => $message->getSubject(),
            'body' => $message->getBody(),
            'headers' => (string)$message->getHeaders(),
            'attachments' => json_encode($attachmentsNames, JSON_FORCE_OBJECT),
            'created_at' => date('Y-m-d H:i:s'),
        ]);
    }

    /**
     * Get the names of the attached files.
     *
     * @param $message
     * @return array
     */
    public function getAttachmentsNames($message)
    {
        $names = [];

        foreach ($message->getChildren() as $child) {
            if($child->getContentType() != 'text/plain') {
                array_push($names, $child->getFileName());
            }
        }

        return $names;
    }

    /**
     * Format address strings for sender, to, cc, bcc.
     *
     * @param $message
     * @param $field
     * @return null|string
     */
    function formatAddressField($message, $field)
    {
        $headers = $message->getHeaders();

        if (!$headers->has($field)) {
            return null;
        }

        $mailboxes = $headers->get($field)->getFieldBodyModel();

        $strings = [];
        foreach ($mailboxes as $email => $name) {
            $mailboxStr = $email;
            if (null !== $name) {
                $mailboxStr = $name . ' <' . $mailboxStr . '>';
            }
            $strings[] = $mailboxStr;
        }
        return implode(', ', $strings);
    }
}
