<?php

namespace App\Services;

use App\DashboardReport;
use Carbon\Carbon;
use Illuminate\Support\Str;

class DashboardReports
{
    /**
     * Generate dashboard reports.
     * @return array
     * @throws \ReflectionException
     */
    public function generate()
    {
        $data = [];

        $items = 21;

        $models = ['App\Agreement', 'App\Contract', 'App\Contact', 'App\Address', 'App\Estimate'];

        foreach ($models as $model) {
            $reports = DashboardReport::ofType($model)->latest()->get()->take($items);

            $key = Str::plural(strtolower((new \ReflectionClass($model))->getShortName()));

            if(!count($reports)) {
                $data[$key] = [0];
            }

            foreach ($reports as $report) {
                if($model == 'App\Contract' || $model == 'App\Agreement') {
                    $date = Carbon::createFromFormat('Y-m-d H:i:s', $report['created_at']);

                    $data[$key][] = [
                        intval(strtotime($date->format('Y-m-d')).'000'),
                        intval($report['total'])
                    ];
                } else {
                    $data[$key][] = intval($report['total']);
                }

            }
        }

        foreach($data as $value => $key) {
            $data[$value] = array_reverse($data[$value]);
        }

        return $data;
    }
}
