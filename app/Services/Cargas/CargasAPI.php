<?php

namespace App\Services\Cargas;

use Carbon\Carbon;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CargasAPI
{
		
    /**
	 * //https://weichie.com/blog/curl-api-calls-with-php/	
     * This is an API handler 
     * used for Cargas
     * enjoy!
     */

   function callAPI($method, $call, $data, $test='test'){
	   $base_url = "https://miamigas-cargas.silverlinevps.com/cargasenergytest/API/";
	   if($test=='live'){$base_url = "https://miamigas-cargas.silverlinevps.com/cargasenergy/API/";}
	   
	   $url = $base_url.$call;
	   
		$curl = curl_init();
		switch ($method){
	      case "POST":
	         curl_setopt($curl, CURLOPT_POST, 1);
	         if ($data)
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	         break;
	      case "PUT":
	         curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
	         if ($data)
	            curl_setopt($curl, CURLOPT_POSTFIELDS, $data);			 					
	         break;
	      default:
	         if ($data)
	            $url = sprintf("%s?%s", $url, http_build_query($data));
   }
   // OPTIONS:
   curl_setopt($curl, CURLOPT_URL, $url);
 
 
 if($test=='live'){ 
	//LIVE KEY
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKey: TH5MI:X8SRSVI;36J?AL:L?02SYTCE2:=10Q81MS0BE97TJW',
      'Content-Type: application/json',	 
   ));
   } else {
	//TEST KEY
	   curl_setopt($curl, CURLOPT_HTTPHEADER, array(
      'APIKey: WB6S?;?;:7?T;DGJH018RXEY2N:6WLI8@U86@NY;V358N;8V2',
      'Content-Type: application/json',
   ));
}
   curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
   // EXECUTE:
   $result = curl_exec($curl);
   if(!$result){die("Connection Failure");}
   curl_close($curl);
   return $result;
   }



public function create_customer($input){

if(!is_object($input)){$input = (object)$input;}
if(isset($input->firstname)){	
		
		$result = "";
	    $call = "CreateCustomer";
	   
	    $contact_array =  array(
	      "UserName"  => 'Joel',
	      "FirstName"  => $input->firstname,
	      "LastName"  => $input->lastname,
	      "CustomerSince"  => '01-01-2020',
	      "CustomerType"  => $input->customertype,
	      "Division"  => 'Miami Gas',
	      "email"	=> $input->email,
	      "PhoneNumber"	=> $input->phone,
	      "ReferralSource"	=> $input->referral,
	      "SalesPerson"	=> $input->salesperson
	      
	      
	    );
	    
	    $get_data = $this->callAPI('POST', $call, json_encode($contact_array), 'live');
		$response = json_decode($get_data, true);
		$data = array('msg'=>$get_data);
		Mail::send(['html'=>'emails.blank'], $data, function($message) {
         $message->to('joel@miamigas.com', 'DEBUG')->subject
            ('DEBUG GET ID ');
         $message->from('system@miamigas.com','Miami Gas');
        });
 
         
        $result = "";
	    $call = "CreateCustomerLocation";
         
         $location_array =  array(
	      "CustomerID"=>$response['ResponseValues']['CustomerID'],
	      "Address1"=> $input->clientaddress,
	      "City"=> $input->clientcity,
	      "State"=> $input->clientstate,
	      "Zipcode"=> $input->clientzip,
	      "DegreeDayTable"=>"Miami",
	      "UserName"=>"Joel"
	    );
         
         
        $get_data = $this->callAPI('POST', $call, json_encode($location_array), 'live');
		$response = json_decode($get_data, true);
		
		$data = array('msg'=>$get_data);
		Mail::send(['html'=>'emails.blank'], $data, function($message) {
        $message->to('joel@miamigas.com', 'DEBUG')->subject
            ('DEBUG ADDRESS/LOCATION CREATION ');
        $message->from('system@miamigas.com','Miami Gas');
         });
         
         
         }
         
       
return $data;
}



 public function inventory($input){
	
 	$result = "";
    $call = "CQ_Items";

	$get_data = $this->callAPI('GET', $call, false, 'live');
	
	$response = json_decode($get_data, true);
   
    
   foreach($response['DataSet']['Table'] as $key=>$value){
	if(isset($value) && is_array($value)){   
	foreach($value as $k=>$v){
		
	}
	$item_id = $value['ItemID'];
	$item[$value['ItemID']]['name'] = $value['Item Number'];
	$item_name = $value['Item Number'];
	$location = $value['Inventory Location'];
	$balance = $value['Balance'] ;
	$item[$item_id][$location]['Balance'] = $balance;
	}
   }
   
   
   $percent_bulkplant = $input->percent_bulkplant;
   $percent_bobtail = $input->percent_bobtail;
   

   if( (!is_numeric($percent_bulkplant)) || ($percent_bulkplant > 100) || ($percent_bulkplant < 0)){$percent_bulkplant = 100;}
   if( (!is_numeric($percent_bobtail)) || ($percent_bobtail > 100) || ($percent_bobtail < 0)){$percent_bobtail = 100;}
   
   

   $bulkplant_tank_size = 30000;
   $current_bulkplant = $bulkplant_tank_size * ($percent_bulkplant/100);
    
   $bulkplant_qty = floatval($item['2']['Bulk Plant']['Balance']);
   $bulkplant_percent = ($bulkplant_qty/$bulkplant_tank_size)*100;
   
   $difference_gallons_bulkplant = number_format($current_bulkplant - $bulkplant_qty ,2);
   $difference_percent_bulkplant = number_format($percent_bulkplant - $bulkplant_percent,2);
   
   $bulkplant_qty = number_format($bulkplant_qty,2);
   $bulkplant_percent = number_format($bulkplant_percent,2);
   
   $current_bulkplant = number_format($current_bulkplant);
 
      
   

   $bobtail_tank_size = 3400;
   $current_bobtail = $bobtail_tank_size * ($percent_bobtail/100);
   
   $bobtail_qty = floatval($item['2']['Truck - Delivery - Bobtail Peterbilt']['Balance']);
   $bobtail_percent = ($bobtail_qty/$bobtail_tank_size)*100;
   
   $difference_gallons_bobtail = number_format($current_bobtail - $bobtail_qty ,2);
   $difference_percent_bobtail = number_format($percent_bobtail - $bobtail_percent,2);
   
   $bobtail_qty = number_format($bobtail_qty,2);
   $bobtail_percent = number_format($bobtail_percent,2);


   $current_bobtail = number_format($current_bobtail);
   
   
   
   
   $result .= "<h1>PROPANE</h1>";
   $result .= "PROPANE IN THE BULK PLANT (System): ".$bulkplant_qty." gallons (".$bulkplant_percent."%) <br>" ;
   if($percent_bulkplant != 100){
   $result .= "<span style='color:green;'>PROPANE IN THE BULK PLANT (Gauge reading) : ".$current_bulkplant." gallons (".$percent_bulkplant."%)<br/>" ;   
   $result .= "Difference = ".$difference_gallons_bulkplant." gallons (".$difference_percent_bulkplant.")%</span>";
   }
   
   
   $result .= "<br /><br />";
   $result .= "PROPANE IN THE BOBTAIL (System): ".$bobtail_qty." gallons (".$bobtail_percent."%)<br>" ;
   if($percent_bobtail != 100){
   $result .= "<span style='color:green;'>PROPANE IN THE BOBTAIL (Gauge reading) : ".$current_bobtail." gallons (".$percent_bobtail."%)<br/>" ;   
   $result .= "Difference = ".$difference_gallons_bobtail." gallons (".$difference_percent_bobtail.")% </span>";
	}
 
   $result .= "<br><br><hr><br><h1>TANKS</h1>";
   
   
   $exchange_item_ids = array('1','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','24','28','32','34','36','37','42','43','46','47','48','58','60',);
   $locations = array('Truck - Exchange - Mitsubishi','Truck - Exchange - Sprinter','Yard - Exchange','Truck - Misc - Toyota Tundra','Containers - Inventory Tanks', 'Yard - Recon and Repair','Yard - Inventory Tanks');
   
   
   foreach($locations as $location){
	   $result .= "<h3> ".$location." </h3>";
	   foreach ($exchange_item_ids as $k){
	   
	   if(isset($item[$k][$location]['Balance'])){
	   $balance = floatval($item[$k][$location]['Balance']);
	   if ($balance > 0){
	   $result .= $item[$k]['name']." : ".$balance;$result .= "<br/>";   
	   }
	   }
	   }
   }
   return $result;
     }




}










