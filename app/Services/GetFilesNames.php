<?php

namespace App\Services;

use Illuminate\Support\Facades\File;

class GetFilesNames
{
    /**
     * Get the files names from a specific folder.
     *
     * @param string $path
     * @return array
     */
    public function from($path)
    {
        $filesNames = [];

        $files = File::files(storage_path('files/shares/' . $path));

        foreach ($files as $file) {
            array_push($filesNames, [
                'path' => $path . '/' . $file->getFilename(),
                'name' => $file->getFilename()
            ]);
        }

        return $filesNames;
    }
}
