<?php

namespace App\Services;

use \Illuminate\Contracts\Session\Session;
use Illuminate\Database\Eloquent\Collection;

class ContractDataTransformer
{
    public function getContractServicesAsArray(Collection $contractServices): array
    {
        $i = 0;
        $j = 0;
        foreach ($contractServices as $key => $service) {
            if ($key === 0) {
                $services[$i]['name'] = $service->service_name;
                $services[$i]['qty'] = $service->qty;
                $services[$i]['charges'][$i]['name'] = $service->charge_type;
                $services[$i]['charges'][$i]['amount'] = $service->amount;
            } else {
                $prev_key = $key - 1;
                if ($service->service_name !== $contractServices[$prev_key]['service_name']) {
                    $i++;
                    $j = 0;
                }
                $services[$i]['name'] = $service->service_name;
                $services[$i]['qty'] = $service->qty;
                $services[$i]['charges'][$j]['name'] = $service->charge_type;
                $services[$i]['charges'][$j]['amount'] = $service->amount;
                $j++;
            }
        }

        return $services;
    }

    public function getForkliftContractServicesAsArray(Collection $contractServices): array
    {
        $i = 0;
        $j = 0;
        $prev_service_name = '';
        foreach ($contractServices as $key => $service) {
            if($prev_service_name != $service['service_name']) {
                $i++;
                $j = 0;

            } else {
                $j++;
            }

            $services[$i]['name'] = $service->service_name;
            $services[$i]['qty'] = $service->qty;
            $services[$i]['charges'][$j]['name'] = $service->charge_type;
            $services[$i]['charges'][$j]['amount'] = $service->amount;

            $prev_service_name = $service['service_name'];
        }

        return $services;
    }

    public function saveContractServicesInSession(array $contractData, array $services_qty, Session $session): void
    {
        $session->put('contractData', $contractData);
        $session->forget('contractData.services');
        $session->forget('contractData.services_qty');

        $session->forget('services');

        foreach ($contractData['services'] as $service) {
            $session->put('services.' . $service, config('contract_' . $contractData['base'] . '.services.' . $service));
            if ($session->exists('services.' . $service . '.qty')) {
                $session->put(
                    sprintf('services.%s.qty', $service),
                    $services_qty["$service"]
                );
            } else {
                $session->put(
                    sprintf('services.%s.qty', $service),
                    1
                );
            }
        }
        $services = session('services');

        if($contractData['base'] != 'forklift') {
            foreach ($services as $typeOfService => $service) {
                $session->forget('services.' . $typeOfService);
                for ($i = 0; $i < $service['qty']; $i++) {
                    foreach ($service['charges'] as $typeOfCharge => $charge) {
                        if ($service['qty'] > 1) {
                            $session->put(
                                sprintf('services.%s_%s.charges.%s', $typeOfService, $i, $typeOfCharge),
                                $charge
                            );
                            $session->put(
                                sprintf('services.%s_%s.name', $typeOfService, $i),
                                sprintf('%s %s', $service['name'], ++$i)
                            );
                            $i--;
                        } else {
                            $session->put(
                                sprintf('services.%s.charges.%s', $typeOfService, $typeOfCharge),
                                $charge
                            );
                            $session->put(
                                sprintf('services.%s.name', $typeOfService),
                                $service['name']
                            );
                        }
                    }
                }
            }
        } else {
            foreach ($services as $typeOfService => $service) {
                if($typeOfService == 'forklift_cylinder_cage_rental') {
                    $session->forget('services.' . $typeOfService);
                    for ($i = 0; $i < $service['qty']; $i++) {
                        foreach ($service['charges'] as $typeOfCharge => $charge) {
                            if ($service['qty'] >= 1) {
                                $session->put(
                                    sprintf('services.%s_%s.charges.%s', $typeOfService, $i, $typeOfCharge),
                                    $charge
                                );
                                $session->put(
                                    sprintf('services.%s_%s.qty', $typeOfService, $i),
                                    sprintf('%s', 1)
                                );
                                $session->put(
                                    sprintf('services.%s_%s.name', $typeOfService, $i),
                                    sprintf('%s %s', $service['name'], ++$i)
                                );

                                $i--;
                            } else {
                                $session->put(
                                    sprintf('services.%s.charges.%s', $typeOfService, $typeOfCharge),
                                    $charge
                                );
                                $session->put(
                                    sprintf('services.%s.name', $typeOfService),
                                    $service['name']
                                );
                            }
                        }
                    }
                }
            }
        }
    }

    public function savePricesOfContractServicesInSession(array $services_prices, Session $session): void
    {
        $j = 0;
        foreach ($session->get('services') as $typeOfService => $service) {
            foreach ($service['charges'] as $typeOfCharge => $charge) {
                if ($typeOfService === 'propane') {
                    $propanePrice = money_format('%.2n', $services_prices[$j]);
                    $session->put('propanePrice', $propanePrice);
                }
                $charge['amount'] = money_format('%.2n', $services_prices[$j++]);
                $session->put(
                    sprintf('services.%s.charges.%s', $typeOfService, $typeOfCharge),
                    $charge
                );
            }
        }
    }

    public function saveCancellationFeesInSession(array $cancellationPrices, Session $session): void
    {
        $j = 0;
        foreach ($session->get('cancellationFees')['charges'] as $typeOfCharge => $charge) {
            $charge['amount'] = money_format('%.2n', $cancellationPrices[$j++]);
            $session->put(
                sprintf('cancellationFees.charges.%s', $typeOfCharge),
                $charge
            );
        }
    }

    public function getContractFormData($id)
    {
        $modelName = 'App\\' . ucfirst(str_singular(current(explode("/", request()->path()))));
        $model = new $modelName();

        $item = $model->findOrFail($id);

        switch($modelName)
        {
            case 'App\Address' :
                $data = [
                    'contact_id' => $item->contact_id,
                    'estimate_id' => null,
                    'contact_first_name' => $item->contact->first_name,
                    'contact_last_name' => $item->contact->last_name,
                    'contact_email' => $item->contact->email,
                    'contact_phone' => $item->contact->phone,
                    'full_address' => $item->fullAddress,
                    'street_address' => $item->street_address,
                    'city' => $item->city,
                    'state' => $item->state,
                    'zip' => $item->zip,
                ];
                break;

            case 'App\Contact' :
                $addressId = request()->address;

                if($addressId && $item->addresses->find($addressId)) {
                    $address = $item->addresses->find($addressId);
                } else {
                    abort(404, 'This page does not exist!');
                }

                $data = [
                    'contact_id' => $item->id,
                    'estimate_id' => null,
                    'contact_first_name' => $item->first_name,
                    'contact_last_name' => $item->last_name,
                    'contact_email' => $item->email,
                    'contact_phone' => $item->phone,
                    'full_address' => $address->fullAddress,
                    'street_address' => $address->street_address,
                    'city' => $address->city,
                    'state' => $address->state,
                    'zip' => $address->zip,
                ];
                break;

            case 'App\Estimate' :
                $data = [
                    'contact_id' => $item->address->contact_id,
                    'estimate_id' => $item->id,
                    'contact_first_name' => $item->address->contact->first_name,
                    'contact_last_name' => $item->address->contact->last_name,
                    'contact_email' => $item->address->contact->email,
                    'contact_phone' => $item->address->contact->phone,
                    'full_address' => $item->address->fullAddress,
                    'street_address' => $item->address->street_address,
                    'city' => $item->address->city,
                    'state' => $item->address->state,
                    'zip' => $item->address->zip,
                ];
                break;

            case 'App\Lead' :
                $data = [
                    'contact_id' => $item->address->contact_id,
                    'estimate_id' => null,
                    'contact_first_name' => $item->address->contact->first_name,
                    'contact_last_name' => $item->address->contact->last_name,
                    'contact_email' => $item->address->contact->email,
                    'contact_phone' => $item->address->contact->phone,
                    'full_address' => $item->address->fullAddress,
                    'street_address' => $item->address->street_address,
                    'city' => $item->address->city,
                    'state' => $item->address->state,
                    'zip' => $item->address->zip,
                ];
                break;
        }

        return $data;
    }
}
