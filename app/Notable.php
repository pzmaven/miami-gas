<?php

namespace App;

use Carbon\Carbon;

trait Notable {

    /**
     * Boot the trait.
     */
    protected static function bootNotable()
    {
        if (auth()->guest()) {
            return;
        }

        static::created(function ($model) {
            $body = request()->notes;

            if($body) {
                $model->addNote($body);
            }
        });
    }

    /**
     * Fetch the notes relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function notes()
    {
        return $this->morphMany(Note::class, 'notable')->latest();
    }

    /**
     * Create a note for the model.
     */
    public function addNote($body)
    {
        return $this->notes()->create([
            'user_id' => auth()->id() ? auth()->id() : 1,
            'body' => $body,
            'privacy' => 'Public'
        ]);
    }
}
