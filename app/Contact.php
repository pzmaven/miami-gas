<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Contact extends Model
{
    use SoftDeletes, RecordsActivity, Notable, HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path', 'fullName', 'phoneNumber'];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['notes'];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
    * Boot the model.
    */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($contact) {
            $contact->addresses->each->delete();
        });
    }

    /**
    * A contact has many addresses.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function addresses()
    {
        return $this->hasMany(Address::class);
    }

    /**
    * A contact has many contracts.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    /**
    * Fetch the full name for the contact as a property.
    */
    public function getFullNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
    * Fetch the contact's phone number without the extension, as a property.
    */
    public function getPhoneNumberAttribute()
    {
        return substr(preg_replace('/[^0-9]/', '', $this->phone), 0, 10);
    }

    /**
     * Search contacts.
     *
     * @param Builder $query
     * @param $keywords
     * @return Builder
     */
    public function scopeSearch(Builder $query, $keywords)
    {
        return Contact::where(\DB::raw('concat(first_name," ",last_name)'), 'LIKE', '%' . $keywords . '%');
    }
}
