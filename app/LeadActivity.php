<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeadActivity extends Model
{
    use RecordsActivity;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['activity_name', 'bucket_name'];

    /**
     * A lead activity belongs to a lead.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function lead()
    {
        return $this->belongsTo(Lead::class);
    }

    /**
     * A lead activity belongs to a user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Fetch from config the lead's activity name as a property.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getActivityNameAttribute()
    {
        $types = config('leads.activity_types');

        if(!array_key_exists($this->type, $types)) {
            return 'No type';
        }

        return $types[$this->type];
    }

    /**
     * Fetch from config the lead's bucket name as a property.
     *
     * @return mixed
     */
    public function getBucketNameAttribute()
    {
        $buckets = config('leads.buckets');

        if(!array_key_exists($this->bucket, $buckets)) {
            return 'Unassigned';
        }

        return $buckets[$this->bucket];
    }
}
