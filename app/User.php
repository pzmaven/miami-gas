<?php

namespace App;

use App\Notifications\NewMessageReceived;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes, Notable, HasPath;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar', 'password',
    ];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path', 'isSuperAdmin', 'hasSignature'];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['roles'];

    /**
     * The attributes that should be hidden for arrays.
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
    * A user has many appointments.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /**
    * A user has many sent messages.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function sentMessages()
    {
        return $this->hasMany(Message::class, 'sender_id');
    }

    /**
    * A user has many received messages.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function receivedMessages()
    {
        return $this->hasMany(Message::class, 'receiver_id');
    }

    /**
     * Send message to receiver.
     * @param int $receiver
     * @param string $body
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function sendMessageTo($receiver, $body)
    {
        $message = $this->sentMessages()->create([
            'receiver_id' => $receiver,
            'body' => $body
        ]);

        $data = [
            'sender_id' => $message->sender_id,
            'sender_name' => $message->sender->name,
            'sender_avatar' => $message->sender->getAvatar(),
            'message_id' => $message->fresh()->id,
            'message' => $message->body
        ];

        $message->receiver->notify(new NewMessageReceived($data));

        return $message;
    }

    /**
    * Get user's activity.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function activity()
    {
        return $this->hasMany(Activity::class);
    }

    /**
     * Fetch the grouped activities for the user.
     */
    public function groupedActivities()
    {
        $groupedActivities = $this->activity->groupBy(function($activity) {
            return $activity->created_at->format('Y-m-d h:m:s');
        })->reverse();

        return $groupedActivities;
    }

    /**
    * A user belongs to many roles.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * Assign roles to the user.
     * @param array $roles
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function assignRoles($roles)
    {
        return $this->roles()->sync($roles);
    }

    /**
     * Check if the role has been assigned to the user.
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        return !! $role->intersect($this->roles)->count();
    }

    /**
     * A user can be assigned to many leads.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function leads()
    {
        return $this->belongsToMany(Lead::class);
    }

    /**
     * Assign leads to the user.
     *
     * @param array $leads
     * @return array
     */
    public function assignLeads($leads)
    {
        return $this->leads()->syncWithoutDetaching($leads);
    }

    /**
     * Check if the user is super admin.
     * @return bool
     */
    public function isSuperAdmin()
    {
        return $this->hasRole('superadmin');
    }

    /**
     * Fetch the superadmin role as a property.
     * @return bool
     */
    public function getIsSuperAdminAttribute()
    {
        return $this->isSuperAdmin();
    }

    /**
     * Check if the user created the resource.
     *
     * @param string $model
     * @return bool
     */
    public function owns($model)
    {
        return $this->id === $model->user_id;
    }

    /**
     * Get user's permissions.
     *
     * @return mixed
     */
    public function permissions()
    {
        return $permissions = \DB::table('permissions')
            ->join('permission_role', 'permission_role.permission_id', '=', 'permissions.id')
            ->join('roles', 'roles.id', '=', 'permission_role.role_id')
            ->join('role_user', 'role_user.role_id', '=', 'roles.id')
            ->select('permissions.name')
            ->where('role_user.user_id', '=', auth()->id())
            ->get();
    }

    /**
    * A user has a signature.
    * @return \Illuminate\Database\Eloquent\Relations\HasOne
    */
    public function signature()
    {
        return $this->hasOne(Signature::class);
    }

    /**
     * Check if the user has signature.
     *
     * @return bool
     */
    public function hasSignature()
    {
        return $this->signature()->exists();
    }

    /**
     * Fetch hasSignature as a property.
     * @return bool
     */
    public function getHasSignatureAttribute()
    {
        return $this->hasSignature();
    }

    /**
     * Get user's avatar path or return a default avatar.
     * @return string
     */
    public function getAvatar()
    {
        return Storage::disk('public')
            ->exists($this->avatar) ? Storage::url($this->avatar) : '/images/user.png';
    }

    /**
    * A user has many tickets.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function tickets()
    {
        return $this->hasMany(Ticket::class);
    }
}
