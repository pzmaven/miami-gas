<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Tags\HasTags;

class Lead extends Model
{
    use Notable, HasPath, RecordsActivity, HasTags;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['address', 'contact', 'tags', 'assignees'];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path', 'priority_name', 'bucket_name', 'schedule_hours', 'buckets', 'activity_types'];

    /**
    * A lead belongs to a contact.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function contact()
    {
        return $this->belongsTo(Contact::class)->withTrashed();
    }

    /**
    * A lead belongs to an address.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function address()
    {
        return $this->belongsTo(Address::class)->withTrashed();
    }

    /**
     * A lead has many schedules.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schedules()
    {
        return $this->hasMany(LeadActivity::class)
            ->where('type', '=', 1)
            ->orderBy('date', 'desc');
    }

    /**
     * A lead has many logs.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function logs()
    {
        return $this->hasMany(LeadActivity::class)
            ->where('type', '=', 2)
            ->orderBy('date', 'desc');
    }

    /**
     * A lead has many activities.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activities()
    {
        return $this->hasMany(LeadActivity::class)
            ->orderBy('date', 'desc')
            ->orderBy('time', 'desc');
    }

    /**
     * A lead belongs to many assignees (users).
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function assignees()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * Fetch from config the lead's priority name as a property.
     *
     * @return mixed
     */
    public function getPriorityNameAttribute()
    {
        $priorities = config('leads.priorities');

        if(!array_key_exists($this->priority, $priorities)) {
            return 'No priority';
        }

        return $priorities[$this->priority];
    }

    /**
     * Fetch from config the lead's bucket name as a property.
     *
     * @return mixed
     */
    public function getBucketNameAttribute()
    {
        $buckets = config('leads.buckets');

        if(!array_key_exists($this->bucket, $buckets)) {
            return 'Unassigned';
        }

        return $buckets[$this->bucket];
    }

    /**
     * Fetch from config the lead's working hours as a property.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getScheduleHoursAttribute()
    {
        return config('leads.schedule_hours');
    }

    /**
     * Fetch from config the lead's buckets as a property.
     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getBucketsAttribute()
    {
        return config('leads.buckets');
    }

    /**
     * Fetch from config the lead's activity types as a property.

     *
     * @return \Illuminate\Config\Repository|mixed
     */
    public function getActivityTypesAttribute()
    {
        return config('leads.activity_types');
    }

    /**
     * Scope a query to only include this month's leads.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeThisMonth($query)
    {
        return $query->whereMonth('created_at', \Carbon\Carbon::now()->month)->get();
    }

    /**
     * Search leads.
     *
     * @param Builder $query
     * @param $keywords
     * @return Builder
     */
    public function scopeSearch(Builder $query, $keywords)
    {
        return $query->whereHas('contact', function ($q) use ($keywords) {
            $q->where('first_name', 'LIKE', '%' . $keywords . '%')
                ->orWhere('last_name', 'LIKE', '%' . $keywords . '%');
        });
    }
}
