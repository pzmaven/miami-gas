<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\DashboardReport;

class GenerateDashboardReports extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'dashboard:reports';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate dashboard daily reports';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $models = [
            'App\Agreement',
//            'App\Contract',
            'App\Contact',
            'App\Address',
            'App\Estimate'
        ];

        foreach($models as $model) {
            $instance = new $model;

            $reports = new DashboardReport;

            $reports->create([
                'type' => $model,
                'total' => $instance->count()
            ]);
        }
    }
}
