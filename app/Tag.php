<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends \Spatie\Tags\Tag
{
    public function toArray()
    {
        $attributes = parent::toArray();

        foreach ($this->getTranslatableAttributes() as $name) {
            in_array($name, array_keys($attributes), true) ? $attributes[$name] = $this->getTranslation($name, app()->getLocale()) : '';
        }

        return $attributes;
    }
}
