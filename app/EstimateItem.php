<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EstimateItem extends Model
{
    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
    * An estimate item belongs to an estimate.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function estimate()
    {
        return $this->belongsTo(Estimate::class);
    }

    /**
    * An estimate item belongs to a job item.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function jobItem()
    {
        return $this->belongsTo(JobItem::class);
    }
}
