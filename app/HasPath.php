<?php

namespace App;

trait HasPath
{
    /**
    * Get the path for the model.
    */
    public function path()
    {
        $model = str_plural(strtolower((new \ReflectionClass($this))->getShortName()));

        return "/{$model}/{$this->id}";
    }

    /**
     * Fetch the path to the model as a property.
     */
    public function getPathAttribute()
    {
        return $this->path();
    }
}
