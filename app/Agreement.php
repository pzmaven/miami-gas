<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agreement extends Model
{
    use HasPath, RecordsActivity, SoftDeletes;

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['path', 'isSigned'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $with = ['status'];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * A service agreement belongs to a contact.
     * @return BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
     * An agreement has a status.
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function status()
    {
        return $this->hasOne(AgreementStatus::class);
    }

    /**
     * Check if the agreement is signed by both client and employee.
     *
     * @return boolean
     */
    public function isSigned()
    {
        return $this->status['client_signed'] && $this->status['company_signed'];
    }

    /**
     * Check if the agreement is initiated and signed only by the employee.
     *
     * @return boolean
     */
    public function isInitiatedAndSignedByTheEmployee()
    {
        return !$this->status['client_signed'] && $this->status['company_signed'];
    }

    /**
     * Check if the agreement is initiated and signed only by the client.
     *
     * @return boolean
     */
    public function isInitiatedAndSignedByTheClient()
    {
        return $this->status['client_signed'] && !$this->status['company_signed'];
    }

    /**
     * Check if the agreement is initiated and NOT signed by the employee or client.
     *
     * @return boolean
     */
    public function isInitiatedAndNotSigned()
    {
        return !$this->status['client_signed'] && !$this->status['company_signed'];
    }

    /**
     * Fetch the agreement's isSigned, as a property.
     *
     * @return boolean
     */
    public function getIsSignedAttribute()
    {
        return $this->isSigned();
    }

    /**
     * Scope a query to only include pending agreements.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function scopePending($query)
    {
        return $query->join('agreement_statuses', 'agreement_statuses.agreement_id', '=', 'agreements.id')
            ->select('agreements.*')
            ->where('agreement_statuses.client_signed', 0)
            ->orWhere('agreement_statuses.company_signed', 0)
            ->latest()
            ->get();
    }

    /**
     * Scope a query to only include signed agreements.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function scopeSigned($query)
    {
        return $query->join('agreement_statuses', 'agreement_statuses.agreement_id', '=', 'agreements.id')
            ->select('agreements.*')
            ->where('agreement_statuses.client_signed', 1)
            ->where('agreement_statuses.company_signed', 1)
            ->latest()
            ->get();
    }
}
