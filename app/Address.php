<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Address extends Model
{
    use SoftDeletes, RecordsActivity, Notable, HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path', 'fullAddress'];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['contact'];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
    * Boot the model.
    */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($address) {
            $address->estimates->each->delete();
        });
    }

    /**
    * An address belongs to a contact.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }

    /**
    * An address has many estimates.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function estimates()
    {
        return $this->hasMany(Estimate::class);
    }

    /**
    * An address has many appointments.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function appointments()
    {
        return $this->hasMany(Appointment::class);
    }

    /**
    * Get the complete address, including street, city, state and zip.
    */
    public function fullAddress()
    {
        $address = '';
        $address .= !empty($this->street_address) ? $this->street_address . ', ' : '';
        $address .= !empty($this->suite_no) ? 'Suite ' . $this->suite_no . ', ' : '';
        $address .= !empty($this->city) ? $this->city . ', ' : '';
        $address .= !empty($this->state) ? $this->state . ', ' : '';
        $address .= !empty($this->zip) ? $this->zip : '';

        return $address;
    }

    /**
    * Get the full address as a property.
    */
    public function getFullAddressAttribute()
    {
        return $this->fullAddress();
    }

    /**
     * Search addresses.
     *
     * @param Builder $query
     * @param $keywords
     * @return Builder
     */
    public function scopeSearch(Builder $query, $keywords)
    {
        return $query->where('street_address', 'LIKE', '%' . $keywords . '%');
    }
}
