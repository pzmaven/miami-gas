<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    use HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
    * A permission belongs to many roles.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}
