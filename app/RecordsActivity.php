<?php

namespace App;

use Carbon\Carbon;

trait RecordsActivity {

    /**
     * Boot the trait.
     */
    protected static function bootRecordsActivity()
    {
        if (auth()->guest()) {
            return;
        }

        foreach (static::getActivitiesToRecord() as $event) {
            static::$event(function($model) use ($event) {
                $model->recordActivity($event);
            });
        }
    }

    /**
     * Get all model events that will be recorded.
     *
     * @return array
     */
    protected static function getActivitiesToRecord()
    {
        return ['created', 'updated', 'deleted'];
    }

    /**
     * Record new activity for given the model.
     *
     * @param string $event
     * @throws \ReflectionException
     */
    protected function recordActivity($event)
    {
        if($event == 'updated') {
            $changed = $this->getDirty();

            foreach ($changed as $field => $newValue) {
                $oldValue = $this->getOriginal($field);

                if ($oldValue != $newValue && $field != 'updated_at') {
                    $this->activity()->create([
                        'user_id' => auth()->id(),
                        'type' => $this->getActivityType($event),
                        'field' => $field,
                        'old_value' => $oldValue,
                        'new_value' => $newValue,
                    ]);
                }
            }
        } else {
            $this->activity()->create([
                'user_id' => auth()->id(),
                'type' => $this->getActivityType($event)
            ]);
        }
    }

    /**
     * Fetch the activity relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function activity()
    {
        return $this->morphMany(\App\Activity::class, 'subject')->latest();
    }

    /**
     * Fetch the grouped activities.
     */
    public function groupedActivities()
    {
        $groupedActivities = $this->activity->groupBy(function($activity) {
            return $activity->created_at->format('Y-m-d h:m:s');
        })->reverse();

        return $groupedActivities;
    }

    /**
     * Determine the activity type.
     *
     * @param string $event
     * @return string
     * @throws \ReflectionException
     */
    protected function getActivityType($event)
    {
        return $event . '_' . strtolower((new \ReflectionClass($this))->getShortName());
    }
}
