<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Estimate extends Model
{
    use SoftDeletes, HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['address', 'user'];

    /**
     * The attributes that should be mutated to dates.
     * @var array
     */
    protected $date = ['deleted_at'];

    /**
    * An estimate has many items.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function items()
    {
        return $this->hasMany(EstimateItem::class);
    }

    /**
    * An estimate belongs to an address.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
    * An estimate belongs to a user.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class, 'salesperson');
    }

    /**
    * Calculate cost for the estimate.
    */
    public function cost()
    {
        $items = $this->items()->get();

        $cost = 0;

        foreach ($items as $item) {
            $cost += $item->jobItem->cost() * $item->qty;
        }

        return $cost;
    }

    /**
    * Fetch revenue (subtotal) for the estimate.
    */
    public function revenue()
    {
        return $this->subtotal;
    }

    /**
    * Fetch profit for the estimate.
    */
    public function profit()
    {
        return $this->revenue() - $this->cost();
    }

    /**
     * An estimate has many contracts.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contracts()
    {
        return $this->hasMany(Contract::class);
    }

    /**
     * Scope a query to only include estimates without contracts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Support\Collection
     */
    public function scopeWithoutContract($query)
    {
        return $query->doesntHave('contracts')
            ->whereMonth('created_at', '=', Carbon::now()->month)
            ->get();
    }

    /**
     * Scope a query to get active estimates.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->select()
            ->whereDate('date_expiration', '>=', Carbon::now())
            ->latest()
            ->get();
    }

    /**
     * Scope a query to get expired estimates.
     *
     * @param $query
     * @return mixed
     */
    public function scopeExpired($query)
    {
        return $query->select()
            ->whereDate('date_expiration', '<', Carbon::now())
            ->latest()
            ->get();
    }
}
