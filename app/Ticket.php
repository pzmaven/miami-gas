<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasPath, Notable, RecordsActivity;
    
    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The relations to eager load on every query.
     * @var array
     */
    protected $with = ['items', 'activity'];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
    * Boot the model.
    */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($ticket) {
            $ticket->items->each->delete();
        });
    }
    
    /**
    * A ticket belongs to a user.
    * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
    * A ticket has many items.
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
    public function items()
    {
        return $this->hasMany(TicketItem::class);
    }
}
