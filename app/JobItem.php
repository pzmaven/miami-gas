<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobItem extends Model
{
    use HasPath;

    /**
     * The attributes that aren't mass assignable.
     * @var array
     */
    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     * @var array
     */
    protected $appends = ['path'];

    /**
    * Fetch the job item cost.
    */
    public function cost()
    {
        return $this->cost;
    }
}
