<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ContractCancellationFees extends Model
{
    /**
     * Indicates if the model should be timestamped.
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = ['contract_id', 'charge_type', 'amount'];

    /**
     * A cancellation fee belongs to a contract.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }
}
