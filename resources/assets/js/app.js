/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

window.Vue = require('vue');

require('./bootstrap');

const VueInputMask = require('vue-inputmask').default;
Vue.use(VueInputMask);

Vue.prototype.$eventHub = new Vue(); // Global event bus

import Datetime from 'vue-datetime';
import 'vue-datetime/dist/vue-datetime.css';

Vue.use(Datetime);

import Vue from 'vue';
import money from 'v-money';
import Croppa from 'vue-croppa';
import 'vue-croppa/dist/vue-croppa.css'
Vue.use(Croppa);

// register directive v-money and component <money>
Vue.use(money, { decimal: ',', thousands: '.', precision: 2 });

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('addresses-list', require('./components/AddressesList.vue'));
Vue.component('agreements-list', require('./components/AgreementsList.vue'));
Vue.component('agreement-signature-pad', require('./components/AgreementSignaturePad.vue'));
Vue.component('appointments-list', require('./components/AppointmentsList.vue'));
Vue.component('autocomplete-address', require('./components/AutocompleteAddress.vue'));
Vue.component('autocomplete-contact', require('./components/AutocompleteContact.vue'));
Vue.component('autocomplete-tags', require('./components/AutocompleteTags.vue'));
Vue.component('autocomplete-user', require('./components/AutocompleteUser.vue'));
Vue.component('autocomplete-users', require('./components/AutocompleteUsers.vue'));
Vue.component('calendar-invite-modal', require('./components/CalendarInviteModal.vue'));
Vue.component('contacts-list', require('./components/ContactsList.vue'));
Vue.component('contracts-list', require('./components/ContractsList.vue'));
Vue.component('delete-button', require('./components/DeleteButton.vue'));
Vue.component('email-contract-modal', require('./components/EmailContractModal.vue'));
Vue.component('email-agreement-modal', require('./components/EmailAgreementModal.vue'));
Vue.component('email-logs-list', require('./components/EmailLogsList.vue'));
Vue.component('email-modal', require('./components/EmailModal.vue'));
Vue.component('estimate', require('./components/Estimate.vue'));
Vue.component('estimate-header', require('./components/EstimateHeader.vue'));
Vue.component('estimates-list', require('./components/EstimatesList.vue'));
Vue.component('flash', require('./components/Flash.vue'));
Vue.component('errors', require('./components/Errors.vue'));
Vue.component('jobitems-list', require('./components/JobItemsList.vue'));
Vue.component('jobitems-modal', require('./components/JobItemsModal.vue'));
Vue.component('leads-list', require('./components/LeadsList.vue'));
Vue.component('leads-activity-modal', require('./components/LeadsActivityModal.vue'));
Vue.component('messages', require('./components/Messages.vue'));
Vue.component('message-send-modal', require('./components/MessageSendModal.vue'));
Vue.component('message-user-modal', require('./components/MessageUserModal.vue'));
Vue.component('notes', require('./components/Notes.vue'));
Vue.component('notifications', require('./components/Notifications.vue'));
Vue.component('password-change', require('./components/PasswordChange.vue'));
Vue.component('permissions-list', require('./components/PermissionsList.vue'));
Vue.component('profile-picture', require('./components/ProfilePicture.vue'));
Vue.component('roles-list', require('./components/RolesList.vue'));
Vue.component('search', require('./components/Search.vue'));
Vue.component('signature', require('./components/Signature.vue'));
Vue.component('signature-pad', require('./components/SignaturePad.vue'));
Vue.component('ticket-form', require('./components/TicketForm.vue'));
Vue.component('tickets-list', require('./components/TicketsList.vue'));
Vue.component('update-address', require('./components/UpdateAddress.vue'));
Vue.component('users-list', require('./components/UsersList.vue'));
Vue.component('widget-modal', require('./components/WidgetModal.vue'));

Vue.directive('can', function(el, binding, vnode) {
    if (!App.permissions.includes(binding.value) && App.user.roles[0].name != 'superadmin') {
        vnode.elm.style.display = 'none';
    };
});

const app = new Vue({
    el: "#app"
});
