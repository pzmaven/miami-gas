@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Update Role</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Update Role Details</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form id="roles-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{ $role->path() }}">
                        {!! method_field('patch') !!}
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" name="name" value="{{ $role->name }}" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description">
                                Description <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="description" name="description" value="{{ $role->description }} "required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Permissions</label>

                            <input id="checkAll" class="form-check-input flat" type="checkbox" name="checkAll">
                            <label class="form-check-label" for="checkAll">All</label>

                            <div class="form-check col-md-6 col-sm-6 col-xs-12">
                            @foreach (\App\Permission::get() as $permission)
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input class="form-check-input flat" type="checkbox" name="permission_role[]" value="{{ $permission->id }}" id="{{ $permission->name }}" {{ $role->permissions->contains($permission->id) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="{{ $permission->name }}">
                                        {{ $permission->name }}
                                    </label>
                                </div>
                            @endforeach
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button type="submit" class="btn btn-success">Update</button>
                                @can('delete_roles')
                                    <delete-button
                                        :data="{{ $role }}"
                                        message="Are you sure you want to delete this role?"
                                        class="pull-right">
                                    </delete-button>
                                @endcan
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        // Check all permissions at once
        $(document).ready(function() {
            $('#checkAll').on('ifToggled', function(event){
                if(event.delegateTarget.checked == true) {
                    $('.form-check-input').iCheck('check');
                } else {
                    $('.form-check-input').iCheck('uncheck');
                }
            });
        });
    </script>
@endsection
