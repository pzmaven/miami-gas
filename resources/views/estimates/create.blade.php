@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add Estimate</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Form</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <form id="estimate-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{ route('estimates.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address_id">
                                Job Site <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <autocomplete-address></autocomplete-address>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="prefix">
                                Estimate Prefix
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="prefix" name="prefix" value="MG" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="salesperson">
                                Salesperson <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="salesperson" name="salesperson" required="required" class="form-control col-md-7 col-xs-12">
                                    @foreach(\App\User::get() as $user )
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
	                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="job_name">
                                Job name
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="job_name" name="job_name" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="payment_terms">
                                Payment terms <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="payment_terms" name="payment_terms" required="required" class="form-control col-md-7 col-xs-12">
                                    <option disabled>Choose</option>
                                    @foreach(config('dropdowns.payment_terms') as $term )
                                        <option value="{{ $term }}">{{ $term }}</option>
                                    @endforeach
	                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message_client">
                                Footer Notes
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="message_client" name="message_client" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="message_internal">
                                Internal Notes
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="message_internal" name="message_internal" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="creator">
                                Prepared by <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="creator" name="creator" value="{{ auth()->user()->email }}" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_created">
                                Created Date <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <datetime type="date" name="date_created" format="MM-dd-yyyy" input-class="form-control col-md-7 col-xs-12" :auto="true" required></datetime>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date_expiration">
                                Expires in <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="expires" name="expires" required="required" class="form-control col-md-7 col-xs-12">
                                    @foreach(config('dropdowns.expires') as $value => $name )
                                        <option value="{{ $value }}">{{ $name }}</option>
                                    @endforeach
	                            </select>
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
