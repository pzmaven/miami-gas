@extends('template.main')

@section('content')
<!-- page content -->
<div class="right_col" role="main">
	<div class="">
		<div class="page-title no-print">
			<div class="title_left">
				<h3>Edit Estimate</h3>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row">
			<div class="col-md-12">
				<div class="x_panel">
					<div class="x_title no-print">
						<h2>Estimate <small>details</small></h2>
						<ul class="nav navbar-right panel_toolbox">
							<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
							<li><a class="close-link"><i class="fas fa-times"></i></a></li>
						</ul>
						<div class="clearfix"></div>
					</div>
					<div class="x_content">
						<section class="content invoice">
							<!-- title row -->
					        <div class="row">
					            <img src="{{ asset('img/logo.png') }}" class="pull-right">
					        </div>

							<estimate-header :initial-estimate="{{ $estimate }}" :users="{{ $users }}"></estimate-header>

							<flash message="{{ session('flash') }}"></flash>

							<estimate :estimate="{{ $estimate }}" :job-items-type="{{ json_encode(config('dropdowns.job_items_type')) }}"></estimate>

							<email-modal :estimate="{{ $estimate }}"></email-modal>

							@include('modals.layout')

							<!-- this row will not appear when printing -->
							<div class="row no-print">
								<div class="col-xs-12">
									<button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
									<a href="{{ route('agreements.create') }}/?address={{ $estimate->address->id }}" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fas fa-file-signature"></i> Create Agreement</a>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /page content -->

@endsection

@section('css')
<style>
	/* Tables - No More Tables technique (991px is the bootstrap SM max-width) */
	.table {
		font-size: 13px;
	}

	.invisible-center {
		border: 0;
		text-align: center;
		background-color: rgba(0, 0, 0, 0);
	}

	.invisible-left {
		border: 0;
		text-align: left;
		background-color: rgba(0, 0, 0, 0);
	}

	.qty {
		width: 60px;
		font-weight: bold;
	}

	@media only screen and (max-width: 621px) {
		.table.table-no-more,
		.table.table-no-more thead,
		.table.table-no-more tbody,
		.table.table-no-more tr,
		.table.table-no-more th,
		.table.table-no-more td {
			display: block;
		}
		.table.table-no-more thead tr {
			left: -9999px;
			position: absolute;
			top: -9999px;
		}
		.table.table-no-more tr {
			border-bottom: 1px solid #DDD;
		}
		.table.table-no-more td {
			border: none;
			position: relative;
			padding-left: 50%;
			text-align: left;
			white-space: normal;
		}
		.table.table-no-more td:before {
			content: attr(data-title);
			font-weight: bold;
			left: 6px;
			padding-right: 10px;
			position: absolute;
			text-align: left;
			top: 8px;
			white-space: nowrap;
			width: 45%;
		}
		.table.table-no-more.table-bordered td {
			border-bottom: 1px solid #dedede;
		}
		.table.table-no-more.table-sm td:before {
			top: 5px;
		}
	}

	@media print {
		@page {
			margin: 1cm;
		}
		.no-print,
		.no-print * {
			display: none !important;
		}
		footer,
		.top_nav,
		.left_col {
			display: none !important;
		}
	}
</style>
@endsection
