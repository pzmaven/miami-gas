@extends('template.main')
<style>
/* Tables - No More Tables technique (991px is the bootstrap SM max-width) */
.table {
	font-size: 13px;
}

.invisible-center{
	border: 0;
	text-align: center;
	background-color: rgba(0, 0, 0, 0);
}

.invisible-left{
	border: 0;
	text-align: left;
	background-color: rgba(0, 0, 0, 0);
}

.qty {
	width: 60px;
	font-weight: bold;
}

@media only screen and (max-width: 621px) {
	.table.table-no-more,
	.table.table-no-more thead,
	.table.table-no-more tbody,
	.table.table-no-more tr,
	.table.table-no-more th,
	.table.table-no-more td {
		display: block;
	}

	.table.table-no-more thead tr {
		left: -9999px;
		position: absolute;
		top: -9999px;
	}

	.table.table-no-more tr {
		border-bottom: 1px solid #DDD;
	}

	.table.table-no-more td {
		border: none;
		position: relative;
		padding-left: 50%;
		text-align: left;
		white-space: normal;
	}

	.table.table-no-more td:before {
		content: attr(data-title);
		font-weight: bold;
		left: 6px;
		padding-right: 10px;
		position: absolute;
		text-align: left;
		top: 8px;
		white-space: nowrap;
		width: 45%;
	}

	.table.table-no-more.table-bordered td {
		border-bottom: 1px solid #dedede;
	}

	.table.table-no-more.table-sm td:before {
		top: 5px;
	}
}

@media print {
	@page {
  		margin: 1cm;
	}

	.no-print, .no-print * {
		display: none !important;
	}

	footer, .top_nav, .left_col {
		display: none !important;
	}
}
</style>
@section('content')
	<!-- page content -->
	<div class="right_col" role="main">
		<div class="">
			<div class="page-title no-print">
				<div class="title_left">
					<h3>Edit Estimate</h3>
				</div>
			</div>

			<div class="clearfix"></div>

			<div class="row">
				<div class="col-md-12">
					<div class="x_panel">
						<div class="x_title no-print">
							<h2>Estimate <small>details</small></h2>
							<ul class="nav navbar-right panel_toolbox">
								<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
								<li><a class="close-link"><i class="fas fa-times"></i></a></li>
							</ul>
							<div class="clearfix"></div>
						</div>

						<div class="x_content">
							<section class="content invoice">
								<!-- title row -->
								<div class="row">
									<img src="{{ asset('/img/logo.png') }}" class="pull-right">
								</div>
								<!-- info row -->
								<div class="row invoice-info">
									<div class="col-xs-12 col-sm-4 col-sm-push-4 invoice-col text-center">
										<div>
											<h3>Estimate</h3>
											<span>
												<h3>{{ $estimate->prefix }} #{{ $estimate->id }}</h3>
											</span>
											<h4>{{ $estimate->date_created }}</h4>
											<br><strong>Expiration Date: </strong>
											<span>{{ $estimate->date_expiration }}</span>
											<br><strong>Payment Terms: </strong>
											<span>{{ $estimate->payment_terms }}</span>
											<br><strong>Salesperson: </strong>
											<span>{{ $estimate->salesperson }}</span>
											<br><strong>Job: </strong>
											<span>{{ $estimate->job_name }}</span>
										</div>
									</div>
									<!-- /.col -->

									<div class="col-xs-6 col-sm-4 col-sm-pull-4 invoice-col">
										<div>
											<strong>Job Site</strong>
											<address>
												<p style="white-space: pre-line;">
													<strong>{{ $estimate->address->contact->fullName }}</strong>
                                                    {{ $estimate->address->street_address }}{{ $estimate->address->suite_no ? ', Suite ' . $estimate->address->suite_no : '' }}
													{{ $estimate->address->city }}, {{ $estimate->address->state }}, {{ $estimate->address->zip }}
												</p>
											</address>
										</div>
									</div>
									<!-- /.col -->

									<div class="col-xs-6 col-sm-4 invoice-col text-right">
										<div>
											<address>
												<br>2745 NW 21st Street
												<br>Miami, FL 33142
												<br>(305) 204-3333
												<br>www.miamigas.com
											</address>
										</div>
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>

							<!-- Table row -->
							<div class="row">
								<div class="col-xs-12 table">
									<table class="table table-striped table-no-more mb-0">
										<thead>
											<tr>
												<th>#</th>
												<th class="text-right">Category</th>
												<th></th>
												<th></th>
												<th></th>
												<th class="text-right">Totals</th>
											</tr>
										</thead>

										<tbody>
											@foreach($categories as $category => $total)
												<tr class="line_item_row">
													<td>{{ $loop->iteration }}</td>
													<td data-title="Item" class="text-right">{{ $category }}</td>
													<td><input class="invisible-center" type="text" disabled></td>
													<td><input class="invisible-center" type="text" disabled></td>
													<td><input class="invisible-center" type="text" disabled></td>
													<td data-title="Totals" class="text-right">${{ number_format($total, 2) }}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
								<!-- /.col -->
							</div>
							<!-- /.row -->

							<div class="row">
								<div class="col-xs-6 col-xs-push-6 col-xs-12">
									<p class="lead">Totals</p>
									<div class="table-responsive">
										<table class="table">
											<tbody>
												<tr>
													<th class="text-right">Subtotal</th>
													<td class="text-right">${{ number_format($estimate->subtotal, 2) }}</td>
												</tr>
												<tr>
													<th class="text-right">Sales Tax</th>
													<td class="text-right">${{ number_format($estimate->sales_tax, 2) }}</td>
												</tr>
												<tr>
													<th class="text-right">Total</th>
													<td class="text-right">${{ number_format($estimate->total, 2) }}</td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
								<!-- /.col -->

								<div class="col-xs-6 col-xs-pull-6 col-xs-12">
									<p class="lead">Footer notes</p>
									<span>{{ $estimate->message_client }}</span>
								</div>
								<!-- /.col -->
							</div>
							<!-- /.row -->
						</div>

						<!-- this row will not appear when printing -->
						<div class="row no-print">
							<div class="col-xs-12">
								<button class="btn btn-default pull-right" onclick="window.print();"><i class="fa fa-print"></i> Print</button>
								<a href="{{ route('agreements.create') }}/?address={{ $estimate->address->id }}" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fas fa-file-signature"></i> Create Agreement</a>
							</div>
						</div>
					</section>
				</div>
			</div>
		</div>
	</div>
	<!-- /page content -->
@endsection
