<!doctype html>

<html>
<head>
    <meta name="viewport" content="width=40, initial-scale=1">
    <meta charset="utf-8">
    <title>Miami Gas Estimate</title>
    <link href="{{ public_path('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <style>

        @page {
            margin: 1cm;
        }

        body {
            width: 19.5cm;
            xmargin: auto;
        }

        h2 {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 500;
            font-size: 30px;
        }

        h3 {
            font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            font-weight: 500;
        }

        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 1px;
            padding-right: 0px;
            padding-left: 0px;
        }

        .table > tbody > tr.emptyrow > td {
            border-top: 0;
        }

    </style>
</head>

<body>

<div class="container">

    <div class="row">
        <div class="col-xs-6">
            <div class="invoice-title">
                <br><br>
                <h2>Estimate</h2>

                <span><h3>{{ $estimate->prefix }} #{{ $estimate->id }}</h3></span>
            </div>
        </div>
        <div class="col-xs-6">
            <img src="img/logo150.png" class="pull-right">
        </div>
    </div>

    <hr class="col-xs-11">

    <div class="row">
        <div class="col-xs-6">
            <address>
                <strong><p>Job Site</p></strong>
                <p style="white-space: pre-line;">
                    {{$estimate->address->contact->fullName}}
                    {{$estimate->address->street_address}}
                    {{$estimate->address->city}}, {{$estimate->address->state}}, {{$estimate->address->zip}}
                </p>
            </address>
        </div>
        <div class="col-xs-6 text-right">
            <address>
                2745 NW 21st Street<br>
                Miami FL 33142<br>
                (305) 204-3333<br>
                www.miamigas.com
            </address>
        </div>
    </div>
    <div class="row text-center">
        <div class="col-xs-3 text-left">
            <address>
                <strong> Salesperson </strong>
                <p> {{ \App\User::find($estimate->salesperson)->name }}</p>
            </address>
        </div>
        <div class="col-xs-3 text-left">
            <address>
                <strong> Job </strong>
                <p>{{$estimate->job_name}}</p>
            </address>
        </div>
        <div class="col-xs-2">
            <address>
                <strong> Payment Terms </strong>
                <p class="text-center">{{$estimate->payment_terms}}</p>
            </address>
        </div>
        <div class="col-xs-2 text-center">
            <address>
                <strong> Created Date </strong>
                <p class="text-center">{{$estimate->date_created}}</p>
            </address>
        </div>
        <div class="col-xs-2 text-right">
            <address>
                <strong> Expiration Date </strong>
                <p class="text-center">{{$estimate->date_expiration}}</p>
            </address>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading" style="background-color: transparent;" id="el">
                    <div class="panel-title">
                        <strong>Job Summary</strong>
                    </div>
                </div>
                <div class="panel-body">
                    <div class="">
                        <table id="items" class="table table-no-more mb-0">
                            <thead>
                            <tr>
                                <th class="col-xs-2">Item</th>
                                <th class="col-xs-3">Description</th>
                                <th class="col-xs-2 text-center">Price</th>
                                <th class="col-xs-1 text-center">Quantity</th>
                                <th class="col-xs-1 text-center">UM</th>
                                <th class="col-xs-1 text-center">Totals</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($estimate->items as $item)
                                <tr class="line_item_row">
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->description_customer}}</td>
                                    <td class="text-center">{{$item->price}}</td>
                                    <td class="text-center"><strong>{{$item->qty}}</strong></td>
                                    <td class="text-center">{{$item->um}}</td>
                                    <td class="text-right">${{$item->price_extended}}</td>
                                </tr>
                            @endforeach
                            <tr class="emptyrow"> <td></td></tr>
                            <tr class="emptyrow">
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="text-center"><strong>Subtotal</strong></td>
                                <td class="text-right">${{$estimate->subtotal}}</td>
                            </tr>
                            <tr class="emptyrow">
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="text-center"><strong>Sales Tax</strong></td>
                                <td class="text-right">${{$estimate->sales_tax}}</td>
                            </tr>
                            <tr class="emptyrow">
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="emptyrow"></td>
                                <td class="text-center"><strong>Total</strong></td>
                                <td class="text-right">${{$estimate->total}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="panel-footer">
                    Footer notes: {{ $estimate->message_client }}
                </div>
            </div>
        </div>
    </div>

</div>

</body>
</html>
