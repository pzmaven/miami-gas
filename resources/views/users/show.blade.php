@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>User Information</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>User <small>details</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li><a class="close-link"><i class="fas fa-times"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-2 col-sm-12 col-xs-12 profile_left">
                            <div class="profile_img">
                                <div id="crop-avatar">
                                    <!-- Current avatar -->
                                    @if($user->id == auth()->id() || auth()->user()->isSuperAdmin() || auth()->user()->hasRole('admin'))
                                        <profile-picture :initial-user="{{ $user }}"></profile-picture>
                                    @else
                                        <img src="{{ asset($user->avatar) }}" width="150px" height="150px"/>
                                    @endif
                                </div>
                            </div>

                            <h3>{{ $user->name }}</h3>

                            <ul class="list-unstyled user_data">
                                <li>
                                    <i class="far fa-envelope user-profile-icon"></i>
                                    <a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                </li>
                            </ul>

                            @can('update_users')
                                <a href="{{ $user->path() . '/edit' }}" class="btn btn-success" data-toggle="tooltip" data-original-title="Edit User">
                                    <i class="fa fa-edit m-right-xs"></i>Edit User
                                </a>
                            @endcan

                            @if(auth()->id() != $user->id)
                                <a class="btn btn-primary" data-toggle="modal" data-target="#message-user-modal">
                                    <i class="fas fa-comment m-right-xs" data-toggle="tooltip" data-original-title="Send Message"></i>
                                </a>

                                <message-user-modal :initial-user="{{ json_encode($user) }}"></message-user-modal>
                            @endif

                        </div>
                        <div class="col-md-10 col-sm-12 col-xs-12">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#appointments" role="tab" id="appointments-tab" data-toggle="tab" aria-expanded="true">Appointments</a>
                                    </li>

                                    @if(auth()->id() == $user->id)
                                        <li role="presentation" class="">
                                            <a href="#messages" role="tab" id="messages-tab" data-toggle="tab" aria-expanded="false">Messages</a>
                                        </li>
                                    @endif

                                    <li role="presentation" class="">
                                        <a href="#notes" role="tab" id="notes-tab" data-toggle="tab" aria-expanded="false">Notes</a>
                                    </li>

                                    <li role="presentation" class="">
                                        <a href="#activity" role="tab" id="activity-tab" data-toggle="tab" aria-expanded="false">Activity Feed</a>
                                    </li>

                                    @can('update_roles')
                                        <li role="presentation" class="">
                                            <a href="#roles" role="tab" id="roles-tab" data-toggle="tab" aria-expanded="false">Roles</a>
                                        </li>
                                    @endcan

                                    @if(auth()->id() == $user->id)
                                        <li role="presentation" class="">
                                            <a href="#signature" role="tab" id="signature-tab" data-toggle="tab" aria-expanded="false">Signature</a>
                                        </li>

                                        <li role="change-password" class="">
                                            <a href="#change-password" role="tab" id="change-password-tab" data-toggle="tab" aria-expanded="false">Change Password</a>
                                        </li>
                                    @endif
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="appointments" aria-labelledby="appointments-tab">
                                        <table class="data table table-striped no-margin">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Address</th>
                                                    <th>Status</th>
                                                    <th>Urgency</th>
                                                    <th>Type</th>
                                                    <th>Date</th>
                                                    <th>Time</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                @foreach ($user->appointments as $appointment)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td><a href="{{ $appointment->path() }}">{{ $appointment->address->fullAddress }}</a></td>
                                                    <td>{{ $appointment->status }}</td>
                                                    <td>{{ $appointment->urgency }}</td>
                                                    <td>{{ $appointment->type }}</td>
                                                    <td>{{ $appointment->date }}</td>
                                                    <td>{{ $appointment->time }}</td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                    @if(auth()->id() == $user->id)
                                        <div role="tabpanel" class="tab-pane fade" id="messages" aria-labelledby="messages-tab">
                                            <messages :initial-messages="{{ $user->receivedMessages }}"></messages>
                                        </div>
                                    @endif

                                    <div role="tabpanel" class="tab-pane fade" id="notes" aria-labelledby="notes-tab">
                                        <notes :initial-notes="{{ $user->notes }}"
                                            :id="{{ $user->id }}"
                                            type="{{ get_class($user) }}">
                                        </notes>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="activity" aria-labelledby="activity-tab">
                                        <!-- start recent activity -->
                                        <ul class="messages">
                                            @foreach ($user->groupedActivities() as $items)
                                                <li>
                                                    <img src="{{ asset(auth()->user()->getAvatar()) }}" class="avatar" alt="Avatar">

                                                    <div class="message_date">
                                                        <h3 class="date text-info">{{ date('d', strtotime($items[0]->created_at)) }}</h3>
                                                        <p class="month">{{ date('M', strtotime($items[0]->created_at)) }}</p>
                                                    </div>

                                                    <div class="message_wrapper">
                                                        <h4 class="heading">{{ $items[0]->user->name }}</h4>
                                                        @foreach ($items as $activity)
                                                            @if($activity->field)
                                                                @if($activity->field == "address_id")
                                                                <blockquote class="message">Changed address from "{{ \App\Address::find($activity->old_value)->street_address }}" to "{{ \App\Address::find($activity->new_value)->street_address }}"</blockquote>
                                                                @elseif($activity->field == "user_id")
                                                                    <blockquote class="message">Changed assignee from "{{ \App\User::find($activity->old_value)->name }}" to "{{ \App\User::find($activity->new_value)->name }}"</blockquote>
                                                                @elseif($activity->field == "time")
                                                                	<blockquote class="message">Changed Time from {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->old_value)->format('h:ia') }} to {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->new_value)->format('h:ia') }}</blockquote>
                                                                @elseif($activity->field == "date")
                                                                	<blockquote class="message">Changed appointment date from {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->old_value)->format('m-d-Y') }} to {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->new_value)->format('m-d-Y') }}</blockquote>
                                                                @else
                                                                    <blockquote class="message">Changed {{ $activity->field }} from "{{ $activity->old_value }}" to "{{ $activity->new_value }}"</blockquote>
                                                                @endif
                                                            @else
                                                                @if(substr($activity->type, 0, 7 ) === 'deleted')
                                                                    <blockquote class="message">Deleted a {{ strtolower(str_replace('App\\', '', $activity->subject_type)) }}</blockquote>
                                                                @else
                                                                    @if($activity->subject)
                                                                        <blockquote class="message">Created a <a href="{{ $activity->subject->path }}">{{ strtolower(str_replace('App\\', '', $activity->subject_type)) }}</a></blockquote>
                                                                    @endif
                                                                @endif
                                                            @endif
                                                        @endforeach
                                                        <br />
                                                        <p class="url">
                                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }} on {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $activity->created_at)->format('l\\, F d\\, Y h:i A') }}
                                                        </p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <!-- end recent activity -->
                                    </div>

                                    @can('update_roles')

                                    <div role="tabpanel" class="tab-pane fade" id="roles" aria-labelledby="roles-tab">
                                        @if (!$user->isSuperAdmin())
                                            <form class="form-horizontal form-label-left" method="POST" action="{{ route('users.roles')}}">
                                                {{ csrf_field() }}
                                                <div class="form-group">
                                                    <label>Select the roles for this user:</label>
                                                    <div class="form-check col-md-12 col-sm-12 col-xs-12">
                                                        @foreach (\App\Role::exclude('superadmin')->get() as $role)
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <input class="form-check-input flat" type="checkbox" name="role_user[]" value="{{ $role->id }}" id="{{ $role->name }}" {{ $user->roles->contains($role->id) ? 'checked' : '' }}>
                                                                <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                                <label class="form-check-label" for="{{ $role->name }}">
                                                                    {{ $role->name }}
                                                                </label>
                                                            </div>
                                                        @endforeach
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">Update</button>
                                                </div>
                                        @else
                                            <label>{{ $user->name }} is a superadmin! You do not have permission to change his role!</label>
                                        @endif
                                        </form>
                                    </div>
                                    @endcan

                                    @if(auth()->id() == $user->id)
                                        <div role="tabpanel" class="tab-pane fade" id="signature" aria-labelledby="signature-tab">
                                            <signature :initial-user="{{ $user }}"></signature>
                                        </div>

                                        <div role="tabpanel" class="tab-pane" id="change-password" aria-labelledby="change-password-tab">
                                            <password-change :initial-user="{{ $user }}"></password-change>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        let url = new URL(window.location.href);
        let tab = url.searchParams.get('tab');

        if(tab) {
            $('#'+tab+'-tab').tab('show');
        }
    </script>
@endsection
