@extends('errors.layout')

@section('content')
    <div class="col-md-12">
        <div class="col-middle">
            <div class="text-center text-center">
                <h1 class="error-number">403</h1>
                <h2>{{ $exception->getMessage() }}</h2>
                <p>You do not have permission to access this page. <a href="{{ url('/') }}">Click here</a> to go back to homepage!</p>
            </div>
        </div>
    </div>
@endsection
