@extends('errors.layout')

@section('content')
    <div class="col-md-12">
        <div class="col-middle">
            <div class="text-center text-center">
                <h1 class="error-number">404</h1>
                <h2>Sorry, the page you are looking for could not be found. </h2>
                <p><a href="{{ url('/') }}">Click here</a> to go back to homepage!</p>
            </div>
        </div>
    </div>
@endsection
