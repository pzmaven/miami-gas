@extends('errors.layout')

@section('content')
    <div class="col-md-12">
        <div class="col-middle">
            <div class="text-center text-center">
                <h1 class="error-number">Thank you!</h1>
                <h2>Your service agreement has been processed and is ready to download.</h2>
                <h2>Please click the button below to start the download.</h2>

                <a href="/agreements/{{$id}}/download/{{ $hash}}" class="btn btn-large btn-success">
                    <i class="far fa-file-pdf"></i> Download Service Agreement (.pdf)
                </a>
            </div>
        </div>
    </div>
@endsection
