<!DOCTYPE html>
<html>
<head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} Service Agreement</title>

    <link href="{{ asset('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        p.sub-point {
            margin-left: 50px;
        }

        .contract-text {
            font-size: 12px;
        }

        .signatures {
            margin-left: 100px;
            max-width: 200px;
            max-width: 200px;
        }
    </style>
</head>

<body>
    <div class="container" id="app">
        @auth()
            <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                <i class="fas fa-arrow-left"></i> Back
            </button>
        @endauth

        <h2 class="text-center">Review and sign the service agreement</h2>

        <div class="panel panel-default" style="max-height: 400px; overflow-x: auto; margin: 10px">
            @include('agreements.bases.' . $base . '.template')
        </div>

        @yield('content')

        <flash message="{{ session('flash') }}"></flash>
    </div>

    <!-- Scripts -->
    <script>
        window.App = {!! json_encode([
            'csrfToken' => csrf_token(),
            'user' => auth()->user(),
            'signedIn' => Auth::check(),
            'permissions' => auth()->user() ? auth()->user()->permissions()->pluck('name')->toArray() : '',
        ]) !!};
    </script>

    <!-- jQuery -->
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>
    <!-- Bootstrap -->
    <script src="{{ asset('vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
