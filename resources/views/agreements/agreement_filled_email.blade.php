@extends(auth()->id() ? 'template.main' : 'agreements.agreement_layout')

@auth
    @section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Review & sign service agreement</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        @auth()
                            <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                                <i class="fas fa-arrow-left"></i> Back
                            </button>
                        @endauth
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default" style="max-height: 400px; overflow: auto;">
                            @include('agreements.bases.' . $base . '.template')
                        </div>
                        @if(!$agreement instanceof App\Agreement)
                            <h1>Review the agreement before sending it to the client</h1>
                            <a data-toggle="modal" data-target="#emailModal" class="btn btn-lg btn-success"><i class="far fa-envelope"></i> Send to e-mail</a>
                            <email-agreement-modal :agreement-data="{{ json_encode($agreement) }}"></email-agreement-modal>
                        @else
                            @if($agreement->status->company_signed)
                                <h2 class="text-success">This contract was already signed by {{ config('app.name') }}.</h2>
                            @else
                                <agreement-signature-pad :data="{userType: 'employee', page: 'email'}"></agreement-signature-pad>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
@endauth

@guest
    @section('content')
        <agreement-signature-pad :data="{userType: 'client', page: 'email'}"></agreement-signature-pad>
    @endsection
@endguest
