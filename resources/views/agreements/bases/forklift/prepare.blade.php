@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Service Agreement Form</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                            <i class="fas fa-arrow-left"></i> Back
                        </button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" action="{{ route('agreements.services') }}" class="form-horizontal">
                            @csrf

                            <input type="hidden" name="agreement_type" value="commercial" required>

                            <div class="form-group">
                                <label for="client_company_name" class="control-label col-md-3 col-sm-3 col-xs-12">Company name:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="client_company_name" placeholder="Enter company name" value="{{ old('client_company_name') ? old('client_company_name') : $address->contact->company }}" autocomplete="off" required>
                                    @if ($errors->has('client_company_name'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('client_company_name') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="client_company_address" class="control-label col-md-3 col-sm-3 col-xs-12">Registered address:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="client_company_address" placeholder="Enter company registered address" value="{{ old('client_company_address') }}" autocomplete="off" required>
                                    @if ($errors->has('client_company_address'))
                                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('client_company_address') }}</strong>
                                </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="agreement_date" class="control-label col-md-3 col-sm-3 col-xs-12">Agreement Date:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <datetime type="date" name="agreement_date"
                                              format="MM-dd-yyyy"
                                              value="{{ Carbon\Carbon::today()->format('Y-m-d') }}"
                                              input-class="form-control col-xs-7 col-xs-12" :auto="true" required>
                                    </datetime>
                                    @if ($errors->has('agreement_date'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('agreement_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="agreement_duration" class="control-label col-md-3 col-sm-3 col-xs-12">Agreement Duration:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select class="form-control" name="agreement_duration">
                                        @foreach (config('dropdowns.contract_duration') as $key => $value)
                                            <option value="{{ $key }}" {{ (old('agreement_duration')==$key)? 'selected':'' }}>{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="first_name" class="control-label col-md-3 col-sm-3 col-xs-12">First Name: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="hidden" name="contact_id" value="{{ $address->contact->id }}">
                                    <input type="hidden" name="address_id" value="{{ $address->id }}">
                                    <input type="hidden" name="base" value="{{ $base }}">
                                    <input type="text" class="form-control" name="first_name"  value="{{ $address->contact->first_name }}" readonly required>
                                    @if ($errors->has('first_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="last_name" class="control-label col-md-3 col-sm-3 col-xs-12">Last Name: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="last_name" value="{{ $address->contact->last_name }}" readonly required>
                                    @if ($errors->has('last_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Address: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" value="{{ $address->full_address }}" readonly>
                                    <input type="hidden" name="street_address" value="{{ $address->street_address }}">
                                    <input type="hidden" name="suite_no" value="{{ $address->suite_no }}">
                                    <input type="hidden" name="city" value="{{ $address->city }}">
                                    <input type="hidden" name="state" value="{{ $address->state }}">
                                    <input type="hidden" name="zip" value="{{ $address->zip }}">
                                    @if ($errors->has('address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="email" class="form-control" name="email" value="{{ $address->contact->email }}" readonly required>
                                    @if ($errors->has('email'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Phone: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="tel" class="form-control" name="phone"
                                           value="{{ $address->contact->phoneNumber }}"
                                           v-mask="{mask: '(999) 999-9999 [ext:99999]', greedy: true}" readonly required>
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="emergency_contact_name" class="control-label col-md-3 col-sm-3 col-xs-12">Emergency Contact Name: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="emergency_contact_name" value="{{ old('emergency_contact_name') }}">
                                    @if ($errors->has('emergency_contact_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('emergency_contact_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="emergency_contact_phone" class="control-label col-md-3 col-sm-3 col-xs-12">Emergency Contact Phone: </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="tel" class="form-control" name="emergency_contact_phone" value="{{ old('emergency_contact_phone') }}"
                                           v-mask="{mask: '(999) 999-9999 [ext:99999]', greedy: true}">
                                    @if ($errors->has('emergency_contact_phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('emergency_contact_phone') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <br>
                            <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-1">
                                <button type="submit" class="btn btn-lg btn-block btn-success">Continue <i class="fas fa-arrow-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
