@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Services configuration</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        @auth()
                            <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                                <i class="fas fa-arrow-left"></i> Back
                            </button>
                        @endauth
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" action="{{ route('agreements.show.client') }}" class="form-horizontal">
                            @csrf

                            <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-1">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Services configuration</div>
                                    <div class="panel-body">
                                        <p>No services configuration is required for this type of agreement</p>
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <button type="submit" name="submit" value="signNow" class="btn btn-lg btn-block btn-primary"><i class="fas fa-pen-nib"></i> Sign it now</button>
                                </div>

                                <div class="form-group col-md-6">
                                    <button type="submit" name="submit" value="sendEmail" class="btn btn-lg btn-block btn-success"><i class="far fa-envelope"></i> Send to e-mail</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
