@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Service Agreement Form</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                            <i class="fas fa-arrow-left"></i> Back
                        </button>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" action="{{ route('agreements.configuration') }}" class="form-horizontal">
                            @csrf
                            <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-1">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Services</div>
                                    <div class="panel-body">
                                        @foreach(config('contract_' . session()->get('agreement.base') . '.services') as $key => $service)
                                            <div class="form-inline col-xs-12">
                                                @if($loop->first)
                                                    <div class="form-group"></div>
                                                    <div class="form-group">
                                                        <input class="form-check-input flat" type="checkbox" value="{{ $service['name'] }}" name="services[{{ $key }}][name]" id="{{ $key }}" required checked>
                                                        <label class="form-check-label" for="{{ $key }}">
                                                            {{ $service['name'] }}
                                                        </label>
                                                    </div>
                                                @else
                                                    <div class="form-group">
                                                        <label>Qty:</label>
                                                        <input type="number" class="form-control" value="1" step="1" min="1" name="services[{{ $key }}][qty]">
                                                    </div>

                                                    <div class="form-group">
                                                        <input class="form-check-input flat" type="checkbox" value="{{ $service['name'] }}" id="{{ $key }}" name="services[{{ $key }}][name]">
                                                        <label class="form-check-label" for="{{ $key }}">
                                                            {{ $service['name'] }}
                                                        </label>
                                                    </div>
                                                @endif
                                            </div>
                                        @endforeach
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-lg btn-block btn-success">Continue <i class="fas fa-arrow-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
