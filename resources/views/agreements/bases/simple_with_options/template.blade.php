@if($options['agreement_type'] == 'commercial')
    @php
        $client_name = $options['client_company_name'];
        $client_address = $options['client_company_address'];
    @endphp
@elseif($options['agreement_type'] == 'residential')
    @php
        $client_name = $options['first_name'] . ' ' . $options['last_name'];

        $client_address = '';
        $client_address .= !empty($options['street_address']) ? $options['street_address'] . ', ' : '';
        $client_address .= !empty($options['suite_no']) ? 'Suite ' . $options['suite_no'] . ', ' : '';
        $client_address .= !empty($options['city']) ? $options['city'] . ', ' : '';
        $client_address .= !empty($options['state']) ? $options['state'] . ', ' : '';
        $client_address .= !empty($options['zip']) ? $options['zip'] : '';
    @endphp
@endif

<div class="container contract-text col-md-12">
    <div class="row">
        <div class="col-md-10 pull-left">
            <span>
                <strong>{{ config('contracts.company_details.company_name') }}</strong><br>
                {{ config('contracts.company_details.address') }}<br>
                {{ config('contracts.company_details.phone') }}<br>
                A Category One Liquefied Petroleum Gas Dealer, License #{{ config('contracts.company_details.license') }}<br>
            </span>
        </div>

        <div class="col-md-2 pull-right">
            <img src="{{ asset('img/logo150.png') }}"/>
        </div>
    </div>
    <hr/>

    <p>
        These terms and conditions are agreed upon by {{ $client_name }} (“Customer”) located at {{ $client_address }} (“Premises”) with respect to certain services to be provided by Discount Gas LLC,
        DBA Miami Gas, a Florida Limited Liability Company located at 2747 NW 21 St. Miami, FL 33142 (“Miami Gas”).  Miami Gas has agreed to supply and deliver liquid
        propane (“L.P.”) gas to Customer at such times and in such amounts as Customer may reasonably require and request at prices to be mutually agreed upon by
        Miami Gas and Customer. Customer grants to Miami Gas free and unobstructed right of ingress and egress to the Customer’s Premises for all purposes necessary to
        render/ retire services, service and maintain equipment and deliver L.P. gas and shall allow Miami Gas to park any of its vehicles on the driveway(s) at the
        Premises as well as on any right of ways. Customer agrees to pay all sums due Miami Gas at the address set forth in Miami Gas’ billing statements provided to
        Customer from time to time.  It is understood and agreed that Miami Gas may discontinue Customer’s service any time that L.P. gas purchased by Customer has not been
        paid for in accordance with Miami Gas’ payment terms. In the event of a returned check, a fee of $35.00 will be charged to Customer.
    </p>

    <p>
        It is understood and agreed that Miami Gas. shall in no way be responsible for damages to property or personal injuries to Customer or any person or entity arising
        out of or related to Miami Gas’ services including, without limitation, (i) the storage or use of L.P. gas, (ii) the operation, maintenance or repair of any equipment
        or appliance storing or utilizing L.P. gas or (iii) the presence of any Miami Gas representative on or about the Premises for the purpose of carrying out the
        Services provided by Miami Gas. Customer agrees to indemnify and hold Miami Gas harmless from all liability, loss, damages, attorney’s fees, cost, or expense claimed,
        suffered or sustained by any person or entity growing out of the installation, removal, use, servicing, repair, maintenance, filling, refilling, or any other
        activity whosoever in connection or related to any services provided by Miami Gas to Customer.  Should it be necessary for Miami Gas to retain an attorney for
        collections or enforcement of these terms and conditions, said attorneys' fees shall be borne by the Customer whether or not litigation ensues.
    </p>

    <p>
        Customer acknowledges that Customer is aware that section 527.07 Florida Statutes 1997 restricts the use of containers. No person, other than the owner and those
        authorized by the owner, shall sell, fill, refill, deliver, permit to be delivered, or use in any manner any liquefied petroleum gas container or receptacle for
        any gas or compound, or for any degree, punishable as provided is Section 775.083.
    </p>

    @if($options['services_prices'])
        <p>Below are the terms of this agreement:</p>

        Propane price per gallon: ${{ $options['services_prices']['propane']['price_per_gallon'] }}
        @foreach($options['services_prices'] as $service)
            <ul>
                @if($service['name'] != 'Propane')
                    <li>{{ $service['name'] }}</li>
                    @foreach($service['charges'] as $charge)
                        {{ $charge['name'] }}: ${{ $charge['amount'] }}<br>
                    @endforeach
                @endif
            </ul>
        @endforeach
    @endif

    @if($options['cancellation_fees']['cancellation_fee'])
        Cancellation Fee: ${{ $options['cancellation_fees']['cancellation_fee'] }}<br>
    @endif

    @if($options['cancellation_fees']['piping_reimbursement'])
        Piping Reimbursement: ${{ $options['cancellation_fees']['piping_reimbursement'] }}
    @endif

    <br><br>

    @if($pdfable)
        <table>
            <tr>
                <td><h5>CUSTOMER</h5></td>
            </tr>

            <tr>
                <td><img class="img-signature" src="{{ storage_path('app/public/'.$agreement['client_signature']) }}" /></td>
            </tr>

            <tr>
                <td>Print Name: {{ $agreement['client_signature_name_typed'] }}</td>
            </tr>

            <tr>
                <td>Email: {{ $options['email'] }}</td>
            </tr>

            <tr>
                <td>Telephone # {{ $options['phone'] }}</td>
            </tr>
        </table>
    @endif
</div>
