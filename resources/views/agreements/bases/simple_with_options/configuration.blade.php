@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Services configuration</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        @auth()
                            <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                                <i class="fas fa-arrow-left"></i> Back
                            </button>
                        @endauth
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-9 col-sm-12 col-xs-12">
                        <form method="POST" action="{{ route('agreements.show.client') }}" class="form-horizontal">
                            @csrf
                            @foreach($options['services'] as $key => $service)
                                @if(!isset($options['services'][$key]['qty']))
                                    <div class="panel panel-primary">
                                        <div class="panel-heading">{{ $service['name'] }}</div>
                                        @foreach($service['charges'] as $charge_key => $charge)
                                            <div class="panel-body">
                                                <div class="form-group">
                                                    <label for="contractDate" class="col-xs-3 control-label">{{ $charge['name'] }}:</label>
                                                    <div class="col-xs-9">
                                                        <div class="input-group">
                                                            <span class="input-group-addon">$</span>
                                                            <input type="hidden" name="services_prices[{{ $key }}][name]" value="{{ $service['name'] }}">
                                                            <input type="number" class="form-control" name="services_prices[{{ $key }}][{{ $charge_key }}]" min="0" step=".01">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    @if(isset($options['services'][$key]['name']))
                                        @for($i=0; $i<$options['services'][$key]['qty'];$i++)
                                            <div class="panel panel-primary">
                                                <div class="panel-heading">{{ $service['name'] }} {{ $i+1 }}</div>

                                                <div class="panel-body">
                                                    @foreach($service['charges'] as $charge_key => $charge)
                                                        <div class="panel-body">
                                                            <div class="form-group">
                                                                <label for="contractDate" class="col-xs-3 control-label">{{ $charge['name'] }}:</label>
                                                                <div class="col-xs-9">
                                                                    <div class="input-group">
                                                                        <span class="input-group-addon">$</span>
                                                                        <input type="hidden" class="form-control" name="services_prices[{{ $key }}_{{ $i+1 }}][name]" value="{{ $service['name'] }} {{ $i+1 }}">
                                                                        <input type="number" class="form-control" name="services_prices[{{ $key }}_{{ $i+1 }}][charges][{{ $charge_key }}][amount]" min="0" step=".01">
                                                                        <input type="hidden" class="form-control" name="services_prices[{{ $key }}_{{ $i+1 }}][charges][{{ $charge_key }}][name]" value="{{ $charge['name'] }}">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endfor
                                    @endif
                                @endif
                            @endforeach
                            <div class="panel panel-danger">
                                <div class="panel-heading">{{ $options['cancellation_fees']['name'] }}</div>
                                @foreach($options['cancellation_fees']['charges'] as $key => $charge)
                                    <div class="panel-body">
                                        <div class="form-group">
                                            <label for="contractDate" class="col-xs-3 control-label">{{ $charge['name'] }}:</label>
                                            <div class="col-xs-9">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="number" class="form-control" name="cancellation_fees[{{ $key }}]" min="0" step=".01">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="form-group col-md-6">
                                <button type="submit" name="submit" value="signNow" class="btn btn-lg btn-block btn-primary"><i class="fas fa-pen-nib"></i> Sign it now</button>
                            </div>

                            <div class="form-group col-md-6">
                                <button type="submit" name="submit" value="sendEmail" class="btn btn-lg btn-block btn-success"><i class="far fa-envelope"></i> Send to e-mail</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
