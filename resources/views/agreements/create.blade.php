@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Service Agreement</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Choose base and address</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <form method="POST" action="{{ route('agreements.prepare') }}" class="form-horizontal">
                            @csrf
                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="base">
                                    Agreement base <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="base" name="base" required="required" class="form-control col-md-7 col-xs-12">
                                        <option value="" disabled selected>Choose base</option>
                                        @foreach(config('agreements.templates') as $key => $template)
                                            <option value="{{ $key }}" {{ ($key == $base) ? 'selected' : '' }}>{{ $template['name'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address_id">
                                    Address <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    @if($address)
                                        <autocomplete-address :initial-address="{{ $address }}"></autocomplete-address>
                                    @else
                                        <autocomplete-address></autocomplete-address>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Continue</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
