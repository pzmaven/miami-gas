@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Review & sign service agreement</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    @auth()
                        <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                            <i class="fas fa-arrow-left"></i> Back
                        </button>
                    @endauth
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="panel panel-default" style="max-height: 400px; overflow: auto;">
                        @include('agreements.bases.' . $base . '.template')
                    </div>

                    <agreement-signature-pad :data="{userType: 'employee', page: 'employee'}"></agreement-signature-pad>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
