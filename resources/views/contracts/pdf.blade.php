<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=40, initial-scale=1">
    <meta charset="utf-8">
    <title>Miami Gas</title>
    <link href="{{ public_path('/vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <style>
        @page {
            margin: 1cm;
        }

        body {
            width: 19.5cm;
            xmargin: auto;
        }

        h2 {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 500;
            font-size: 30px;
        }

        h3 {
            font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
            font-weight: 500;
        }

        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            position: relative;
            min-height: 1px;
            padding-right: 0px;
            padding-left: 0px;
        }

        .table > tbody > tr.emptyrow > td {
            border-top: 0;
        }

        .img-signature{
            max-width: 200px;
        }

        p.sub-point {
            margin-left: 50px;
        }

        .contract-text {
            font-size: 10px;
        }
    </style>
</head>

<body>
    @include('contracts.templates.' . $contractData['base'])
</body>
</html>
