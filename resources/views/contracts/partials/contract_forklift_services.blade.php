<div class="panel panel-primary">
    <div class="panel-heading">Forklift Services</div>
    <div class="panel-body">
        @foreach(config('contract_' . $data['base'] . '.services') as $key => $service)
            <div class="form-inline col-xs-12">
                <div class="form-group">
                    <label>Qty:</label>
                    <input type="number" class="form-control" value=1 step="1" min="1"
                           name="services_qty[{{$key}}]">
                </div>
                <div class="form-group">
                    <input class="form-check-input flat" type="checkbox" value="{{$key}}" id="{{$key}}"
                           name="services[]">
                    <label class="form-check-label" for="{{$key}}">
                        {{$service['name']}}
                    </label>
                </div>
            </div>
        @endforeach
    </div>
</div>
