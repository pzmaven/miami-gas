@if($contractData['contract_type'] == 'commercial')
    @php
        $client_name = $contractData['client_company_name'];
        $client_address = $contractData['client_company_address'];
    @endphp
@elseif($contractData['contract_type'] == 'residential')
    @php
        $client_name = $contractData['first_name'] . ' ' . $contractData['last_name'];
        $client_address = $contractData['street_address'] . ' ' . $contractData['city'] . ', ' . $contractData['state'] . ', ' . $contractData['zip'];
    @endphp
@endif

<div class="container contract-text col-md-12">
    <div class="row">
        <div class="col-md-10 pull-left">
            <span>
                <strong>{{ config('contracts.company_details.company_name') }}</strong><br>
                {{ config('contracts.company_details.address') }}<br>
                {{ config('contracts.company_details.phone') }}<br>
                A Category One Liquefied Petroleum Gas Dealer, License #{{ config('contracts.company_details.license') }}<br>
                {{ strtoupper($contractData['contract_type']) }} LP GAS SUPPLY AND LEASE AGREEMENT<br>
            </span>
        </div>

        <div class="col-md-2 pull-right">
            <img src="{{ asset('img/logo150.png') }}"/>
        </div>
    </div>
    <hr/>
    <p>
        This service agreement (“Agreement”) made this <u>{{ $contractDate['day'] }}</u>
        day of <u>{{ $contractDate['month'] }}</u>, <u>{{ $contractDate['year'] }}</u>,
        is entered into by and between {{ config('contracts.company_details.company_name') }},
        Florida Limited Liability Company located at {{ config('contracts.company_details.address') }} (“Miami Gas”),
        and <u>{{ $client_name }}</u> (“Customer”) located at
        <u>{{ $client_address }}</u> (“Premises”).
    </p>

    <p class="text-center">WITNESSETH</p>

    <p>
        In consideration of the mutual covenants contained herein and for other good and valuable considerations,
        the receipt and sufficiency of which are hereby acknowledged, the parties hereto, intending to be
        legally bound, agree as follows:
    </p>

    <p>
        1. Equipment. Customer agrees to lease the following Equipment:
		<?php

		/**
			This counter is being implemented because we have removed Propane as an item that is required.
			If propane price is 0.00 it will not be displayed and the contract terminology will
			state that Miami Gas is not required to deliver gas at any specific price.
		**/
		$counter = 0;
        foreach($services as $service){
        if($service['name'] == 'Propane'){continue;}
        $counter = $counter + 1;
        ?>

        <ul>
            <li>{{ $service['name'] }}</li>
            @foreach($service['charges'] as $charge)
            {{ $charge['name'] }}: ${{ $charge['amount'] }},
            @endforeach
        </ul>
        <?php }
        /**
	        If Propane is the only item on contract,
	        it will not show up under eqipment
	        list. We will display N/A.
	    **/
	    if($counter == 0) {
	    echo "<ul><li> • Not Applicable.  </li></ul> </p>";
	        }
            else
            {
	    ?>

    </p>

    <p>
        The equipment listed above (the “Equipment”) will at all times remain the property of Miami Gas. Customer will at no time retain any ownership interest in the Equipment
        regardless of any affixation to the Premises but will only have the right to us the Equipment under the terms of this Agreement. Upon Miami Gas’ request, Customer agrees to
        execute a U.C.C. Financing Statement, which covers the Equipment under this Agreement and authorizes Miami Gas to file same in the public records until otherwise noted
        (L.P. gas Equipment such as tank(s), vaporizers, regulators and meters. If any are not considered part of the piping system and shall at all times remain the property of Miami Gas).
    </p>
    <?php } ?>
    <p>

        2. L.P. Gas Service. During the term of this Agreement Miami Gas agrees to supply and deliver L.P. gas to Customer as Customer may reasonably require and request.<br>
        <?php
	    /**
		    Here we are adding a condition that checks if the price of propane has been set to 0.
		    If so we will state that Miami Gas is not required to sell Gas at any price
		    But the client is still required to purchase Gas From Miami Gas.
	    **/
	    ?>

        @if(intval($propanePrice) <= 0)
        This agreement does not require Miami Gas to supply or deliver L.P. gas at any specific price.
        @else
        $<u>{{ $propanePrice }}</u> per gallon, plus applicable taxes, charges and fees, will be the initial retail price for L.P. gas sold under this Agreement.
        @endif
        Customer acknowledges that as L.P. gas
        is a commodity for which the market rate of L.P. gas fluctuates, and that Miami Gas’ costs may increase from time to time. Customer agrees that due to price variations from
        our suppliers and increased operating costs, Miami Gas may increase the Customers’ cost no more than $.90 per gallon (before taxes) in any twelve (12) month period without
        notice. Customer agrees that Miami Gas will be the sole and exclusive supplier for all of the Customer’s L.P. gas requirements for Customer’s operations. If, during the term of
        this Agreement, for any reason whatsoever Customer stops purchasing all or part of their L.P. gas requirements from Miami Gas Customer will have breached this Agreement.
    </p>

    <p>
        3. Cancellation Fees/Equipment Removal. In the event, during the term of this Agreement and any extension thereof, Customer stops purchasing LP gas for any reason
        whatsoever, the Customer agrees to pay (in addition to other amounts owed pursuant to this Agreement) to Miami Gas the following amounts:

        @if($contractData['contract_type'] == 'residential')
            <p class="sub-point">
                In the event the Premises is a residential location, Customer will pay on termination a fixed cancellation fee in the amount of $<u>{{ $cancellationFeePrice }}</u>, which Customer
                acknowledges is minimum reasonable reimbursement for the cost of removal of the Equipment. This is in addition to other remedies as specified in
                paragraph 7 below.
            </p>
        @elseif($contractData['contract_type'] == 'commercial')
            <p class="sub-point">
                In the event the Premises is a commercial location, Customer will pay on termination the sum of (i) a fixed cancellation fee in the amount of $<u>{{ $cancellationFeePrice }}</u>,
                which Customer acknowledges is minimum reasonable reimbursement for the cost of removal of the Equipment and (ii) the lost revenue of Miami Gas for
                the balance of the term of the Agreement which shall equal the product of (A) the number of full months left in the term of the Agreement and (B) the
                greater of the average amount expended pursuant to this Agreement by Customer during the past twelve (12) months of this Agreement based on either
                total usage or number of tanks purchased, whichever is greater.
            </p>
        @endif

        <p>
            Upon termination of this Agreement nothing in this Agreement shall require Miami Gas to remove the Equipment should it deem, in its sole discretion that such removal is
            impractical. Should Miami Gas decide not to remove the Equipment, it shall notify Customer of such a decision and it shall be Customer's sole responsibility dispose of the
            Equipment and the removal cost enumerated in this paragraph shall not be charged.
        </p>

        <p>
            4. Piping. In the event, during the initial term of this Agreement, Customer stops purchasing L.P. gas for any reason whatsoever, the Customer agrees to pay Miami Gas
            $<u>{{ $pipingReimbursementPrice }}</u> which Customer acknowledges is the minimum reasonable reimbursement for piping and other work provided by Miami Gas. The sum specified in this section is
            in addition to any other payments and remedies this Agreement provides for Miami Gas.
        </p>

        <p>
            5. Term of Agreement. The initial term of this Agreement will begin on the date indicated above and will continue for {{ $contractData['contract_duration'] ? $contractData['contract_duration'] : 'five (5) years' }} and each subsequent term of this
            Agreement shall be from year to year thereafter unless the Agreement is terminated by either party by giving written notice of termination, at least thirty days, but no sooner
            than ninety days, prior to the expiration of either the original lease term or any subsequent lease term. Upon the expiration of this Agreement, Customer grants Miami Gas a
            right of first refusal to match any bona fide written offer from a third party provider of L.P. gas service. Upon termination of this Agreement, no part of the lease or installation
            charge shall be refundable and Miami Gas shall have the right, without being guilty and/or liable for any trespass to enter upon the Premises to remove the Equipment. Should
            Miami Gas require the use of a locksmith to gain access to the Equipment, Customer shall be obligated to reimburse Miami Gas for all costs related to such locksmith and
            Customer shall be additionally responsible for replacement costs of any locks which were damaged in the course of Miami Gas gaining access to the Equipment. Customer is
            required to give Miami Gas access to the Equipment at any reasonable time Miami Gas desires access and shall grant Miami Gas the authority to park its vehicles on Customer’s
            driveway(s) and/or right of ways. Miami Gas shall not be liable for any injury or damage to the Premises occasioned by any removal of the Equipment. There are no
            circumstances under which, Customer may claim or charge any storage or rent for the Equipment. It is Customer’s responsibility to exercise due care to protect the Equipment
            and insure it for damage or loss. Customer shall bear the risk of damage or loss to the Equipment until its removal or abandonment by Miami Gas.
        </p>

        <p>
            6. Payment Terms. Customer agrees to pay all sums due Miami Gas at the address set forth in Miami Gas’ statement in accordance with Miami Gas’ standard payment terms.
            Customer agrees that Miami Gas shall have the right from time to time to change the payment terms. If Customer’s credit ratings are deemed unsatisfactory by Miami Gas, at
            its sole discretion, Miami Gas will have the right to require payment in advance before deliveries. It is understood and agreed that Miami Gas may discontinue Customer’s
            service any time that L.P. gas purchased by Customer has not been paid for in accordance with Miami Gas’ payment terms. Each individual and/or entity signing below
            authorizes Miami Gas or any assignee to obtain a consumer credit report that will be ongoing and relate not only to Miami Gas’ decision to enter into this Agreement and/or any
            additional lease term extensions, but also for purposes of reviewing the account, taking collection action on the account, and for any other legitimate purpose associated with the
            account as may be needed from time to time. Each individual and/or entity signing below further waives any right or claim which such individual and/or entity would otherwise
            have under the Fair Credit Reporting Act in the absence of this continuing consent.
        </p>

        <p>
            7. Breach of Agreement. In the event of a breach of this Agreement or any other default by Customer on any of the terms, conditions or other provisions of this Agreement,
            Miami Gas shall be entitled to exercise any and all rights and remedies available at law and equity, including but not limited to, seeking relief for costs incurred under
            paragraphs 3 and 4 above, damages, loss of revenue and/or lost profits. Customer hereby authorizes and empowers Miami Gas to enter the Customer’s Premises or any other
            place where Equipment of Miami Gas leased to Customer may be found, to take possession and carry away and remove such Equipment with or without legal process and
            thereby terminate the Customer’s right to retain and use such Equipment. Customer agrees to indemnify and pay Miami Gas for all costs incurred in removal of Equipment and
            repossession, including reasonable attorney’s fees and other legal costs.
        </p>

        <p>
            8. Indemnification\Hold Harmless. It is understood and agreed that Miami Gas. shall in no way be responsible for damages to property or personal injuries to Customer or any
            person or entity not a party to this Agreement rising out of or related to Miami Gas’ activities or omissions including, without limitations, storage or use of L.P. gas, or to use
            and operations, maintenance or repair of any Equipment of appliance storing or utilizing L.P. gas or arising out of the presence of Miami Gas’ representative on or about the
            Premises for the purpose of carrying out the provisions of the Agreement. Customer agrees to indemnify and hold Miami Gas harmless from all liability, loss, damages,
            attorney’s fees, cost, or expense claimed, suffered or sustained by any person or entity growing out of the installation, removal, use, servicing, repair, maintenance, filling,
            refilling, or any other activity whosoever in connection or related to this Agreement, the Equipment, or any sale hereunder.
        </p>

        <p>
            9. Limitations of Liability. Customer understands and agrees that if Miami Gas shall be found liable for breach or damage due to failure of Miami Gas to perform any of
            Miami Gas’ obligations hereunder, Miami Gas’ liability shall be limited to $250.00 and this liability shall be exclusive, and that the provision of this section shall apply if any breach or
            damage, irrespective of cause or origin, results directly or indirectly to persons or property, arising out of or related to Miami Gas’ activities or omissions including, without
            limitations, performance or non-performance of Miami Gas’ obligations, breach of express or implied warranty, or from negligence, active or otherwise by Miami Gas it’s
            agents, servants, assignees or employees. In no event shall Miami Gas be responsible for any other damages, including special or consequential damages.
        </p>

        <p>
            10. Applicable Laws and Venues. The parties waive trial by jury and any legal proceeding arising out of this Agreement or any other m atters between the parties.
            All disputes and matters of any nature arising under, in connection with or incidental to this Agreement shall be subject to the laws on the state of Florida
            and shall be litigated before a State Court of the proper jurisdiction located in MIAMI DADE COUNTY, FLORIDA USA to the exclusion of courts of any other county, state or country.
        </p>

        <p class="text-center">
            ADDITIONAL TERMS AND CONDITIONS OF THE AGREEMENT
        </p>


            1. Ownership and Identification. Miami Gas is the sole owner of the Equipment and Customer agrees and acknowledges that it is intent of the parties to this to create a lease
            by entering into this Agreement. In the event that this Agreement is determined not to be a lease, Customer hereby grants to Miami Gas a security interest in the Equipment.
            Miami Gas may file this Agreement or other documents evidencing ownership with the appropriate authorities.<br>


            2. Location. Customer will keep the Equipment at the address shown on the reverse side of this Agreement (i.e., the Premises) and shall not move Equipment from such
            location without Miami Gas’ prior written consent.<br>


            3. Loss and/or Damage of Equipment. Risk of loss concerning the Equipment is on Customer. In the event that the Equipment becomes lost, stolen or damaged, as long as
            Customer is not in default under this Agreement, or any other Agreements with Miami Gas, Customer will have the opportunity to a) replace the Equipment with like
            equipment; or b) purchase the Equipment by paying the full amount as listed in Miami Gas’s price schedule. In the event that the Equipment becomes lost, stolen or damaged
            and the Customer is in default under this Agreement, or any other Agreements with Miami Gas, Miami Gas shall be under no duty or obligation to replace or repair the
            Equipment.<br><br>
            The exclusive warranty for all Equipment and materials used, if any, is solely that of the original manufacturer of said Equipment and materials. MIAMI GAS OFFERS
            NEITHER EXPRESS NOR IMPLIED WARRANTIES FOR EQUIPMENT AND MATERIALS USED NOR DOES IT OFFER WARRANTIES OF MERCHANTABILITY
            NOR FITNESS FOR SAID EQUIPMENT AND MATERIALS.<br>


            4. Taxes and Certain Fees. Customer agrees to pay all fees, assessments, taxes and charges governmentally imposed upon the lease, purchase, ownership, possession,
            operation, use or maintenance of the Equipment, whether assessed against Miami Gas or Customers or the Equipment, whether due before or after the end of the term; including
            without limitations, sales taxes and personal property taxes. If Miami Gas pays any of these charges, penalties or fines for Customer, Customer agree to reimburse Miami Gas
            upon demand for the entire amount advanced by Miami Gas.<br>


            5. Ingress and Egress. Customers grants to Miami Gas free and unobstructed right of ingress and egress to the Customer’s Premises for all purposes necessary to render/
            retire services, service and maintain equipment and deliver L.P. gas and shall allow Miami Gas to park any of its vehicles on the driveway(s) at the Premises as well as on any
            right of ways.<br>


            6. Late Charges. TIME IS OF THE ESSENCE TO THIS AGREEMENT. In the event that any payment(s) required under this Agreement are not received by Miami Gas
            within ten days from their due date, Customer agrees to pay in addition thereto a delinquency charge equal to one and one-half percent per month on the entire outstanding
            balance due Miami Gas or $30.00, whichever is greater, plus other amounts permitted by law.<br>


            7. Returned Checks. Customer agrees to pay Miami Gas a returned check or non-sufficient funds charge in the amount of $45.00 for each occurrence. This remedy is in
            addition to and not in lieu of any statutory remedy available to Miami Gas.<br>


            8. Miami Gas’ Obligation: Miami Gas’ obligations under this Agreement are contingent upon all federal and state laws and legally promulgated rules and regulations of any
            governmental agency or department having jurisdiction over the subject matter of this Agreement. Miami Gas shall not be liable to Customer due to any delays in deliveries as
            a result of hurricanes, natural disasters, accidents, war, whether declared or not, embargoes, availability of L.P. gas or L.P. gas Equipment or any other causes that are beyond
            the control of Miami Gas.<br>


            9. No Assignment. This Agreement may not be assigned or transferred by Customer without the prior written consent of Miami Gas. This Agreement may, however, be
            assigned by Miami Gas without the consent of Customer.<br>


            10. Entire Agreement. The parties agree that this Agreement contains the entire agreement between the parties and may not be changed, amended modified altered or
            terminated except as in writing signed by duly authorized representatives of both parties.<br>


            11. BINDING EFFECT. This Agreement shall inure to the benefit of and be binding upon the parties hereto, their successors and permitted assigns. The term “Customer”
            shall denote the singular and/or plural, the masculine and/or feminine and natural and/or artificial persons, whenever the context so requires or properly applies.<br>


            12. No Waiver and Interpretation. A waiver of either party of a breach of this Agreement by the other shall not be considered a waiver of any subsequent breach. This
            contract shall be interpreted under the laws of Florida. When interpreting this contract the terms and/or conditions shall not be more strictly construed against either party.<br>


            13. Severability. The unenforceability, for any reason, of any term, condition, covenant, or provision of this Agreement shall neither limit nor impair the operation or validity
            of any other terms, conditions, or covenants of this Agreement. If any provision hereof, is held invalid by a court of competent jurisdiction it shall be automatically detected and
            all remaining provisions shall remain in full force and effect.<br>


            14. Attorney’s Fee. Should it be necessary for Miami Gas to retain an attorney for collections or enforcement of this Agreement, said attorneys' fees shall be borne by the
            Customer whether or not litigation ensues.<br>


            15. Authority to Contract. The persons executing this Agreement on behalf of both parties are acknowledging having full and express authority to execute said document.<br>


            16. Transfer. In the event Customer’s business is transferred to a third party, Customer will notify Miami Gas & buyer at least 30 days before the transfer and shall notify the
            third party of the existence of this Agreement. Assumption of this Agreement by the third party shall not serve to relieve the Customer of its obligations hereunder. This
            Agreement shall survive any sales, assignment, or other transfers of the Customer’s business.<br>


            17. Notice. Any notice or notices made or required to be made hereunder shall be directed to the respective parties by certified, United States mail, return receipt requested, to
            the party’s address set forth on the first page of this Agreement. Nothing contained herein shall prevent either party from changing the place at which notice may be given in
            accordance with the provisions hereof.<br>


            18. Florida Statues. Customer acknowledges that Customer is aware that section 527.07 Florida Statutes 1997 restricts the use of containers. No person, other than the owner
            and those authorized by the owner, shall sell, fill, refill, deliver, permit to be delivered, or use in any manner any liquefied petroleum gas container or receptacle for any gas or
            compound, or for any degree, punishable as provided is Section 775.083.<br>


            19. Counterparts. This Agreement may be executed in counterparts with each being an original and when taken together considered one and the same instrument. PDF, faxed
            or other electronic copies of this Agreement shall be deemed originals for all purposes of this Agreement.<br>

        <br><br>

        @if($pdfable)

        <table>
            <tr>
                <td>
                    <p>Telephone # {{ $contractData['phone'] }}</p>
                    <p>Emergency contact telephone # {{ $contractData['emergency_contact_phone'] }}</p>
                </td>
                <td></td>
            </tr>

            <tr>
                <td><h5>CUSTOMER</h5></td>
                <td><h5>{{ strtoupper(config('contracts.company_details.company_name')) }}</h5></td>
            </tr>

            <tr>
                <td><img class="img-signature" src="{{ storage_path('app/public/'.$contractData['client_signature']) }}" /></td>
                <td><img class="img-signature" src="{{ storage_path('app/public/'.$contractData['company_signature']) }}" /></td>
            </tr>

            <tr>
                <td>Print Name: {{ $contractData['client_signature_name_typed'] }}</td>
                <td>Written by: {{ $contractData['company_signature_name_typed'] }}</td>
            </tr>

            <tr>
                <td>Email: {{ $contractData['email'] }}</td>
                @if(auth()->user())
                    <td>Email: {{ auth()->user()->email }}</td>
                @else
                    <td>Email: {{ config('contracts.company_details.email') }}</td>
                @endif
            </tr>

            <tr>
                <td>
                    <span style="font-size: 11px;">Customer acknowledges that Customer has read and understands the additional terms and conditions on the reverse side
                    of this Agreement, which is hereby expressly made a part of this Agreement, as well as any addenda hereto, all of which
                    are incorporated herein.</span>
                </td>

                <td>
                    <span style="font-size: 11px;">This Agreement shall not be binding upon Discount Gas LLC, DBA Miami Gas unless expressly authorized in
                    writing by an officer of Discount Gas LLC, DBA Miami Gas.</span>
                </td>
            </tr>
        </table>
        @endif
</div>
