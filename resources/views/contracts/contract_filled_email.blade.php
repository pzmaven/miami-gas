@extends(auth()->id() ? 'template.main' : 'contracts.contract_layout')

@auth
    @section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Review & sign service agreement</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        @auth()
                            <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                                <i class="fas fa-arrow-left"></i> Back
                            </button>
                        @endauth
                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default" style="max-height: 400px; overflow: auto;">
                            @include('contracts.templates.' . $contractData['base'])
                        </div>
                        @if(!$contractData instanceof App\Contract)
                            <h1>Review the agreement before sending it to the client</h1>
                            <a data-toggle="modal" data-target="#emailModal" class="btn btn-lg btn-success"><i class="far fa-envelope"></i> Send to e-mail</a>
                            <email-contract-modal :contract-data="{{ json_encode($contractData) }}"></email-contract-modal>
                        @else
                            @if($contractData->status->company_signed)
                                <h2 class="text-success">This contract was already signed by {{ config('app.name') }}.</h2>
                            @else
                                <signature-pad :data="{userType: 'employee', page: 'email'}"></signature-pad>
                            @endif
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
@endauth

@guest
    @section('content')
        <signature-pad :data="{userType: 'client', page: 'email'}"></signature-pad>
    @endsection
@endguest
