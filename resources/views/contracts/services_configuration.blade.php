@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Services configuration</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    @auth()
                        <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                            <i class="fas fa-arrow-left"></i> Back
                        </button>
                    @endauth
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-9 col-sm-12 col-xs-12">
                    <form method="POST" action="filled/client" class="form-horizontal">
                        @csrf
                        @foreach($services as $service)
                                <div class="panel panel-primary">
                                    <div class="panel-heading">{{$service['name']}}</div>
                                    @foreach($service['charges'] as $charge)
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <label for="contractDate" class="col-xs-3 control-label">{{$charge['name']}}:</label>
                                                <div class="col-xs-9">
                                                    <div class="input-group">
                                                        <span class="input-group-addon">$</span>
                                                        <input type="number" class="form-control" name="services_prices[]" min="0" step=".01" required>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                        @endforeach
                        <div class="panel panel-danger">
                            <div class="panel-heading">{{$cancellationFees['name']}}</div>
                            @foreach($cancellationFees['charges'] as $charge)
                                <div class="panel-body">
                                    <div class="form-group">
                                        <label for="contractDate" class="col-xs-3 control-label">{{$charge['name']}}:</label>
                                        <div class="col-xs-9">
                                            <div class="input-group">
                                                <span class="input-group-addon">$</span>
                                                <input type="number" class="form-control" name="cancellation_fees[]" min="0" step=".01" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <div class="form-group col-md-6">
                            <button type="submit" name="submit" value="signNow" class="btn btn-lg btn-block btn-primary"><i class="fas fa-pen-nib"></i> Sign it now</button>
                        </div>

                        <div class="form-group col-md-6">
                            <button type="submit" name="submit" value="sendEmail" class="btn btn-lg btn-block btn-success"><i class="far fa-envelope"></i> Send to e-mail</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
