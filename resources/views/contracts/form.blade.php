@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Service Agreement form</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <button onclick="window.history.back()" class="btn btn-md btn-success no-print">
                        <i class="fas fa-arrow-left"></i> Back
                    </button>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <form method="POST" action="/contracts/services" class="form-horizontal">
                        @csrf

                        @if($data['base'] != 'forklift')
                            <div class="form-group">
                                <label for="contract_date" class="control-label col-md-3 col-sm-3 col-xs-12">Agreement Type:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <div id="contract_type" class="btn-group" data-toggle="buttons">
                                        <label class="btn btn-default" data-toggle-active-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="contract_type" value="commercial" required> Commercial
                                        </label>

                                        <label class="btn btn-default" data-toggle-class="btn-primary" data-toggle-passive-class="btn-default">
                                            <input type="radio" name="contract_type" value="residential" required> Residential
                                        </label>
                                    </div>
                                </div>
                                @if ($errors->has('contract_type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contract_type') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group" id="client_company_name" style="display: none;">
                                <label for="client_company_name" class="control-label col-md-3 col-sm-3 col-xs-12">Company name:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="client_company_name" placeholder="Enter company name">
                                    @if ($errors->has('client_company_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('client_company_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group" id="client_company_address" style="display: none;">
                                <label for="client_company_address" class="control-label col-md-3 col-sm-3 col-xs-12">Registered address:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="client_company_address" placeholder="Enter company registered address">
                                    @if ($errors->has('client_company_address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('client_company_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        @else
                            <input type="hidden" name="contract_type" value="commercial">

                            <div class="form-group">
                                <label for="client_company_name" class="control-label col-md-3 col-sm-3 col-xs-12">Company name:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="client_company_name" placeholder="Enter company name">
                                    @if ($errors->has('client_company_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('client_company_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="client_company_address" class="control-label col-md-3 col-sm-3 col-xs-12">Registered address:</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" name="client_company_address" placeholder="Enter company registered address">
                                    @if ($errors->has('client_company_address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('client_company_address') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="contract_date" class="control-label col-md-3 col-sm-3 col-xs-12">Agreement Date:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <datetime type="date" name="contract_date" format="MM-dd-yyyy"
                                    value="{{Carbon\Carbon::today()->format('Y-m-d')}}"
                                    input-class="form-control col-xs-7 col-xs-12" :auto="true" required>
                                </datetime>
                                @if ($errors->has('contract_date'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('contract_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="contract_duration" class="control-label col-md-3 col-sm-3 col-xs-12">Agreement Duration:</label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select class="form-control" name="contract_duration">
                                    @foreach (config('dropdowns.contract_duration') as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="first_name" class="control-label col-md-3 col-sm-3 col-xs-12">First Name: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="hidden" name="contact_id" value="{{ $data['contact_id'] }}">
                                <input type="hidden" name="estimate_id" value="{{ $data['estimate_id'] }}">
                                <input type="hidden" name="base" value="{{ $data['base'] }}">
                                <input type="text" class="form-control" name="first_name"
                                       value="{{ $data['contact_first_name'] }}" readonly="readonly" required>
                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="last_name" class="control-label col-md-3 col-sm-3 col-xs-12">Last Name: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" name="last_name"
                                       value="{{ $data['contact_last_name'] }}" readonly="readonly" required>
                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="address" class="control-label col-md-3 col-sm-3 col-xs-12">Address: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" value="{{ $data['full_address'] }}" readonly="readonly">
                                <input type="hidden" name="street_address" value="{{ $data['street_address'] }}">
                                <input type="hidden" name="city" value="{{ $data['city'] }}">
                                <input type="hidden" name="state" value="{{ $data['state'] }}">
                                <input type="hidden" name="zip" value="{{ $data['zip'] }}">
                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="email" class="form-control" name="email"
                                    value="{{ $data['contact_email'] }}" readonly="readonly" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="control-label col-md-3 col-sm-3 col-xs-12">Phone: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="tel" class="form-control" name="phone"
                                    value="{{ $data['contact_phone'] }}" readonly="readonly"
                                    v-mask="{mask: '(999) 999-9999 [ext:99999]', greedy: true}" required>
                                @if ($errors->has('phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <label for="emergency_contact_name" class="control-label col-md-3 col-sm-3 col-xs-12">Emergency Contact Name: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" class="form-control" name="emergency_contact_name">
                                @if ($errors->has('emergency_contact_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('emergency_contact_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="emergency_contact_phone" class="control-label col-md-3 col-sm-3 col-xs-12">Emergency Contact Phone: </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="tel" class="form-control" name="emergency_contact_phone"
                                       v-mask="{mask: '(999) 999-9999 [ext:99999]', greedy: true}">
                                @if ($errors->has('emergency_contact_phone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('emergency_contact_phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <br>
                        <div class="col-md-8 col-sm-9 col-xs-12 col-md-offset-1">
                            @include('contracts.partials.contract_' . $data['base'] . '_services')

                            <button type="submit" class="btn btn-lg btn-block btn-success">Continue <i class="fas fa-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <script type="text/javascript">
        $(document).ready(function() {
            $('input:radio').change(function() {
                var $self = $(this);
                if ($self.prop('checked')) {
                    $('input[type=radio]').parent().removeClass('btn-primary');
                    $('input[type=radio]').parent().addClass('btn-default');
                    $self.parent().addClass('btn-primary');
                } else {
                    $self.parent().removeClass('btn-default');
                }

                if($self.val() == 'commercial') {
                    $('#client_company_name').show();
                    $('#client_company_address').slideDown();
                } else {
                    $('#client_company_name').hide();
                    $('#client_company_address').slideUp();
                }
            });

            $('#wizard').show();
        });
    </script>
@endsection
