@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Agreements</h3>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Select an agreement base</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30"></p>
                        <ul>
                            @foreach($templates as $key => $template)
                                @if($model == 'contacts')
                                    <li>
                                        <a href="{{ route($model . '.create.contract', ['id' => request()->id, 'base' => $key]) }}?address={{ request()->address }}">{{ $template['name'] }}</a> - <small>{{ $template['description'] }}</small>
                                    </li>
                                @else
                                    <li>
                                        <a href="{{ route($model . '.create.contract', ['id' => request()->id, 'base' => $key]) }}">{{ $template['name'] }}</a> - <small>{{ $template['description'] }}</small>
                                    </li>
                                @endif
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
