@extends('errors.layout')

@section('content')
    <div class="col-md-12">
        <div class="col-middle">
            <div class="text-center text-center">
                <h1 class="error-number">Thank you!</h1>
                <h2>Your service agreement has been signed and it is now being processed by Miami Gas.</h2>
                <p>You will receive an e-mail once the service agreement is ready.</p>
            </div>
        </div>
    </div>
@endsection
