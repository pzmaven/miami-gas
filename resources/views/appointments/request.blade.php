<!DOCTYPE html>
<html lang="en">
<head>
	<title>Request Service - Miami Gas</title>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/request-form.css') }}">
</head>
<body>
	<div class="container-contact100">
        <div class="wrap-contact100">
            <div id="header"">
                <img src="{{ asset('/img/logo.png') }}" alt="Miami Gas" style="display: block; margin-left: auto; margin-right: auto; padding-bottom: 50px;">
                <h3 id="thank-you"></h3>    
            </div>
			<form id="verify-phone" method="POST" class="contact100-form validate-form">
                @csrf
				<span class="contact100-form-title">
					Request Service
                </span>
                
                <h3 style="margin-bottom: 50px; width: 100%; text-align: center;">
                    Please enter your phone number and we will lookup your account
                </h3>

				<div class="wrap-input100 bg1 phone">
					<span class="label-input100 phone-label">Phone number</span>
                    <input class="input100" type="text" name="phone" placeholder="Enter your phone number" required autocomplete="off">
				</div>

				<div class="container-contact100-form-btn">
					<button class="contact100-form-btn">
						<span>
							Next
							<i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
						</span>
					</button>
				</div>
            </form>
            
            <form id="request-service" method="POST" class="contact100-form validate-form" style="display: none;">
                @csrf
                <span class="contact100-form-title">
                    Request Service
                </span>
                
                <div style="width: 100%; text-align: center;">
                    <h3 class="choose-address"></h3>
                    <ul class="addresses"></ul>
                    <br><br>
                </div>

                <div class="wrap-input100 validate-input bg0 rs1-alert-validate message">
                    <span class="label-input100">Message</span>
                    <textarea class="input100" name="message" placeholder="Your message here..." required></textarea>
                    <input type="hidden" name="contact_id" value="">
                </div>

                <div class="container-contact100-form-btn">
                    <button class="contact100-form-btn">
                        <span>
                            Confirm and Request Service
                            <i class="fa fa-long-arrow-right m-l-7" aria-hidden="true"></i>
                        </span>
                    </button>
                </div>
            </form>
		</div>
    </div>
    
    <!-- jQuery -->
    <script src="{{ asset('vendors/jquery/dist/jquery.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            let phoneForm = $('#verify-phone');
            let requestForm = $('#request-service');
            let contact = {};

            function setContactId(value) {
                contact = value;
            }

            phoneForm.on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                var data = phoneForm.serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});

                $.ajax({
                    type: 'POST',
                    url: '/request/verify-phone',
                    data: data,
                    success: function(response) {
                        phoneForm.slideUp();
                        requestForm.slideDown();
                        $('input[name=phone]').val(data.phone);
                        setContactId(response.contact);
                        
                        $('.choose-address').text(response.message);
                        console.log(contact.addresses.length > 1);
                        console.log(contact.addresses);
                        if(contact.addresses.length > 1) {
                            contact.addresses.forEach(function(address) {
                                $('.addresses').append(
                                    '<label class="label-container">'+ contact.fullName + ', ' + address.fullAddress +'<input type="radio" name="address_id" required value="'+ address.id +'"><span class="checkmark"></span></label>'
                                )
                            });
                        } else {
                            contact.addresses.forEach(function(address) {
                                $('.addresses').append(
                                    '<label class="label-container">'+ contact.fullName + ', ' + address.fullAddress +'<input type="radio" name="address_id" checked required value="'+ address.id +'"><span class="checkmark"></span></label>'
                                )
                            });
                        }

                    },
                    error: function(error) {
                        $('.phone-label').text(error.responseJSON.message).css('color', 'red');
                        $('.phone').css('border-color', 'red');
                    },
                });
            });

            requestForm.on('submit', function(e) {
                e.preventDefault();
                e.stopPropagation();

                $('input[name=contact_id]').val(contact.id);

                var requestFormData = requestForm.serializeArray().reduce(function(obj, item) {
                    obj[item.name] = item.value;
                    return obj;
                }, {});

                $.ajax({
                    type: 'POST',
                    url: '/request',
                    data: requestFormData,
                    success: function(response) {
                        requestForm.slideUp();
                        $('#thank-you').text(response.message);
                        $('#thank-you').slideDown();
                    },
                    error: function(error) {
                        console.log(error.responseJSON.message);
                    },
                });
            });
        });
    </script>
</body>
</html>
