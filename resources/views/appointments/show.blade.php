@extends('template.main')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Appointment <small> details</small></h3>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <a href="{{ $appointment->address->contact->path() }}">
                                    <h2>Address: {{ $appointment->address->fullAddress }} - {{ $appointment->address->contact->fullName }}</h2>
                                </a>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a href="{{ $appointment->path . '/edit' }}"><i class="fas fa-edit"></i></a></li>
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li><a class="close-link"><i class="fas fa-times"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="col-md-8 col-sm-12 col-xs-12">
                                    <ul class="stats-overview">
                                        <li>
                                            <span class="name">Urgency</span>
                                            <span class="value {{ $appointment->urgency == 'High' || $appointment->urgency == 'Emergency' ? 'text-danger' : 'text-success' }}">
                                                {{ $appointment->urgency }}
                                            </span>
                                        </li>
                                        <li>
                                            <span class="name">Type</span>
                                            <span class="value text-success">{{ $appointment->type }}</span>
                                        </li>
                                        <li>
                                            <span class="name">Status</span>
                                            <span class="value {{ $appointment->status == 'Missed' || $appointment->status == 'Cancelled' ? 'text-danger' : 'text-success' }}">
                                                {{ $appointment->status }}
                                            </span>
                                        </li>
                                    </ul>
                                    <br />

                                    <div id="mainb" style="height:350px;">
                                        <iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?q='{{ $appointment->address->fullAddress }}'&key=AIzaSyBHGM8d0-mgqq1LXw9CHorgi09LRGzyKjw" allowfullscreen></iframe>
                                    </div>
                                    @if($appointment->address->estimates->count() > 0)
                                    <div>
                                        <h4>Address Estimates</h4>
                                        <div class="x_content">
                                            <estimates-list :initial-estimates="{{ $appointment->address->estimates }}"></estimates-list>
                                        </div>
                                    </div>
                                    @endif

                                    @if ($appointment->activity->count() > 0)
                                        <div class="activity">
                                            <h4>Recent Activity</h4>
                                            <!-- end of user messages -->
                                            <ul class="messages">
                                            @foreach ($appointment->groupedActivities() as $items)
                                                <li>
                                                    <img src="{{ asset($items[0]->user->getAvatar()) }}" class="avatar" alt="Avatar">

                                                    <div class="message_date">
                                                        <h3 class="date text-info">{{ date('d', strtotime($items[0]->created_at)) }}</h3>
                                                        <p class="month">{{ date('M', strtotime($items[0]->created_at)) }}</p>
                                                    </div>

                                                    <div class="message_wrapper">
                                                        <h4 class="heading">
                                                            <a href="{{ $items[0]->user->path() }}">{{ $items[0]->user->name }}</a>
                                                        </h4>
                                                        @foreach ($items as $activity)
                                                            @if($activity->field)
                                                                @if($activity->field == "address_id")
                                                                <blockquote class="message">Changed address from "{{ \App\Address::find($activity->old_value)->street_address }}" to "{{ \App\Address::find($activity->new_value)->street_address }}"</blockquote>
                                                                @elseif($activity->field == "user_id")
                                                                    <blockquote class="message">Changed assignee from "{{ \App\User::find($activity->old_value)->name }}" to "{{ \App\User::find($activity->new_value)->name }}"</blockquote>
                                                                @elseif($activity->field == "time")
                                                                	<blockquote class="message">Changed Time from {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->old_value)->format('h:ia') }} to {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->new_value)->format('h:ia') }}</blockquote>
                                                                @elseif($activity->field == "date")
                                                                	<blockquote class="message">Changed appointment date from {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->old_value)->format('m-d-Y') }} to {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->new_value)->format('m-d-Y') }}</blockquote>
                                                                @else
                                                                    <blockquote class="message">Changed {{ $activity->field }} from "{{ $activity->old_value }}" to "{{ $activity->new_value }}"</blockquote>
                                                                @endif
                                                            @else
                                                                <blockquote class="message">Created an appointment</blockquote>
                                                            @endif
                                                        @endforeach
                                                        <br />
                                                        <p class="url">
                                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }} on {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $activity->created_at)->format('l\\, F d\\, Y h:i A') }}
                                                        </p>
                                                    </div>
                                                </li>
                                            @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                                <calendar-invite-modal :initial-appointment="{{ $appointment }}"></calendar-invite-modal>

                                <!-- start project-detail sidebar -->
                                <div class="col-md-4 col-sm-12 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Details</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="project_detail">
                                                <p class="title">Assigned to</p>
                                                <p><a href="{{ $appointment->user->path() }}">{{ $appointment->user->name }}</a></p>

                                                <p class="title">Appointment Date</p>
                                                <p>{{ $appointment->date ? date('l jS \of F Y', strtotime($appointment->date)) : 'Not set' }}</p>

                                                <p class="title">Appointment Time</p>
                                                <p>{{ $appointment->time ? date('h:ia',strtotime($appointment->time)) : 'Not set' }}</p>

                                                <p class="title">Client Company</p>
                                                <p>{{ $appointment->address->contact->company }}</p>

                                                <p class="class title">Client Phone Number</p>
                                                <p><a href="tel:{{ $appointment->address->contact->phone }}">{{ $appointment->address->contact->phone }}</a></p>
                                                @if($appointment->date)
                                                    <button class="btn btn-sm btn-success" data-toggle="modal" data-target="#calendar-invite-modal"><i class="fas fa-calendar-check"></i> Send Calendar Invite</button>
                                                @endif
                                            </div>

                                            {{-- <h5>Project files</h5>
                                            <ul class="list-unstyled project_files">
                                                <li><a href=""><i class="fa fa-file-word-o"></i> Functional-requirements.docx</a></li>
                                                <li><a href=""><i class="fa fa-file-pdf-o"></i> UAT.pdf</a></li>
                                                <li><a href=""><i class="fa fa-mail-forward"></i> Email-from-flatbal.mln</a></li>
                                                <li><a href=""><i class="fa fa-picture-o"></i> Logo.png</a></li>
                                                <li><a href=""><i class="fa fa-file-word-o"></i> Contract-10_12_2014.docx</a></li>
                                            </ul>
                                            <br />

                                            <div class="text-center mtop20">
                                                <a href="#" class="btn btn-sm btn-primary">Add files</a>
                                                <a href="#" class="btn btn-sm btn-warning">Report contact</a>
                                            </div> --}}
                                        </div>
                                    </div>

                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Notes</h2>
                                            <div class="clearfix"></div>
                                        </div>

                                        <notes :initial-notes="{{ $appointment->notes }}"
                                            :id="{{ $appointment->id }}"
                                            type="{{ get_class($appointment) }}">
                                        </notes>
                                    </div>

                                </div>
                                <!-- end project-detail sidebar -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /page content -->
@endsection
