@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Add Appointment</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Form</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <form id="appointment-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{ route('appointments.store') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address_id">
                                Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <autocomplete-address></autocomplete-address>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_id">
                                Assigned To <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <autocomplete-user :initial-user="{{ auth()->user() }}"></autocomplete-user>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                Type
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="type" name="type" class="form-control col-md-7 col-xs-12">
                                    @foreach(config('dropdowns.appointment_type') as $type)
                                        <option value="{{ $type }}">{{ $type }}</option>
                                    @endforeach
	                            </select>
                             </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="urgency">
                                Urgency
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="urgency" name="urgency" class="form-control col-md-7 col-xs-12">
                                    @foreach(config('dropdowns.urgency') as $urgency)
                                        <option value="{{ $urgency }}">{{ $urgency }}</option>
                                    @endforeach
	                            </select>
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">
                                Status
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="status" name="status" class="form-control col-md-7 col-xs-12">
                                    @foreach(config('dropdowns.appointment_status') as $status)
                                        <option value="{{ $status }}">{{ $status }}</option>
                                    @endforeach
	                            </select>
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="date">
                                Date  <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <datetime type="date" name="date" format="MM-dd-yyyy" required="required" :auto="true" input-class="form-control col-md-7 col-xs-12"></datetime>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="time">
                                Time
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="time" name="time" class="form-control col-md-7 col-xs-12">
                                    @foreach(config('dropdowns.hours') as $value => $title)
                                        <option value="{{ $value }}">{{ $title }}</option>
                                    @endforeach
	                            </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="notes">
                                Notes
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="notes" name="notes" class="form-control" placeholder="Notes"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="invite">
                                Send calendar invite
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="checkbox" name="invite" class="form-control">
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
