@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="row top_tiles">
            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fas fa-headset"></i></div>
                    <div class="count">{{ \App\Lead::thisMonth()->count() }}</div>
                    <h3><a href="{{ route('leads.index') }}">New Leads</a></h3>
                    <p>Total leads received this month</p>
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fas fa-calendar-check"></i></div>
                    <div class="count">{{ \App\Appointment::upcoming()->count() }}</div>
                    <h3><a href="{{ route('appointments.index') }}">Appointments</a></h3>
                    <p>Upcoming appointments</p>
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fas fa-file-invoice-dollar"></i></div>
                    <div class="count">{{ \App\Estimate::active()->count() }}</div>
                    <h3><a href="{{ route('estimates.index') }}">Open Estimates</a></h3>
                    <p>Estimates that are not expired</p>
                </div>
            </div>

            <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="tile-stats">
                    <div class="icon"><i class="fas fa-file-contract"></i></div>
                    <div class="count">{{ \App\Agreement::pending()->count() }}</div>
                    <h3><a href="{{ route('agreements.index') }}/pending">Agreements</a></h3>
                    <p>Pending agreements</p>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Reports <small>Daily progress</small></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-9 col-sm-12 col-xs-12">
                            <br>
                            <div class="demo-container" style="height:280px">
                                <div id="agreements_chart" class="demo-placeholder"></div>
                            </div>

                            <div class="tiles">
                                <div class="col-md-4 tile">
                                    <span>Total Addresses</span>
                                    <h2>{{ \App\Address::count() }}</h2>
                                    <span class="addresses graph" style="height: 160px;">
                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                    </span>
                                </div>
                                <div class="col-md-4 tile">
                                    <span>Total Contacts</span>
                                    <h2>{{ \App\Contact::count() }}</h2>
                                    <span class="contacts graph" style="height: 160px;">
                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                    </span>
                                </div>
                                <div class="col-md-4 tile">
                                    <span>Total Estimates</span>
                                    <h2>{{ \App\Estimate::count() }}</h2>
                                    <span class="estimates graph" style="height: 160px;">
                                        <canvas width="200" height="60" style="display: inline-block; vertical-align: top; width: 94px; height: 30px;"></canvas>
                                    </span>
                                </div>
                            </div>

                        </div>

                        <div class="col-md-3 col-sm-12 col-xs-12">
                            <div>
                                <div class="x_title">
                                    {{-- <h2>Top Users</h2> --}}
                                    <h2>Latest Addresses</h2>
                                    <div class="clearfix"></div>
                                </div>

                                <ul class="list-unstyled top_profiles scroll-view">
                                @foreach (\App\Address::latest()->get()->take(5) as $address)
                                    <li class="media event">
                                        <div class="pull-left" style="margin: 10px;">
                                            <span class="fas fa-map-marked-alt"></span>
                                        </div>
                                        <div class="media-body">
                                            <a class="title" href="{{$address->path()}}">{{ $address->street_address }}</a>
                                            <p><strong>{{ $address->city }}</strong>, {{$address->state}}</p>
                                            <p><small>Contact name: <a href="{{$address->contact->path()}}">{{$address->contact->fullName}}</a></small></p>
                                        </div>
                                    </li>
                                @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
    <!-- Chart.js -->
    <script src="{{ asset('vendors/Chart.js/dist/Chart.min.js') }}"></script>
    <!-- jQuery Sparklines -->
    <script src="{{ asset('vendors/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <!-- Flot -->
    <script src="{{ asset('vendors/Flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('vendors/Flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('vendors/Flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('vendors/Flot/jquery.flot.stack.js') }}"></script>
    <script src="{{ asset('vendors/Flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('vendors/Flot/jquery.flot.tooltip.min.js') }}"></script>
    <!-- Flot plugins -->
    <script src="{{ asset('vendors/flot.orderbars/js/jquery.flot.orderBars.js') }}"></script>
    <script src="{{ asset('vendors/flot-spline/js/jquery.flot.spline.min.js') }}"></script>
    <script src="{{ asset('vendors/flot.curvedlines/curvedLines.js') }}"></script>
    <!-- DateJS -->
    <script src="{{ asset('vendors/DateJS/build/date.js') }}"></script>

    <script type="text/javascript">
        function init_agreements_chart() {
            if (typeof ($.plot) === 'undefined') { return; }

            var agreements_chart_data = [];
            agreements_chart_data = {!! json_encode($reports['agreements']) !!};

            var agreements_chart_settings = {
                grid: {
                    show: true,
                    aboveData: true,
                    color: "#3f3f3f",
                    labelMargin: 10,
                    axisMargin: 0,
                    borderWidth: 0,
                    borderColor: null,
                    minBorderMargin: 5,
                    clickable: true,
                    hoverable: true,
                    autoHighlight: true,
                    mouseActiveRadius: 100
                },
                series: {
                    lines: {
                        show: true,
                        fill: true,
                        lineWidth: 2,
                        steps: false
                    },
                    points: {
                        show: true,
                        radius: 4.5,
                        symbol: "circle",
                        lineWidth: 3.0
                    }
                },
                legend: {
                    position: "ne",
                    margin: [0, -25],
                    noColumns: 0,
                    labelBoxBorderColor: null,
                    labelFormatter: function(label, series) {
                        return label + '&nbsp;&nbsp;';
                    },
                    width: 40,
                    height: 1
                },
                colors: ['#96CA59', '#3F97EB', '#72c380', '#6f7a8a', '#f7cb38', '#5a8022', '#2c7282'],
                shadowSize: 0,
                tooltip: true,
                tooltipOpts: {
                    content: "%x.0, %s: %y.0",
                    xDateFormat: "%m/%d/%y",
                shifts: {
                    x: -30,
                    y: -50
                },
                defaultTheme: false
                },
                yaxis: {
                    min: 0
                },
                xaxis: {
                    mode: "time",
                    minTickSize: [1, "day"],
                    timeformat: "%m/%d/%y",
                    min: agreements_chart_data[0][0],
                    max: agreements_chart_data[agreements_chart_data.length-1][0]
                }
            };

            if ($("#agreements_chart").length){
                $.plot( $("#agreements_chart"),
                [{
                    label: "Total agreements",
                    data: agreements_chart_data,
                    lines: {
                        fillColor: "rgba(150, 202, 89, 0.12)"
                    },
                    points: {
                        fillColor: "#fff" }
                }], agreements_chart_settings);

            }
        }

    	function init_widget_charts() {
    		if(typeof (jQuery.fn.sparkline) === 'undefined'){ return; }
    		console.log('init_sparklines');

    		$(".contacts").sparkline({!! json_encode($reports['contacts']) !!}, {
    			type: 'bar',
    			height: '40',
    			barWidth: 8,
    			barSpacing: 2,
    			barColor: '#26B99A'
    		});

            $(".addresses").sparkline({!! json_encode($reports['addresses']) !!}, {
    			type: 'bar',
    			height: '40',
    			barWidth: 8,
    			barSpacing: 2,
    			barColor: '#26B99A'
    		});

            $(".estimates").sparkline({!! json_encode($reports['estimates']) !!}, {
    			type: 'bar',
    			height: '40',
    			barWidth: 8,
    			barSpacing: 2,
    			barColor: '#26B99A'
    		});
    	};

        $(document).ready(function() {
            init_agreements_chart();
            init_widget_charts();
        });
    </script>
@endsection
