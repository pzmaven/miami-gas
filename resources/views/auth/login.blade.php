@extends('auth.template')

@section('content')
    <div class="animate form login_form">
        <section class="login_content">
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <h1>Login</h1>

                <div>
                    <input type="text" class="form-control" name="email" placeholder="E-mail" value="{{ old('email') }}" required />
                </div>

                <div>
                    <input type="password" class="form-control" name="password" placeholder="Password" required />
                </div>

                <div class="pull-right">
                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                    <label class="form-check-label" for="remember">Remember me</label>
                </div>

                <div>
                    <button class="btn btn-default">Log in</button>
                    <a class="reset_pass" href="{{ route('password.request') }}">Forgot your password?</a>
                </div>

                @include('auth.footer')
            </form>
        </section>
    </div>
@endsection
