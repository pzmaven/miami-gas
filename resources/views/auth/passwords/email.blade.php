@extends('auth.template')

@section('content')
    <div class="animate form login_form">
        <section class="login_content">
            <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                @csrf
                <h1>{{ __('Reset Password') }}</h1>
                <div>
                    <input tid="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div>
                    <button type="submit" class="btn btn-default">
                        {{ __('Send Password Reset Link') }}
                    </button>
                </div>

                @include('auth.footer')
            </form>
        </section>
    </div>
@endsection
