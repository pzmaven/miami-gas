<div class="clearfix"></div>

<div class="separator">
    <div>
        <h1><i class="fa fa-fire"></i> {{ config('app.name') }}</h1>
        <p>Copyright &copy; {{ \Carbon\Carbon::now()->format('Y') }} {{ config('app.name') }}. All rights reserved. Powered by <a href="{{ config('links.digitalhost.url') }}" target="_blank">{{ config('links.digitalhost.title') }}</a></p>
    </div>
</div>
