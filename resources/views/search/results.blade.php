@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Search</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Search results for "{{ $query }}"</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <p class="text-muted font-13 m-b-30"></p>
                    @if(count($addresses))
                        <h3>Addresses</h3>
                        <addresses-list :initial-addresses="{{ $addresses }}"></addresses-list>
                    @endif
                    
                    @if(count($contacts))
                        <h3>Contacts</h3>
                        <contacts-list :initial-contacts="{{ $contacts }}"></contacts-list>
                    @endif

                    @if(count($leads))
                        <h3>Leads</h3>
                        <leads-list :initial-leads="{{ $leads }}"></leads-list>
                    @endif

                    @if(!count($addresses) && !count($contacts) && !count($leads))
                        <p>No results found.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
