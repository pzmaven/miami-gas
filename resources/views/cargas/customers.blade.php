@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cargas Customers</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                	<form>
	                Customer Type:
	                <select name="customertype">
	                    <option>Commercial</option>
	                    <option>Residential</option>
	                    <option>Exchange</option>
                    </select><br />
                    <input type="text" name="firstname" placeholder="Customer's First Name " value="{{\Request::get('firstname')}}" ><br />
                    <input type="text" name="lastname" placeholder=" Customer's Last Name" value="{{\Request::get('lastname')}}"><br />
                    <input type="text" name="email" placeholder=" Customer's email" value="{{\Request::get('email')}}"><br />
                    <input type="text" name="phone" placeholder=" Customer's Phone" value="{{\Request::get('phone')}}"><br />
                    Referral Source : <select name="referral">
	                    <option>FPH</option>
                    </select><br />
					SalesPerson : <select name="salesperson">
	                    <option>Joel Almanzar</option>
                    </select><br />
                    
                    <br />
                    <input type="submit" value="Create a new Customer ">
                	</form>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
