@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Cargas Inventory Report</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                	<form>
                    <input type="text" name="percent_bulkplant" placeholder="Percent in the plant " value="{{\Request::get('percent_bulkplant')}}" ><br>
                    <input type="text" name="percent_bobtail" placeholder="Percent in the truck " value="{{\Request::get('percent_bobtail')}}">
                    <input type="submit" value="Check the difference">
                	</form>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    {!! $inventory !!}
                </div>


            </div>
        </div>
    </div>
</div>
@endsection
