@component('mail::message')
Congratulations!

A new service agreement has been signed by the customer.

The prospective customer is {{ $options['first_name'] }} {{ $options['last_name'] }} at {{ $options['street_address'] }}, {{ $options['city'] }}, {{ $options['state'] }}, {{ $options['zip'] }}.

The customer has been signed by {{ $agreement->client_signature_name_typed }}.

This service agreement has not yet been signed by a company employee.

Please click the button below to review the service agreement. Make sure you are authenticated before clicking the button.

@component('mail::button', ['url' => $agreement->link . '?user=employee'])
View agreement details
@endcomponent

Thank you,<br>
The team at {{ config('app.name') }}
@endcomponent
