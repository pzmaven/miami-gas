@component('mail::message')
Dear {{ $firstName }},

Thank you for signing our service agreement. Here is your link to download the executed document.

@component('mail::button', ['url' => $agreementLink])
Download agreement
@endcomponent

Sincerely,<br>
The team at {{ config('app.name') }}
@endcomponent
