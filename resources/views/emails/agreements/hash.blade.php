@component('mail::message')

{{ $emailBody }}

@component('mail::button', ['url' => $agreementLink])
Review service agreement
@endcomponent

Thank you,<br>
The team at {{ config('app.name') }}
@endcomponent
