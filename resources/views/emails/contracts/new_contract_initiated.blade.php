@component('mail::message')
Congratulations!

A new service agreement has been initiated.

The prospective customer is {{ $contract->first_name }} {{ $contract->last_name }} at {{ $contract->street_address }}, {{ $contract->city }}, {{ $contract->state }}, {{ $contract->zip }}.

The customer has not yet signed.

This service agreement has not yet been signed by a company employee.

Please click the button below to review and sign the service agreement. Make sure you are authenticated before clicking the button.

@component('mail::button', ['url' => $contract->link . '?user=employee'])
service agreement details
@endcomponent

Thank you,<br>
The team at {{ config('app.name') }}
@endcomponent
