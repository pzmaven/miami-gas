@component('mail::message')
    
{{ $emailBody }}

@component('mail::button', ['url' => $contractLink])
Review service agreement
@endcomponent

Thank you,<br>
The team at {{ config('app.name') }}
@endcomponent
