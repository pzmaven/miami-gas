@component('mail::message')
Congratulations!

A new service agreement has been fully executed.

The prospective customer is {{ $contract->first_name }} {{ $contract->last_name }} at {{ $contract->street_address }}, {{ $contract->city }}, {{ $contract->state }}, {{ $contract->zip }}.

The customer has been signed by {{ $contract->client_signature_name_typed }}.

This service agreement has been signed by company employee {{ $contract->company_signature_name_typed }}.

Please click the button below to view the service agreement. Make sure you are authenticated before clicking the button.

@component('mail::button', ['url' => $contract->link . '?user=employee'])
service agreement details
@endcomponent

Thank you,<br>
The team at {{ config('app.name') }}
@endcomponent
