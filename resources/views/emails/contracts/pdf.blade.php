@component('mail::message')
Dear {{ $firstName }},

Please find attached a copy of the service agreement for Miami Gas Services for your records.
Feel free to contact us if you have any questions or concerns.

Thank you,<br>
The team at {{ config('app.name') }}
@endcomponent
