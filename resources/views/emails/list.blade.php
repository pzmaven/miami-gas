@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Emails Logs</h3>
            </div>

        @can('create_contacts')
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search" align="right">
                    <div class="input-group">
                        <a href="{{ route('emails.logs') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
            </div>
        @endcan
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Emails Logs</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">This is the description for email logs listing</p>
                </div>

                <email-logs-list :initial-logs="{{ $logs }}"></email-logs-list>
            </div>
        </div>
    </div>
</div>
@endsection
