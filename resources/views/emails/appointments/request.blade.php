@component('mail::message')
This message was generated automatically because a new service request was received.
Click the button below to view the details.

@component('mail::button', ['url' => env('APP_URL') . $appointment->path()])
View appointment
@endcomponent

The team at {{ config('app.name') }}
@endcomponent
