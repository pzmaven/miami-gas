@component('mail::message')
Hello,

A new appointment has been created for Miami Gas, please find the details attached.

Appointment type: {{ $appointment->type }}<br>
Appointment date: {{ \Carbon\Carbon::createFromFormat('Y-m-d', $appointment->date)->format('m-d-Y') }}<br>
Appointment time: {{ \Carbon\Carbon::createFromFormat('H:i:s', $appointment->time)->format('H:ia') }}<br>
Appointment location: {{ $appointment->address->fullAddress }}<br>

Best regards,<br>
The team at {{ config('app.name') }}
@endcomponent
