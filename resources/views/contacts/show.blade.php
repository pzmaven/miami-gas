@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Contact Information</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Contact <small>details</small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li><a class="close-link"><i class="fas fa-times"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <h3>{{ $contact->fullName }}</h3>

                            <ul class="list-unstyled user_data">
                                <li>
                                    <i class="fas fa-phone user-profile-icon"></i>
                                    <a href="tel:{{ $contact->phoneNumber }}">{{ $contact->phone }}</a>
                                </li>

                                <li>
                                    <i class="far fa-envelope user-profile-icon"></i>
                                    <a href="mailto:{{ $contact->email }}">{{ $contact->email }}</a>
                                </li>

                                <li>
                                    <i class="fa fa-briefcase user-profile-icon"></i>
                                    {{ $contact->company }}
                                </li>
                            </ul>

                            <a href="{{ $contact->path() . '/edit' }}" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Contact</a>
                            <br />
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#contact-addresses" id="contact-addresses-tab" role="tab" data-toggle="tab" aria-expanded="true">Addresses</a>
                                    </li>

                                    <li role="presentation" class="">
                                        <a href="#contact-contracts" id="contact-contracts-tab" role="tab" data-toggle="tab" aria-expanded="false">Agreements</a>
                                    </li>

                                    <li role="presentation" class="">
                                        <a href="#contact-activity" role="tab" id="contact-activity-tab" data-toggle="tab" aria-expanded="false">Activity Feed</a>
                                    </li>

                                    <li role="presentation" class="">
                                        <a href="#contact-notes" role="tab" id="contact-notes-tab" data-toggle="tab" aria-expanded="false">Notes</a>
                                    </li>
                                </ul>

                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="contact-addresses" aria-labelledby="contact-addresses-tab">
                                        <!-- start contact addresses -->
                                        @if(count($contact->addresses))
                                            <table class="data table table-striped no-margin">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Street</th>
                                                        <th>City</th>
                                                        <th>State</th>
                                                        <th>Zip code</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($contact->addresses as $address)
                                                        <tr>
                                                            <td>{{ $loop->iteration }}</td>
                                                            <td>
                                                                <a href="{{ $address->path() }}">{{ $address->street_address }}</a>
                                                            </td>
                                                            <td>{{ $address->city }}</td>
                                                            <td>{{ $address->state }}</td>
                                                            <td>{{ $address->zip }}</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        @else
                                            <span>This contact has no addresses.</span>
                                        @endif

                                        <a data-toggle="modal" data-target="#widget-modal" class="btn btn-success pull-right"><i class="fa fa-plus"></i> New Address</a>
                                        <widget-modal :initial-contact="{{ $contact }}"></widget-modal>
                                        <!-- end contact addresses -->
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="contact-contracts" aria-labelledby="contact-contracts-tab">
                                        <contracts-list :initial-contracts="{{ $contact->contracts }}"></contracts-list>
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="contact-activity" aria-labelledby="contact-activity-tab">
                                        <!-- start recent activity -->
                                        <ul class="messages">
                                            @foreach ($contact->groupedActivities() as $items)
                                                <li>
                                                    <img src="{{ asset('images/img.jpg') }}" class="avatar" alt="Avatar">

                                                    <div class="message_date">
                                                        <h3 class="date text-info">{{ date('d', strtotime($items[0]->created_at)) }}</h3>
                                                        <p class="month">{{ date('M', strtotime($items[0]->created_at)) }}</p>
                                                    </div>

                                                    <div class="message_wrapper">
                                                        <h4 class="heading">{{ $items[0]->user->name }}</h4>
                                                        @foreach ($items as $activity)
                                                            @if($activity->field)
                                                                @if($activity->field == "address_id")
                                                                <blockquote class="message">Changed address from "{{ \App\Address::find($activity->old_value)->street_address }}" to "{{ \App\Address::find($activity->new_value)->street_address }}"</blockquote>
                                                                @elseif($activity->field == "user_id")
                                                                    <blockquote class="message">Changed assignee from "{{ \App\User::find($activity->old_value)->name }}" to "{{ \App\User::find($activity->new_value)->name }}"</blockquote>
                                                                @elseif($activity->field == "time")
                                                                	<blockquote class="message">Changed Time from {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->old_value)->format('h:ia') }} to {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->new_value)->format('h:ia') }}</blockquote>
                                                                @elseif($activity->field == "date")
                                                                	<blockquote class="message">Changed appointment date from {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->old_value)->format('m-d-Y') }} to {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->new_value)->format('m-d-Y') }}</blockquote>
                                                                @else
                                                                    <blockquote class="message">Changed {{ $activity->field }} from "{{ $activity->old_value }}" to "{{ $activity->new_value }}"</blockquote>
                                                                @endif
                                                            @else
                                                                <blockquote class="message">Created this contact</blockquote>
                                                            @endif
                                                        @endforeach
                                                        <br />
                                                        <p class="url">
                                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }} on {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $activity->created_at)->format('l\\, F d\\, Y h:i A') }}
                                                        </p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <!-- end recent activity -->
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="contact-notes" aria-labelledby="contact-notes-tab">
                                        <notes :initial-notes="{{ $contact->notes }}"
                                            :id="{{ $contact->id }}"
                                            type="{{ get_class($contact) }}">
                                        </notes>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
