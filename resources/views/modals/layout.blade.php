<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">ADD JOB ITEMS</h4>
			</div>

			<div class="modal-body">
			@foreach (config('dropdowns.job_items_type') as $type)
				<button type="button" class="btn btn-default" data-toggle="modal" data-target="#{{ strtolower($type) }}" data-type="{{ strtolower($type) }}" data-dismiss="modal">Add {{ $type }}</button>
			@endforeach
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
