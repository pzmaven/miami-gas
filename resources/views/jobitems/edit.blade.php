@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Update JobItem</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Form</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">

                    <form id="contact-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{ $jobitem->path() }}">
                        {!! method_field('patch') !!}
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">
                                Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="name" name="name" value="{{ $jobitem->name }}" required="required" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                Type <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="type" name="type" required="required" class="form-control col-md-7 col-xs-12">
                                    @foreach(config('dropdowns.job_items_type') as $type)
                                        <option value="{{ $type }}" {{ $jobitem->type == $type ? 'selected="selected"' : '' }}>{{ $type }}</option>
                                    @endforeach
	                            </select>
                             </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description_internal">
                                Description Internal
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="description_internal" name="description_internal" value="{{ $jobitem->description_internal }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="description_customer">
                                Description Customer
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="description_customer" name="description_customer" value="{{ $jobitem->description_customer }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="um">
                                UM <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="type" name="um" value="{{ $jobitem->um }}" class="form-control col-md-7 col-xs-12" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="cost">
                                Cost
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="type" name="cost" value="{{ $jobitem->cost }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="price">
                                Price
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="type" name="price" value="{{ $jobitem->price }}" class="form-control col-md-7 col-xs-12">
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Update</button>
                                @can('delete_jobitems')
                                    <delete-button
                                        :data="{{ $jobitem }}"
                                        message="Are you sure you want to delete this job item?"
                                        class="pull-right">
                                    </delete-button>
                                @endcan
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
