@extends('template.main')

@section('content')
    <!-- page content -->
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Lead <small> details</small></h3>
                </div>

                <div class="clearfix"></div>

                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2>Address: {{ $lead->address->street_address }} - {{ $lead->contact->fullName }}</h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li><a href="{{ $lead->path . '/edit' }}"><i class="fas fa-edit"></i></a></li>
                                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                                    <li><a class="close-link"><i class="fas fa-times"></i></a></li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>

                            <div class="x_content">
                                <div class="col-md-8 col-sm-9 col-xs-12">
                                    <ul class="stats-overview">
                                        <li>
                                            <span class="name">Requested Service</span>
                                            <span class="value text-success">
                                                {{ $lead->interest }}
                                            </span>
                                        </li>

                                        <li>
                                            <span class="name">Bucket</span>
                                            <span class="value text-success">{{ $lead->bucket_name }}</span>
                                        </li>

                                        <li>
                                            <span class="name">Date created</span>
                                            <span class="value text-success">
                                                {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $lead->created_at)->format('m-d-Y') }}
                                            </span>
                                        </li>
                                    </ul>
                                    <br />

                                    <div id="mainb" style="height:350px;">
                                        <iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?q='{{ $lead->address->fullAddress }}'&key=AIzaSyBHGM8d0-mgqq1LXw9CHorgi09LRGzyKjw" allowfullscreen></iframe>
                                    </div>
                                    @if($lead->address->estimates->count() > 0)
                                    <div>
                                        <h4>Address Estimates</h4>
                                        <div class="x_content">
                                            <estimates-list :initial-estimates="{{ $lead->address->estimates }}"></estimates-list>
                                        </div>
                                    </div>
                                    @endif

                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active">
                                                    <a href="#lead-schedules" id="lead-schedules-tab" role="tab" data-toggle="tab" aria-expanded="true">Logs / Schedules</a>
                                                </li>
                                            </ul>

                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="lead-schedules" aria-labelledby="lead-schedules-tab">
                                                    <button class="btn btn-sm btn-success" data-toggle="modal" data-activity-type="log" data-target="#leads-activity-modal"><i class="fas fa-calendar-check"></i> Add Activity</button>
                                                    <a href="{{ $lead->path . '/edit' }}" class="btn btn-sm btn-success pull-right"><i class="fa fa-edit"></i> Edit Lead</a>
                                                    <!-- start lead schedules -->
                                                    @if(count($lead->activities))
                                                        <table class="data table table-striped no-margin">
                                                            <thead>
                                                                <tr>
                                                                    <th>#</th>
                                                                    <th>Bucket</th>
                                                                    <th>Activity</th>
                                                                    <th>Added by</th>
                                                                    <th>Date</th>
                                                                    <th>Time</th>
                                                                    <th>Details</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach ($lead->activities as $activity)
                                                                <tr class="{{ $activity->class_name }}">
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>
                                                                        @if($activity->bucket == 1)
                                                                            <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path d="M12.1644 10.5843C11.6416 10.0682 10.9891 10.0682 10.4697 10.5843C10.0734 10.9772 9.67722 11.3701 9.28767 11.7696C9.18112 11.8795 9.09122 11.9028 8.96137 11.8295C8.705 11.6897 8.43198 11.5765 8.18559 11.4233C7.0369 10.7008 6.07467 9.77193 5.22231 8.72648C4.79946 8.20709 4.42322 7.65107 4.16019 7.02513C4.10692 6.89861 4.11691 6.81538 4.22012 6.71217C4.61634 6.32928 5.00256 5.9364 5.39212 5.54353C5.93483 4.9975 5.93483 4.35825 5.38879 3.80889C5.07914 3.49592 4.76949 3.18961 4.45985 2.87664C4.14021 2.55702 3.82391 2.23406 3.50094 1.91776C2.97821 1.40836 2.32562 1.40836 1.80621 1.92109C1.40667 2.31397 1.02377 2.71683 0.61757 3.10304C0.241333 3.4593 0.0515499 3.89545 0.0115955 4.40486C-0.0516656 5.23389 0.151436 6.01631 0.437775 6.77875C1.02377 8.35691 1.91609 9.75861 2.99818 11.0438C4.45985 12.7817 6.20452 14.1568 8.24552 15.149C9.16447 15.5951 10.1167 15.9381 11.1522 15.9947C11.8647 16.0346 12.484 15.8548 12.9801 15.2988C13.3197 14.9193 13.7026 14.573 14.0622 14.2101C14.5949 13.6707 14.5983 13.0181 14.0689 12.4854C13.4363 11.8495 12.8003 11.2169 12.1644 10.5843Z" fill="#73879C"></path>
                                                                                <path d="M11.5284 7.93074L12.757 7.72099C12.5639 6.59231 12.0312 5.57017 11.2221 4.75778C10.3664 3.90211 9.28434 3.36274 8.09237 3.19627L7.91923 4.43149C8.84151 4.56134 9.68055 4.97752 10.3431 5.64008C10.9691 6.26602 11.3786 7.05843 11.5284 7.93074Z" fill="#73879C"></path>
                                                                                <path d="M13.4496 2.59031C12.0312 1.17197 10.2366 0.276344 8.25551 0L8.08238 1.23522C9.79376 1.47495 11.3453 2.25071 12.5706 3.47261C13.7326 4.63459 14.4951 6.10288 14.7714 7.71766L16 7.5079C15.677 5.63675 14.7947 3.93874 13.4496 2.59031Z" fill="#73879C"></path>
                                                                            </svg>
                                                                        @elseif($activity->bucket == 2)
                                                                            <svg width="16" height="12" viewBox="0 0 16 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path d="M15.9882 1.60671C15.9882 1.58907 16.0008 1.5719 16 1.55446L11.1173 6.09397L15.9941 10.4885C15.997 10.4571 15.9882 10.4254 15.9882 10.3934V1.60671Z" fill="#73879C"></path>
                                                                                <path d="M10.372 6.79254L8.37873 8.64295C8.27786 8.73659 8.14759 8.78354 8.01725 8.78354C7.88963 8.78354 7.762 8.7386 7.66183 8.64852L5.67401 6.86122L0.765349 11.4265C0.884705 11.4679 1.01282 11.5 1.14698 11.5H14.8876C15.0868 11.5 15.2737 11.4414 15.4361 11.3541L10.372 6.79254Z" fill="#73879C"></path>
                                                                                <path d="M8.01155 7.58776L15.4632 0.661713C15.2945 0.564927 15.0981 0.5 14.8876 0.5H1.14698C0.872887 0.5 0.621733 0.602689 0.426121 0.759305L8.01155 7.58776Z" fill="#73879C"></path>
                                                                                <path d="M0 1.7754V10.3932C0 10.4922 0.0235652 10.5875 0.0491464 10.6787L4.89691 6.17432L0 1.7754Z" fill="#73879C"></path>
                                                                            </svg>
                                                                        @elseif($activity->bucket == 3)
                                                                            <svg width="12" height="17" viewBox="0 0 12 17" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                                <path d="M6 0.5C2.69158 0.5 0 3.09944 0 6.29457C0 10.2598 5.36941 16.081 5.59802 16.3269C5.81275 16.5579 6.18764 16.5575 6.40198 16.3269C6.63059 16.081 12 10.2598 12 6.29457C11.9999 3.09944 9.30839 0.5 6 0.5ZM6 9.20998C4.33545 9.20998 2.98128 7.90213 2.98128 6.29457C2.98128 4.687 4.33549 3.37919 6 3.37919C7.66452 3.37919 9.01869 4.68704 9.01869 6.2946C9.01869 7.90217 7.66452 9.20998 6 9.20998Z" fill="#73879C"></path>
                                                                            </svg>
                                                                        @endif

                                                                        {{ $activity->bucket_name }}
                                                                    </td>
                                                                    <td>{{ $activity->activity_name }}</td>
                                                                    <td>
                                                                        <a href="{{ $activity->user->path() }}">{{ $activity->user->name }}</a>
                                                                    </td>
                                                                    <td>{{ date('m-d-Y', strtotime($activity->date)) }}</td>
                                                                    <td>{{ date('h:ia', strtotime($activity->time)) }}</td>
                                                                    <td>{{ $activity->notes }}</td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    @else
                                                        <span>This lead has no logged or scheduled activities.</span>
                                                    @endif
                                                <!-- end lead schedules -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <leads-activity-modal :initial-lead="{{ $lead }}"></leads-activity-modal>

                                <!-- start project-detail sidebar -->
                                <div class="col-md-4 col-sm-3 col-xs-12">
                                    <div class="x_panel">
                                        <div class="x_title">
                                            <h2>Contact Information</h2>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="panel-body">
                                            <div class="project_detail">
                                                <p class="title">Contact name</p>
                                                <p><a href="{{ $lead->contact->path() }}">{{ $lead->contact->fullName }}</a></p>

                                                <p class="title">Contact phone</p>
                                                <p><a href="tel:{{ $lead->contact->phoneNumber }}">{{ $lead->contact->phone }}</a></p>

                                                <p class="title">Contact email</p>
                                                <p><a href="mailto:{{ $lead->contact->email }}">{{ $lead->contact->email }}</a></p>

                                                <p class="title">Contact company</p>
                                                <p>{{ $lead->contact->company }}</p>

                                                <p class="title">Tags</p>
                                                <p>
                                                    @if(count($lead->tags))
                                                        @foreach($lead->tags as $tag)
                                                            <span class="label label-success">{{ $tag->name }}</span>
                                                        @endforeach
                                                    @else
                                                        <span>This lead has no tags attached</span>
                                                    @endif

                                                </p>

                                                <p class="title">Assignees</p>
                                                <p>
                                                    @if(count($lead->assignees))
                                                        @foreach($lead->assignees as $assignee)
                                                            <a href="{{ $assignee->path }}" class="label label-primary">{{ $assignee->name }}</a>
                                                        @endforeach
                                                    @else
                                                        <span>This lead was not assigned to any user</span>
                                                    @endif
                                                </p>

                                                <p class="title">Note</p>
                                                <p>{{ $lead->lead_notes }}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <notes :initial-notes="{{ $lead->contact->notes }}"
                                        :id="{{ $lead->contact->id }}"
                                        type="{{ get_class($lead->contact) }}">
                                    </notes>

                                </div>
                                <!-- end project-detail sidebar -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /page content -->
@endsection
