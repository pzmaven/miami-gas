@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Leads</h3>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Leads <span><strong>{{ count($leads) }}</strong></span></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        Currently displaying bucket: "{{ !empty(request()->bucket) && array_key_exists(request()->bucket, $buckets) ? ucfirst($buckets[request()->bucket]) : 'All' }}"
                        Sorted by: "{{ !empty(request()->sort_by) ? ucfirst(request()->sort_by) : 'Date entered' }} {{ !empty(request()->sort_order) ? ucfirst(request()->sort_order) : 'Desc' }}"
                    </p>

                    <div class="filters">
                        <div class="row">
                            <div class="col-md-9">
                                <form class="form-inline" method="GET">
                                    <div class="form-group">
                                        <select class="form-control" name="bucket">
                                            <option value="0" selected>All</option>
                                            @foreach($buckets as $key => $bucket)
                                                <option value="{{ $key }}" {{ old('bucket', request()->bucket) == $key ? 'selected' : '' }}>{{ $bucket }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="sort_by">
                                            <option disabled>Sort by (Name / Priority)</option>
                                            <option value="name" {{ old('sort_by', request()->sort_by) == 'name' ? 'selected' : '' }}>Name</option>
                                            <option value="priority" {{ old('sort_by', request()->sort_by) == 'priority' ? 'selected' : '' }}>Priority</option>
                                            <option value="created_at" {{ old('sort_by', request()->sort_by) == 'created_at' ? 'selected' : '' }}>Date Entered</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="sort_order">
                                            <option disabled>Asc / Desc</option>
                                            <option value="asc" {{ old('sort_order', request()->sort_order) == 'asc' ? 'selected' : '' }}>Asc</option>
                                            <option value="desc" {{ old('sort_order', request()->sort_order) == 'desc' ? 'selected' : '' }}>Desc</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md btn-primary">Filter</button>
                                        <a href="{{ route('leads.index') }}" class="btn btn-md btn-default">Reset</a>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-3 pull-right">
                                <form class="form-inline">
                                    <div class="form-group">
                                        <input type="text" name="search_leads" class="form-control" placeholder="Search by name, address, tags etc">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <hr>

                    <leads-list :initial-leads="{{ json_encode($leads) }}"></leads-list>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
