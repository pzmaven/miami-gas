@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Leads</h3>
            </div>

        @can('create_contacts')
            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search" align="right">
                    <div class="input-group">
                        <a href="{{ route('leads.create') }}" class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
            </div>
        @endcan
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Leads <span class="text-primary"><strong>({{ count($leads) }})</strong></span></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i> Filters</a></li>
                    </ul>
                    <div class="clearfix"></div>

                    <p class="text-muted font-13 m-b-30">
                        Currently displaying bucket: "{{ !empty(request()->bucket) && array_key_exists(request()->bucket, $buckets) ? ucfirst($buckets[request()->bucket]) : 'All' }}"
                        Sorted by: "{{ !empty(request()->sort_by) ? ucfirst(request()->sort_by) : 'Date entered' }} {{ !empty(request()->sort_order) ? ucfirst(request()->sort_order) : 'Desc' }}"
                    </p>
                </div>

                <div class="x_content">
                    <div class="filters">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <form class="form-inline" method="GET">
                                    <div class="form-group">
                                        <select class="form-control" name="assignee">
                                            <option value="0" selected>All Assignees</option>
                                            @foreach($users as $key => $assignee)
                                                <option value="{{ $assignee->id }}" {{ old('assignee', request()->assignee) == $assignee->id ? 'selected' : '' }}>{{ $assignee->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="bucket">
                                            <option value="0" selected>All Buckets</option>
                                            @foreach($buckets as $key => $bucket)
                                                <option value="{{ $key }}" {{ old('bucket', request()->bucket) == $key ? 'selected' : '' }}>{{ $bucket }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="sort_by">
                                            <option disabled>Sort by (Name / Priority)</option>
                                            <option value="name" {{ old('sort_by', request()->sort_by) == 'name' ? 'selected' : '' }}>Sort by Name</option>
                                            <option value="priority" {{ old('sort_by', request()->sort_by) == 'priority' ? 'selected' : '' }}>Sort by Priority</option>
                                            <option value="created_at" {{ old('sort_by', request()->sort_by) == 'created_at' ? 'selected' : '' }}>Sort by Date</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <select class="form-control" name="sort_order">
                                            <option disabled>Asc / Desc</option>
                                            <option value="asc" {{ old('sort_order', request()->sort_order) == 'asc' ? 'selected' : '' }}>Asc</option>
                                            <option value="desc" {{ old('sort_order', request()->sort_order) == 'desc' ? 'selected' : '' }}>Desc</option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-md btn-primary">Filter</button>
                                        <a href="{{ route('leads.index') }}" class="btn btn-md btn-default">Reset</a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="x_panel">
                <div class="x_content">
                    <leads-list :initial-leads="{{ json_encode($leads) }}"></leads-list>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
