@extends('template.main')

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <h3>Update Lead</h3>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Form</h2>
                        <div class="clearfix"></div>
                    </div>

                    <div class="x_content">
                        <form id="contact-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{ $lead->path() }}">
                            {!! method_field('patch') !!}
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                    Contact name
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" disabled value="{{ $lead->contact->fullName }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                    Company name
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" disabled value="{{ $lead->contact->company }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                    Phone number
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" disabled value="{{ $lead->contact->phone }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                    Email
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" disabled value="{{ $lead->contact->email }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                    Address
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" class="form-control" disabled value="{{ $lead->address->fullAddress }}">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="type">
                                    Bucket <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="bucket" name="bucket" required="required" class="form-control col-md-7 col-xs-12">
                                        @foreach(config('leads.buckets') as $key => $bucket)
                                            <option value="{{ $key }}" {{ $lead->bucket == $key ? 'selected="selected"' : '' }}>{{ $bucket }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="priority">
                                    Priority <span class="required">*</span>
                                </label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select id="priority" name="priority" required="required" class="form-control col-md-7 col-xs-12">
                                        @foreach(config('leads.priorities') as $key => $priority)
                                            <option value="{{ $key }}" {{ $lead->priority == $key ? 'selected="selected"' : '' }}>{{ $priority }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tags</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <autocomplete-tags :initial-tags="{{ json_encode($lead->tags()->get()->toArray()) }}"></autocomplete-tags>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="priority">Note</label>
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <textarea class="form-control" name="lead_notes" cols="30" rows="5">{{ $lead->lead_notes }}</textarea>
                                </div>
                            </div>

                            <div class="ln_solid"></div>

                            <div class="form-group">
                                <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                    <button type="submit" class="btn btn-success">Update</button>
                                    <delete-button
                                        :data="{{ $lead }}"
                                        message="Are you sure you want to delete this lead?"
                                        class="pull-right">
                                    </delete-button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
