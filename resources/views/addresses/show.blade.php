@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Address Information</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>{{ $address->fullAddress }}</h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                            <li><a class="close-link"><i class="fas fa-times"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                            <h3>
                                <a href="{{ $address->contact->path() }}">{{ $address->contact->fullName }}</a>
                            </h3>

                            <ul class="list-unstyled user_data">
                                <li>
                                    <i class="fas fa-phone user-profile-icon"></i>
                                    <a href="tel:{{ $address->contact->phoneNumber }}">{{ $address->contact->phone }}</a>
                                </li>

                                <li>
                                    <i class="far fa-envelope user-profile-icon"></i>
                                    <a href="mailto:{{ $address->contact->email }}">{{ $address->contact->email }}</a>
                                </li>

                                <li>
                                    <i class="fa fa-briefcase user-profile-icon"></i>
                                    {{ $address->contact->company }}
                                </li>
                            </ul>

                            @can('update_addresses')
                                <a href="{{ $address->path() . '/edit' }}" class="btn btn-success"><i class="fa fa-edit m-right-xs"></i>Edit Address</a>
                            @endcan
                            <br />
                        </div>

                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Map</a>
                                    </li>

                                    <li role="presentation" class="">
                                        <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Activity Feed</a>
                                    </li>

                                    <li role="presentation" class="">
                                        <a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Notes</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                        <!-- start address map -->
                                        <div id="mainb" style="height:350px;">
                                            <iframe width="100%" height="350" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/search?q='{{ $address->fullAddress }}'&key=AIzaSyBHGM8d0-mgqq1LXw9CHorgi09LRGzyKjw" allowfullscreen></iframe>
                                        </div>
                                        </table>
                                        <!-- end address map -->
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                        <!-- start recent activity -->
                                        <ul class="messages">
                                            @foreach ($address->groupedActivities() as $items)
                                                <li>
                                                    <img src="{{ asset('images/img.jpg') }}" class="avatar" alt="Avatar">

                                                    <div class="message_date">
                                                        <h3 class="date text-info">{{ date('d', strtotime($items[0]->created_at)) }}</h3>
                                                        <p class="month">{{ date('M', strtotime($items[0]->created_at)) }}</p>
                                                    </div>

                                                    <div class="message_wrapper">
                                                        <h4 class="heading">{{ $items[0]->user->name }}</h4>
                                                        @foreach ($items as $activity)
                                                            @if($activity->field)
                                                                @if($activity->field == "address_id")
                                                                <blockquote class="message">Changed address from "{{ \App\Address::find($activity->old_value)->street_address }}" to "{{ \App\Address::find($activity->new_value)->street_address }}"</blockquote>
                                                                @elseif($activity->field == "user_id")
                                                                    <blockquote class="message">Changed assignee from "{{ \App\User::find($activity->old_value)->name }}" to "{{ \App\User::find($activity->new_value)->name }}"</blockquote>
                                                                @elseif($activity->field == "time")
                                                                	<blockquote class="message">Changed Time from {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->old_value)->format('h:ia') }} to {{ \Carbon\Carbon::createFromFormat('H:i:s', $activity->new_value)->format('h:ia') }}</blockquote>
                                                                @elseif($activity->field == "date")
                                                                	<blockquote class="message">Changed appointment date from {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->old_value)->format('m-d-Y') }} to {{ \Carbon\Carbon::createFromFormat('Y-m-d', $activity->new_value)->format('m-d-Y') }}</blockquote>
                                                                @else
                                                                    <blockquote class="message">Changed {{ $activity->field }} from "{{ $activity->old_value }}" to "{{ $activity->new_value }}"</blockquote>
                                                                @endif
                                                            @else
                                                                <blockquote class="message">Created this address</blockquote>
                                                            @endif
                                                        @endforeach
                                                        <br />
                                                        <p class="url">
                                                            {{ \Carbon\Carbon::parse($activity->created_at)->diffForHumans() }} on {{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $activity->created_at)->format('l\\, F d\\, Y h:i A') }}
                                                        </p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <!-- end recent activity -->
                                    </div>

                                    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                                        <notes :initial-notes="{{ $address->notes }}"
                                            :id="{{ $address->id }}"
                                            type="{{ get_class($address) }}">
                                        </notes>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
