@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <h3>Update Address</h3>
            </div>
        </div>

        <div class="clearfix"></div>

        <update-address :initial-address="{{ $address }}"></update-address>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Form</h2>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <form id="contact-form" data-parsley-validate class="form-horizontal form-label-left" method="POST" action="{{ $address->path() }}">
                        {!! method_field('patch') !!}
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_id">
                                Contact Name <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="contact_id" name="contact_id" value="{{ $address->contact->fullName }}" class="form-control col-md-7 col-xs-12" disabled="disabled">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street_address">
                                Street Address <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="street_address" name="street_address" value="{{ $address->street_address }}" required="required" class="form-control col-md-7 col-xs-12" disabled="disabled">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="street_address">
                                Suite #
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="street_address" name="suite_no" value="{{ $address->suite_no }}" class="form-control col-md-7 col-xs-12" disabled="disabled">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="city">
                                City <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="city" name="city" value="{{ $address->city }}" required="required" class="form-control col-md-7 col-xs-12" disabled="disabled">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="state">
                                State <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="state" name="state" value="{{ $address->state }}" required="required" class="form-control col-md-7 col-xs-12" disabled="disabled">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="zip">
                                Zip Code <span class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="zip" name="zip" value="{{ $address->zip }}" required="required" class="form-control col-md-7 col-xs-12" disabled="disabled">
                            </div>
                        </div>

                        <div class="ln_solid"></div>

                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a class="btn btn-success" data-toggle="modal" data-target="#updateAddress">Edit this address</a>
                                @can('delete_addresses')
                                    <delete-button
                                        :data="{{ $address }}"
                                        message="Are you sure you want to delete this address"
                                        class="pull-right">
                                    </delete-button>
                                @endcan
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
