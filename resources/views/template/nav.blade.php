<div class="top_nav">
    <div class="nav_menu">
        <nav>
            <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
            </div>

            <ul class="nav navbar-nav navbar-right">
                <li class="">
                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                        <img src="{{ asset(auth()->user()->getAvatar()) }}" class="img-circle"><span class="hidden-xs">{{ auth()->user()->name }}</span>
                        <i class="fas fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                        <li><a href="{{ auth()->user()->path() }}"> Profile</a></li>
                        <li>
                            <a href="javascript:;">
                                <span class="badge bg-red pull-right">50%</span>
                                <span>Settings</span>
                            </a>
                        </li>
                        <li><a href="{{ route('logout') }}"
	                        onclick="event.preventDefault();
		                    document.getElementById('logout-form').submit();">
			                    <i class="fas fa-sign-out-alt pull-right"></i> Log Out</a>
			                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;"><input type="hidden" name="_token" value="{{ Session::token() }}"></form>
			            </li>
                    </ul>
                </li>

                <li role="presentation" class="dropdown" v-if="false">
                    <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                        <i class="far fa-envelope"></i>
                    </a>
                </li>

                <notifications :initial-user="{{ auth()->user() }}" v-cloak></notifications>
                
                <search></search>
            </ul>
        </nav>
    </div>
</div>