<div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
        <a href="{{ route('dashboard')}}" class="site_title"><i class="fa fa-fire"></i> <span>{{ config('app.title') }}</span></a>
    </div>

    <div class="clearfix"></div>

    <!-- menu profile quick info -->
    <div class="profile clearfix">
        <div class="profile_pic">
            <img src="{{ asset(auth()->user()->getAvatar()) }}" class="img-circle profile_img">
        </div>

        <div class="profile_info">
            <span>Welcome,</span>
            <h2>{{ auth()->user()->name }}</h2>
        </div>
    </div>
    <!-- /menu profile quick info -->
    <br />

    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
        <div class="menu_section">
            <h3>General</h3>
            <ul class="nav side-menu">
                <li><a href="{{ route('dashboard') }}"><i class="fas fa-home"></i> Home</a></li>
                
                 <li>
                    <a><i class="fas fa-file-invoice-dollar"></i> Cargas <span class="fas fa-angle-down pull-right"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('cargas.inventory') }}"><span class="fas fa-sort-alpha-down"></span> Inventory</a></li>
                        <li><a href="{{ route('cargas.customers') }}"><span class="fas fa-sort-alpha-down"></span> Customers</a></li>
                        
                    </ul>
                </li>


                <li>
                    <a><i class="fas fa-users"></i> CRM <span class="fas fa-angle-down pull-right"></span></a>
	                <ul class="nav child_menu">
		                <li><a href="{{ route('leads.index') }}"><span class="fas fa-headset"></span> Leads</a></li>
		                <li><a href="{{ route('contacts.index') }}"><span class="fas fa-address-book"></span> Contacts</a></li>
		                <li><a href="{{ route('addresses.index') }}"><span class="fas fa-map-marked-alt"></span> Addresses</a></li>
                        <li><a href="{{ route('appointments.requests') }}"><span class="fas fa-calendar-check"></span> Service Requests</a></li>
                        <li><a href="{{ route('appointments.index') }}"><span class="fas fa-calendar-check"></span> Appointments</a></li>
	                </ul>
                </li>

                <li>
                    <a><i class="fas fa-file-contract"></i> Agreements <span class="fas fa-angle-down pull-right"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('agreements.index') }}"><span class="fas fa-sort-alpha-down"></span> All</a></li>
                        <li><a href="{{ route('agreements.signed') }}"><span class="fas fa-check"></span> Signed</a></li>
                        <li><a href="{{ route('agreements.pending') }}"><span class="fas fa-tasks"></span> Pending</a></li>
                    </ul>
                </li>

                <li>
                    <a><i class="fas fa-file-invoice-dollar"></i> Estimates <span class="fas fa-angle-down pull-right"></span></a>
                    <ul class="nav child_menu">
                        <li><a href="{{ route('estimates.index') }}"><span class="fas fa-sort-alpha-down"></span> All</a></li>
                        <li><a href="{{ route('estimates.active') }}"><span class="fas fa-hourglass-start"></span> Active</a></li>
                        <li><a href="{{ route('estimates.expired') }}"><span class="fas fa-hourglass-end"></span> Expired</a></li>
                    </ul>
                </li>
                               
                
                
                

                <li><a href="{{ route('tickets.index') }}"><i class="fas fa-clipboard-list"></i> Tickets</a></li>
            </ul>
        </div>

        @if(auth()->user()->hasRole('admin') || auth()->user()->isSuperAdmin)
            <div class="menu_section">
                <h3>Admin</h3>
                <ul class="nav side-menu">
                @can('read_roles')
                    <li>
                        <a><i class="fas fa-users"></i> Users <span class="fas fa-angle-down pull-right"></span></a>
                        <ul class="nav child_menu">
                            <li><a href="{{ route('users.index') }}">List</a></li>
                            <li><a href="{{ route('roles.index') }}">Roles</a></li>
                            <li><a href="{{ route('permissions.index') }}">Permissions</a></li>
                        </ul>
                    </li>
                @elsecan('read_users')
                    <li><a href="{{ route('users.index') }}"><i class="fas fa-users"></i> Users</a></li>
                @endcan
                	<li><a href="{{ route('filemanager.index') }}"><i class="fas fa-folder-open"></i> File Manager</a></li>
                	<li><a href="{{ route('jobitems.index') }}"><i class="fas fa-hand-holding-usd"></i> Job Items</a></li>
                    <li><a href="{{ route('emails.logs') }}"><i class="far fa-envelope"></i> Email Logs</a></li>
                </ul>
            </div>
        @endif
    </div>
    <!-- /sidebar menu -->

    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
        <a data-toggle="tooltip" data-placement="top" title="Settings">
            <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
        </a>

        <a data-toggle="tooltip" data-placement="top" title="FullScreen">
            <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
        </a>

        <a data-toggle="tooltip" data-placement="top" title="Lock">
            <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
        </a>

        <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
            <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
        </a>
    </div>
    <!-- /menu footer buttons -->
</div>
