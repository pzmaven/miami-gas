@extends('template.main')

@section('content')
<div class="right_col" role="main">
    <div class="">
        @include('tickets.nav')

        <div class="page-title">
            <div class="title_left">
                <h3>Cleared Tickets</h3>
            </div>

            <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search" align="right">
                    <div class="input-group">
                        <a data-toggle="modal" data-target="#create-new-ticket"class="btn btn-success pull-right"><i class="fa fa-plus"></i> Add New</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Cleared Tickets List</h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>

                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">A cleared ticket is one that was filled and paid but not yet picked up.</p>
                </div>

                <tickets-list :initial-tickets="{{ $tickets }}"></tickets-list>
            </div>
        </div>
    </div>
</div>
@endsection
