<div class="col-xs-12 col-sm-12 col-md-12">
    <a href="{{ route('tickets.index') }}" class="btn btn-default {{ url()->current() == route('tickets.index') ? 'active' : '' }}">All</a>
    <a href="{{ route('tickets.open') }}" class="btn btn-default {{ url()->current() == route('tickets.open') ? 'active' : '' }}">Open</a>
    <a href="{{ route('tickets.submitted') }}" class="btn btn-default {{ url()->current() == route('tickets.submitted') ? 'active' : '' }}">Submitted</a>
    <a href="{{ route('tickets.cleared') }}" class="btn btn-default {{ url()->current() == route('tickets.cleared') ? 'active' : '' }}">Cleared</a>
    <a href="{{ route('tickets.closed') }}" class="btn btn-default {{ url()->current() == route('tickets.closed') ? 'active' : '' }}">Closed</a>
</div>