# Miami Gas

Software to generate estimates and service agreements for Miami Gas

#File Manager Command Please RUN
php artisan vendor:publish --tag=lfm_public

# Gmail configuration
```
MAIL_DRIVER=smtp
MAIL_HOST=smtp.gmail.com
MAIL_PORT=587
MAIL_USERNAME=mail
MAIL_PASSWORD=password
MAIL_ENCRYPTION=tls
MAIL_FROM_ADDRESS=no-repeat@miamigas.com
MAIL_FROM_NAME=miami
```

# Mailcatcher configuration
```
MAIL_DRIVER=smtp
MAIL_HOST=mailcatcher
MAIL_PORT=1025
```
