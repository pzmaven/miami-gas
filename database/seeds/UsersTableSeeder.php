<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $user = User::firstOrNew(['email' => 'pzmaven@gmail.com']);
        if (!$user->exists) {
            $user->fill([
                'name' => 'Joel',
                'email' => 'pzmaven@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(60),
            ])->save();
        }

        $user = User::firstOrNew(['email' => 'daniel.rogobete@gmail.com']);
        if (!$user->exists) {
            $user->fill([
                'name' => 'Daniel',
                'email' => 'daniel.rogobete@gmail.com',
                'password' => bcrypt('password'),
                'remember_token' => str_random(60),
            ])->save();
        }

    }
}
