<?php

use Illuminate\Database\Seeder;


class RoleUsersTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
	
	//Make the first user a superadmin     
    $exists = DB::table('role_user')->where('user_id', '1')->first();

	if(!$exists){
     DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '1',
        ]);
    }
    
    //Make the second user a superadmin     
    $exists = DB::table('role_user')->where('user_id', '2')->first();

	if(!$exists){
     DB::table('role_user')->insert([
            'role_id' => '1',
            'user_id' => '2',
        ]);
    }
    
    
    
    }
}
