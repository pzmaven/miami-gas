<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = [
            'addresses',
            'appointments',
            'contacts',
            'contracts',
            'estimates',
            'jobitems',
            'notes',
            'permissions',
            'roles',
            'users',
        ];

        foreach ($models as $model) {
            $actions = ['read', 'update', 'create', 'delete'];

            foreach ($actions as $action) {
                $permission = Permission::firstOrNew(['name' => "{$action}_{$model}"]);

                if (!$permission->exists) {
                    $permission->fill([
                        'name' => "{$action}_{$model}",
                        'description' => "Give permission to {$action} the {$model}.",
                    ])->save();
                }
            }
        }
    }
}
