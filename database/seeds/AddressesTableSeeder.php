<?php

use Illuminate\Database\Seeder;
use App\Address;
use App\Contact;

class AddressesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $address = Address::firstOrNew(['id' => 1]);
        if (!$address->exists) {
            $address->fill([
                'contact_id' => App\Contact::where('first_name', 'Joel')->first()->id,
                'street_address' =>'13501 SW 128th Street',
                'city' => 'Miami',
                'state' => 'FL',
                'zip' => '33186',
            ])->save();
        }

        $address = Address::firstOrNew(['id' => 2]);
        if (!$address->exists) {
            $address->fill([
                'contact_id' => App\Contact::where('first_name', 'Jason')->first()->id,
                'street_address' =>'656 Alhambra Circle',
                'city' => 'Coral Gables',
                'state' => 'FL',
                'zip' => '33134',
            ])->save();
        }

        $address = Address::firstOrNew(['id' => 3]);
        if (!$address->exists) {
            $address->fill([
                'contact_id' => App\Contact::where('first_name', 'Maria')->first()->id,
                'street_address' =>'1 Glen Royal Pkwy',
                'city' => 'Miami',
                'state' => 'FL',
                'zip' => '33125',
            ])->save();
        }
    }
}
