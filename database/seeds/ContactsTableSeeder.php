<?php

use Illuminate\Database\Seeder;
use App\Contact;


class ContactsTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $contact = Contact::firstOrNew(['email' => 'joel@miamigas.com']);
        if (!$contact->exists) {
            $contact->fill([
                'first_name' => 'Joel',
                'last_name' => 'Almanzar',
                'email' => 'joel@miamigas.com',
                'phone' => '7862827200',
                'company' => 'Miami Gas',
            ])->save();
        }

        $contact = Contact::firstOrNew(['email' => 'jason@miamigas.com']);
        if (!$contact->exists) {
            $contact->fill([
                'first_name' => 'Jason',
                'last_name' => 'Hertzberg',
                'email' => 'jason@miamigas.com',
                'phone' => '3053019455',
                'company' => 'Miami Gas',
            ])->save();
        }

        $contact = Contact::firstOrNew(['email' => 'silvio@miamigas.com']);
        if (!$contact->exists) {
            $contact->fill([
                'first_name' => 'Silvio',
                'last_name' => 'Fernandez',
                'email' => 'silvio@miamigas.com',
                'phone' => '3053054917',
                'company' => 'Miami Gas',
            ])->save();
        }

        $contact = Contact::firstOrNew(['email' => 'msilv016@fiu.edu']);
        if (!$contact->exists) {
            $contact->fill([
                'first_name' => 'Maria',
                'last_name' => 'Almanzar',
                'email' => 'msilv016@fiu.edu',
                'phone' => '7864997786',
                'company' => 'Miami Dade Medical Research',
            ])->save();
        }

        $contact = Contact::firstOrNew(['email' => 'joselalmanzar@gmail.com']);
        if (!$contact->exists) {
            $contact->fill([
                'first_name' => 'Jose',
                'last_name' => 'Almanzar',
                'email' => 'joselalmanzar@gmail.com',
                'phone' => '7864127217',
                'company' => 'Josly Realty',
            ])->save();
        }
    }
}
