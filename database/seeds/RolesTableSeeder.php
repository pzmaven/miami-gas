<?php

use Illuminate\Database\Seeder;
use App\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     *
     * @return void
     */
    public function run()
    {
        $role = Role::firstOrNew(['name' => 'superadmin']);
        if (!$role->exists) {
            $role->fill([
                'description' => 'Superadmin'
            ])->save();
        }
        $role = Role::firstOrNew(['name' => 'admin']);
        if (!$role->exists) {
            $role->fill([
                'description' => 'Admin'
            ])->save();
        }
	}
}
