<?php

use Illuminate\Database\Seeder;

class SuperadminRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = App\User::get();

        $superadmin = App\Role::firstOrCreate([
            'name' => 'superadmin',
            'description' => 'Superadmin'
        ]);

        foreach ($users as $user) {
            $user->assignRoles($superadmin);
        }
    }
}
