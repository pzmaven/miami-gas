<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimate_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estimate_id')->unsigned();
            $table->integer('job_item_id')->unsigned();
            $table->string('name');
            $table->unsignedDecimal('qty', 10, 2);
            $table->string('um');
            $table->unsignedDecimal('price', 10,2)->nullable();
            $table->unsignedDecimal('price_extended', 10,2)->nullable();
            $table->boolean('taxed')->default(0);
            $table->text('description_customer')->nullable();
            $table->text('description_internal')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimate_items');
    }
}
