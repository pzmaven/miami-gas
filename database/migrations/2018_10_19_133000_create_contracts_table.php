<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('base', 250);
            $table->string('contract_type', 100);
            $table->date('contract_date', 100);
            $table->string('first_name', 100);
            $table->string('last_name', 100);
            $table->string('phone', 50);
            $table->string('email', 100);
            $table->string('street_address', 100);
            $table->string('city', 50);
            $table->string('state', 20);
            $table->string('zip', 20);
            $table->string('emergency_contact_name', 100);
            $table->string('emergency_contact_phone', 50);
            $table->string('client_signature_name_typed', 100);
            $table->string('client_signature_date', 50);
            $table->string('client_signature', 250);
            $table->string('company_signature_name_typed', 100);
            $table->string('company_signature_date', 50);
            $table->string('company_signature', 250);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
