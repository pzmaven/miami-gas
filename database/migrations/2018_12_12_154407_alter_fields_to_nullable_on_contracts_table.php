<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterFieldsToNullableOnContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('client_signature_name_typed', 100)->nullable()->change();
            $table->string('client_signature_date', 50)->nullable()->change();
            $table->string('client_signature', 250)->nullable()->change();
            $table->string('company_signature_name_typed', 100)->nullable()->change();
            $table->string('company_signature_date', 50)->nullable()->change();
            $table->string('company_signature', 250)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->string('client_signature_name_typed', 100)->nullable(false)->change();
            $table->string('client_signature_date', 50)->nullable(false)->change();
            $table->string('client_signature', 250)->nullable(false)->change();
            $table->string('company_signature_name_typed', 100)->nullable(false)->change();
            $table->string('company_signature_date', 50)->nullable(false)->change();
            $table->string('company_signature', 250)->nullable(false)->change();
        });
    }
}
