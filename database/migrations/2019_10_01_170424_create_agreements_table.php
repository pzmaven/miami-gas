<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgreementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agreements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contact_id');
            $table->unsignedBigInteger('address_id');
            $table->string('base', 250);
            $table->json('options')->nullable();
            $table->string('client_signature_name_typed', 100)->nullable();
            $table->string('client_signature_date', 50)->nullable();
            $table->string('client_signature', 250)->nullable();
            $table->string('company_signature_name_typed', 100)->nullable();
            $table->string('company_signature_date', 50)->nullable();
            $table->string('company_signature', 250)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agreements');
    }
}
