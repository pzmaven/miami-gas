<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstimatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('estimates', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('address_id');
            $table->string('prefix')->nullable();
            $table->string('status')->nullable();
            $table->unsignedInteger('salesperson');
            $table->string('job_name')->nullable();
            $table->string('payment_terms');
            $table->text('message_client')->nullable();
            $table->text('message_internal')->nullable();
            $table->decimal('subtotal', 10,2)->nullable()->default(0.00);
            $table->decimal('sales_tax', 10,2)->nullable()->default(0.00);
            $table->decimal('total', 10,2)->nullable()->default(0.00);
            $table->decimal('profit', 10,2)->nullable()->default(0.00);
            $table->string('creator')->nullable();
            $table->date('date_created');
            $table->date('date_expiration');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('estimates');
    }
}
