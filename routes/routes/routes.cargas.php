<?php
/* Cargas Routes */

Route::get('/cargas', 'CargasController@index')->name('cargas.index');
Route::get('/cargas/inventory', 'CargasController@inventory')->name('cargas.inventory');
Route::get('/cargas/customers', 'CargasController@create_customer')->name('cargas.customers');