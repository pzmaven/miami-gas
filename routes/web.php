<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/**
 * Cargas routes
 */
require('routes/routes.cargas.php');



/**
 * Service request public routes
 */
Route::get('/request', 'AppointmentsController@getServiceRequest');
Route::post('/request', 'AppointmentsController@postServiceRequest');
Route::post('/request/verify-phone', 'AppointmentsController@verifyPhone');

/*
 * Contracts public routes - these are the old contract routes
 */
//Route::get('contracts/{id}/sign/{hash}', 'ContractsController@signContractLink')->name('contracts.sign');
//Route::get('contracts/{id}/download/{hash}', 'ContractsController@downloadContractLink');
//Route::put('contracts/sign/email/client', 'ContractsController@signEmailClient');
//Route::get('contracts/thankyou', function() {
//	return view('contracts.thankyou');
//})->name('contracts.thankyou');

/*
 * Agreements public routes
 */
Route::get('agreements/{id}/sign/{hash}', 'AgreementsController@signAgreementLink')->name('agreements.sign');
Route::get('agreements/{id}/download/{hash}', 'AgreementsController@downloadAgreementLink');
Route::put('agreements/sign/email/client', 'AgreementsController@signEmailClient');
Route::get('agreements/thankyou', function() {
    return view('agreements.thankyou');
})->name('agreements.thankyou');

/*
 * User routes
 */
Route::middleware(['user'])->group(function () {
	Route::get('/', 'HomeController@index');
	Route::get('/dashboard', 'HomeController@index')->name('dashboard');

    Route::get('estimates/active', 'EstimatesController@active')->name('estimates.active');
    Route::get('estimates/expired', 'EstimatesController@expired')->name('estimates.expired');
	Route::resource('estimates', 'EstimatesController');
    Route::patch('/estimates/update/column/{item}', 'EstimatesController@updateByColumn');
    Route::get('estimates/{id}/pdf', 'EstimatesController@generatePdf');
    Route::post('estimates/email', 'EstimatesController@sendEstimatePDFByEmail');

    // Agreements
    Route::get('agreements', 'AgreementsController@index')->name('agreements.index');
    Route::get('agreements/create/{base?}', 'AgreementsController@create')->name('agreements.create');
    Route::post('agreements/initiate', 'AgreementsController@initiate')->name('agreements.initiate');
    Route::get('agreements/prepare', 'AgreementsController@prepare')->name('agreements.getPrepare');
    Route::post('agreements/prepare', 'AgreementsController@prepare')->name('agreements.prepare');
    Route::post('agreements/services', 'AgreementsController@services')->name('agreements.services');
    Route::get('agreements/configuration', 'AgreementsController@configuration')->name('agreements.configuration');
    Route::post('agreements/configuration', 'AgreementsController@configuration')->name('agreements.configuration');
    Route::post('agreements/filled/client', 'AgreementsController@showFilledAgreementToClient')->name('agreements.show.client');
    Route::get('agreements/filled/employee', 'AgreementsController@showFilledAgreementToEmployee')->name('agreements.show.employee');
    Route::post('agreements/sign/client', 'AgreementsController@signNowClient');
    Route::post('agreements/sign/employee', 'AgreementsController@signNowEmployee');
    Route::get('agreements/store', 'AgreementsController@store');
    Route::get('agreements/{id}/download', 'AgreementsController@downloadAgreementPDF');
    Route::post('agreements/sign/email', 'AgreementsController@sendAgreementViaEmail');
    Route::post('agreements/resend', 'AgreementsController@resendAgreementLink');
    Route::put('agreements/sign/email/employee', 'AgreementsController@signEmailEmployee');
    Route::get('agreements/pending', 'AgreementsController@getPendingAgreements')->name('agreements.pending');
    Route::get('agreements/signed', 'AgreementsController@getSignedAgreements')->name('agreements.signed');
    Route::get('agreements/{agreement}/', 'AgreementsController@show');
    Route::delete('agreements/{agreement}', 'AgreementsController@destroy');

    // Create contract from estimates, contacts, addresses and leads
    // These are the old routes for contracts.
//	Route::get('estimates/{id}/contract/data/{base?}', 'ContractsController@selectBase')->name('estimates.create.contract');
//	Route::get('contacts/{id}/contract/data/{base?}', 'ContractsController@selectBase')->name('contacts.create.contract');
//	Route::get('addresses/{id}/contract/data/{base?}', 'ContractsController@selectBase')->name('addresses.create.contract');
//	Route::get('leads/{id}/contract/data/{base?}', 'ContractsController@selectBase')->name('leads.create.contract');

//  Route::resource('contracts', 'ContractsController')->only(['index']);
//  Route::post('contracts/services', 'ContractsController@configureServices');
//  Route::post('contracts/filled/client', 'ContractsController@showFilledContractToClient');
//  Route::post('contracts/sign/client', 'ContractsController@signNowClient');
//  Route::get('contracts/filled/employee', 'ContractsController@showFilledContractToEmployee');
//  Route::post('contracts/sign/employee', 'ContractsController@signNowEmployee');
//  Route::get('contracts/store', 'ContractsController@store');
//  Route::get('contracts/{id}/download', 'ContractsController@downloadContractPDF');
//	Route::post('contracts/sign/email', 'ContractsController@sendContractViaEmail');
//	Route::post('contracts/resend', 'ContractsController@resendContractLink');
//  Route::put('contracts/sign/email/employee', 'ContractsController@signEmailEmployee');
//  Route::get('contracts/pending', 'ContractsController@getPendingContracts')->name('contracts.pending');
//  Route::get('contracts/signed', 'ContractsController@getSignedContracts')->name('contracts.signed');
//	Route::get('contracts/migrate', 'ContractsController@migrate')->name('contracts.migrate');
//	Route::get('contracts/{contract}/', 'ContractsController@show');
//	Route::delete('contracts/{contract}', 'ContractsController@destroy');

    Route::resource('lineitems', 'EstimateItemsController');
    Route::patch('/lineitems/update/column/{item}', 'EstimateItemsController@updateByColumn');

    Route::resource('jobitems', 'JobItemsController');

    Route::resource('contacts', 'ContactsController');
    Route::get('/contacts/search/{query}', 'ContactsController@search');

    Route::resource('addresses', 'AddressesController');
    Route::get('/addresses/search/{string}', 'AddressesController@search');

	Route::resource('users', 'UsersController');
	Route::post('/users/{user}/avatar', 'UsersController@updateAvatar');
	Route::post('/users/{user}/change-password', 'UsersController@changePassword');
	Route::get('/users/search/{string}', 'UsersController@search');
	Route::post('/users/roles', 'UsersController@roles')->name('users.roles');
	Route::post('/users/{user}/signature', 'SignaturesController@store')->name('users.signature');
	Route::patch('/users/{user}/signature', 'SignaturesController@update');
	Route::delete('/users/{user}/signature', 'SignaturesController@destroy');
	Route::get('/users/{user}/unread-notifications', 'UsersController@getAllUnreadNotifications');
	Route::post('/users/{user}/read-notification', 'UsersController@markNotificationAsRead');
	Route::post('/users/{user}/read-all-notifications', 'UsersController@markAllNotificationsAsRead');
	Route::post('/users/{user}/send-message', 'MessagesController@send');
	Route::post('/users/send-message', 'MessagesController@send');
	Route::get('/messages/{message}', 'MessagesController@show');
	Route::delete('/messages/{message}', 'MessagesController@destroy');
	Route::patch('/messages/{message}/mark-as-read', 'MessagesController@markMessageAsRead');

	Route::resource('roles', 'RolesController');
	Route::resource('permissions', 'PermissionsController');

	Route::resource('appointments', 'AppointmentsController');
	Route::get('requests', 'AppointmentsController@getAllServiceRequests')->name('appointments.requests');
	Route::post('appointments/{appointment}/calendar-invite', 'AppointmentsController@calendarInvite');

	Route::resource('leads', 'LeadsController');
	Route::post('leads/{lead}/add-activity', 'LeadsController@addActivity');
	Route::post('leads/{lead}/assign/{user}', 'LeadsController@assign');

	Route::resource('notes', 'NotesController');

	Route::get('tickets/tanks', 'TicketsController@getTanks');
	Route::get('tickets/open', 'TicketsController@getOpenTickets')->name('tickets.open');
	Route::get('tickets/submitted', 'TicketsController@getSubmittedTickets')->name('tickets.submitted');
	Route::get('tickets/cleared', 'TicketsController@getClearedTickets')->name('tickets.cleared');
	Route::get('tickets/closed', 'TicketsController@getClosedTickets')->name('tickets.closed');
	Route::get('tickets/get-customer-types', 'TicketsController@getCustomerTypes');
	Route::resource('tickets', 'TicketsController');
	Route::post('tickets/add', 'TicketsController@addTicket');
	Route::patch('tickets/{ticket}/add-items', 'TicketsController@addItems');
	Route::patch('tickets/{ticket}/change-status', 'TicketsController@changeStatus');
	Route::get('tickets/{ticket}/get-ticket-activity', 'TicketsController@getTicketActivity');

	Route::get('search', 'SearchController@search');

	Route::post('shared/directories', 'AttachmentsController@directories');
	Route::post('shared/files', 'AttachmentsController@files');
});

/*
 * Admin routes
 */
Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function() {
	Route::get('/emails/logs', 'EmailLogsController@index')->name('emails.logs');
	Route::delete('/emails/logs/{log}', 'EmailLogsController@destroy');
	Route::get('/filemanager', function() {
		return view('filemanager.index');
	})->name('filemanager.index');
});

Auth::routes();

/*
 * API Requests
 */
Route::get('/api/leads/createrequest', 'APILeadsController@createRequest');
