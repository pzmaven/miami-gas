<?php

return [
    'services' => [
        'forklift_cylinder' => [
            'name' => 'Forklift cylinder',
            'charges' => [
                'price_per_cylinder' => [
                    'name' => 'Price per Cylinder',
                    'amount' => null
                ],
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => null
                ],
            ],
            'qty' => null
        ],

        'forklift_cylinder_cage_rental' => [
            'name' => 'Forklift cylinder cage',
            'charges' => [
                'price_per_forklift_cylinder_cage' => [
                    'name' => 'Monthly Fee',
                    'amount' => null
                ],
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => null
                ],
            ],
            'qty' => null
        ],
    ],

    'cancellation_fees' => [
        'name' => 'Cancellation Fees',
        'charges' => [
            'cancellation_fee' => [
                'name' => 'Cancellation Fee',
                'amount' => null
            ]
        ]
    ],

    'contract_default_values' =>[
        'forklift_cage' => [
            'name' => 'Forklift Cage',
            'charges' => [
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => 0.00
                ],
            ],
        ]
    ],
];
