<?php

return [
    'company_details' => [
        'company_name' => 'Discount Gas LLC, DBA Miami Gas',
        'address' => '2747 NW 21 St. Miami, FL 33142',
        'phone' => '(305) 204-3333',
        'license' => '40396',
        'email' => 'joel@miamigas.com',
    ],

    'templates' => [
        'simple' => [
            'name' => 'Simple terms',
            'description' => 'Simple terms agreement only requires the clients signature',
            'services' => [],
            'cancellation_fees' => [],
            'contract_default_values' => [],
            'form_validator' => [
                'first_name' => 'required',
                'last_name' => 'required',
                'client_company_name' => 'required_if:agreement_type,commercial',
                'client_company_address' => 'required_if:agreement_type,commercial',
                'email' => 'required',
                'phone' => 'required',
                'street_address' => 'required',
                'suite_no' => 'nullable',
                'city' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'agreement_date' => 'required',
                'agreement_duration' => 'required',
                'agreement_type' => 'required',
                'emergency_contact_name' => 'nullable',
                'emergency_contact_phone' => 'nullable',
            ],
            'company_signature_required' => false,
        ],

        'simple_with_options' => [
            'name' => 'Simple terms with options',
            'description' => 'Simple terms with options only requires the clients signature',
            'services' => [
                'propane' => [
                    'name' => 'Propane',
                    'charges' => [
                        'price_per_gallon' => [
                            'name' => 'Price per Gallon',
                            'amount' => null
                        ],
                    ],
                ],

                'tank_lease' => [
                    'name' => 'Tank Lease',
                    'charges' => [
                        'monthly_fee' => [
                            'name' => 'Monthly Fee',
                            'amount' => null
                        ],
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],

                'tank_monitoring' => [
                    'name' => 'Tank Monitoring',
                    'charges' => [
                        'monthly_fee' => [
                            'name' => 'Monthly Fee',
                            'amount' => null
                        ],
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],

                'regulator' => [
                    'name' => 'Regulator',
                    'charges' => [
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],
            ],
            'cancellation_fees' => [
                'name' => 'Cancellation Fees',
                'charges' => [
                    'cancellation_fee' => [
                        'name' => 'Cancellation Fee',
                        'amount' => null
                    ],
                    'piping_reimbursement' => [
                        'name' => 'Piping Reimbursement',
                        'amount' => null
                    ],
                ]
            ],
            'contract_default_values' =>[
                'forklift_cage' => [
                    'name' => 'Forklift Cage',
                    'charges' => [
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => 0.00
                        ],
                    ],
                ]
            ],
            'form_validator' => [
                'agreement_type' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'client_company_name' => 'required_if:agreement_type,commercial',
                'client_company_address' => 'required_if:agreement_type,commercial',
                'email' => 'required',
                'phone' => 'required',
                'street_address' => 'required',
                'suite_no' => 'nullable',
                'city' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'agreement_date' => 'required',
                'agreement_duration' => 'required',
                'emergency_contact_name' => 'nullable',
                'emergency_contact_phone' => 'nullable',
            ],
            'company_signature_required' => false,
        ],

        'forklift' => [
            'name' => 'Forklift',
            'description' => 'Forklift Agreement',
            'services' => [
                'forklift_cylinder' => [
                    'name' => 'Forklift cylinder',
                    'charges' => [
                        'price_per_cylinder' => [
                            'name' => 'Price per Cylinder',
                            'amount' => null
                        ],
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],

                'forklift_cylinder_cage_rental' => [
                    'name' => 'Forklift cylinder cage',
                    'charges' => [
                        'price_per_forklift_cylinder_cage' => [
                            'name' => 'Monthly Fee',
                            'amount' => null
                        ],
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],
            ],
            'cancellation_fees' => [
                'name' => 'Cancellation Fees',
                'charges' => [
                    'cancellation_fee' => [
                        'name' => 'Cancellation Fee',
                        'amount' => null
                    ]
                ]
            ],
            'contract_default_values' =>[
                'forklift_cage' => [
                    'name' => 'Forklift Cage',
                    'charges' => [
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => 0.00
                        ],
                    ],
                ]
            ],
            'form_validator' => [
                'first_name' => 'required',
                'last_name' => 'required',
                'client_company_name' => 'required',
                'client_company_address' => 'required',
                'email' => 'required',
                'phone' => 'required',
                'street_address' => 'required',
                'suite_no' => 'nullable',
                'city' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'agreement_date' => 'required',
                'agreement_duration' => 'required',
                'agreement_type' => 'required',
                'emergency_contact_name' => 'nullable',
                'emergency_contact_phone' => 'nullable',
            ],
            'company_signature_required' => true,
        ],

        'sales' => [
            'name' => 'Sales',
            'description' => 'Sales Agreement',
            'services' => [
                'propane' => [
                    'name' => 'Propane',
                    'charges' => [
                        'price_per_gallon' => [
                            'name' => 'Price per Gallon',
                            'amount' => null
                        ],
                    ],
                ],

                'tank_lease' => [
                    'name' => 'Tank Lease',
                    'charges' => [
                        'monthly_fee' => [
                            'name' => 'Monthly Fee',
                            'amount' => null
                        ],
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],

                'tank_monitoring' => [
                    'name' => 'Tank Monitoring',
                    'charges' => [
                        'monthly_fee' => [
                            'name' => 'Monthly Fee',
                            'amount' => null
                        ],
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],

                'regulator' => [
                    'name' => 'Regulator',
                    'charges' => [
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => null
                        ],
                    ],
                    'qty' => null
                ],
            ],
            'cancellation_fees' => [
                'name' => 'Cancellation Fees',
                'charges' => [
                    'cancellation_fee' => [
                        'name' => 'Cancellation Fee',
                        'amount' => null
                    ],
                    'piping_reimbursement' => [
                        'name' => 'Piping Reimbursement',
                        'amount' => null
                    ],
                ]
            ],
            'contract_default_values' =>[
                'forklift_cage' => [
                    'name' => 'Forklift Cage',
                    'charges' => [
                        'replacement_charge' => [
                            'name' => 'Replacement Charge',
                            'amount' => 0.00
                        ],
                    ],
                ]
            ],
            'form_validator' => [
                'agreement_type' => 'required',
                'first_name' => 'required',
                'last_name' => 'required',
                'client_company_name' => 'required_if:agreement_type,commercial',
                'client_company_address' => 'required_if:agreement_type,commercial',
                'email' => 'required',
                'phone' => 'required',
                'street_address' => 'required',
                'suite_no' => 'nullable',
                'city' => 'required',
                'state' => 'required',
                'zip' => 'required',
                'agreement_date' => 'required',
                'agreement_duration' => 'required',
                'emergency_contact_name' => 'nullable',
                'emergency_contact_phone' => 'nullable',
            ],
            'company_signature_required' => true,
        ],
    ],
];
