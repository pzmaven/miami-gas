<?php

return [
    'company_details' => [
        'company_name' => 'Discount Gas LLC, DBA Miami Gas',
        'address' => '2747 NW 21 St. Miami, FL 33142',
        'phone' => '(305) 204-3333',
        'license' => '40396',
        'email' => 'joel@miamigas.com',
    ],

    'templates' => [
        'sales' => [
            'name' => 'Sales',
            'description' => 'Sales contract'
        ],
        'forklift' => [
            'name' => 'Forklift',
            'description' => 'Forklift contract'
        ]
    ],
];
