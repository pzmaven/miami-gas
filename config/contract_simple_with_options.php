<?php

return [
    'services' => [
        'propane' => [
            'name' => 'Propane',
            'charges' => [
                'price_per_gallon' => [
                    'name' => 'Price per Gallon',
                    'amount' => null
                ],
            ],
        ],

        'tank_lease' => [
            'name' => 'Tank Lease',
            'charges' => [
                'monthly_fee' => [
                    'name' => 'Monthly Fee',
                    'amount' => null
                ],
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => null
                ],
            ],
            'qty' => null
        ],

        'tank_monitoring' => [
            'name' => 'Tank Monitoring',
            'charges' => [
                'monthly_fee' => [
                    'name' => 'Monthly Fee',
                    'amount' => null
                ],
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => null
                ],
            ],
            'qty' => null
        ],

        'regulator' => [
            'name' => 'Regulator',
            'charges' => [
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => null
                ],
            ],
            'qty' => null
        ],
    ],

    'cancellation_fees' => [
        'name' => 'Cancellation Fees',
        'charges' => [
            'cancellation_fee' => [
                'name' => 'Cancellation Fee',
                'amount' => null
            ],
            'piping_reimbursement' => [
                'name' => 'Piping Reimbursement',
                'amount' => null
            ],
        ]
    ],

    'contract_default_values' =>[
        'forklift_cage' => [
            'name' => 'Forklift Cage',
            'charges' => [
                'replacement_charge' => [
                    'name' => 'Replacement Charge',
                    'amount' => 0.00
                ],
            ],
        ]
    ],
];
