<?php

return [
    'customer_discounts' => [
        'walk-in' => [
            'default_discount' => 0,
            // 'BBQ' => 3.98,
            // 'Forklift' => 19.98,
            // 'Propane per gallon' => 2.35
        ],

        'member' => [
            'default_discount' => 7,
            // 'BBQ' => 9.98,
            // 'Forklift' => 19.98,
            // 'Propane per gallon' => 2.35
        ],

        'employee' => [
            'default_discount' => 12,
            // 'BBQ' => 9.98,
            // 'Forklift' => 19.98,
            // 'Propane per gallon' => 2.35
        ]
    ],

    'customer_types' => [
        'walk-in' => 'Walk-in', 
        'member' => 'Member',
        'employee' => 'Employee'
    ],
];