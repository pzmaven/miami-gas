<?php

return [
    /*
    |--------------------------------------------------------------------------
    | External links
    |--------------------------------------------------------------------------
    |
    | Here you can specify any third party links
    |
    */

	'digitalhost' => [
		'title' => 'Digital Host',
		'url' => 'https://digitalhost.co/',
	],
];
