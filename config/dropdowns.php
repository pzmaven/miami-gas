<?php

return [
    'job_items_type' => [
        'Materials',
        'Equipment',
        'Service',
        'Subcontract',
        'Labor',
        'Fuel',
        'Ticket',
        'Parts',
        'Miscellaneous',
    ],

    'appointment_type' => [
        'Other',
        'Install',
        'Gas Check',
        'Inspection',
        'Propane Delivery - Residential',
        'Propane Delivery - Commercial',
        'Forklift Tank Exchange',
        'Service Request',
    ],

    'appointment_status' => [
        'Scheduled',
        'Completed',
        'Missed',
        'Cancelled',
        'Requested',
    ],

    'urgency' => [
        'Low',
        'Medium',
        'High',
        'Emergency',
    ],

    'expires' => [
        '30' => '30 days',
        '60' => '60 days',
        '90' => '90 days'
    ],

    'hours' => [
        '10:00:00' => '10:00am',
        '10:30:00' => '10:30am',
        '11:00:00' => '11:00am',
        '11:30:00' => '11:30am',
        '12:00:00' => '12:00pm',
        '12:30:00' => '12:30pm',
        '13:00:00' => '1:00pm',
        '13:30:00' => '1:30pm',
        '14:00:00' => '2:00pm',
        '14:30:00' => '2:30pm',
        '15:00:00' => '3:00pm',
        '15:30:00' => '3:30pm',
        '16:00:00' => '4:00pm',
    ],

    'payment_terms' => [
        'COD',
        '50-50',
        '50-25-25',
        '25-25-25-25',
        'Net-30',
        'Net-60',
        'Net-90',
        'Custom-see notes'
    ],

    'contract_duration' => [
        'five (5) years' => '5 years',
        'four (4) years' => '4 years',
        'three (3) years' => '3 years',
        'two (2) years' => '2 years',
        'one (1) year' => '1 year',
        'time in perpetuity until cancelled' => 'in perpetuity until cancelled',
        'One time only' => '(1) one time only',
    ],
];
